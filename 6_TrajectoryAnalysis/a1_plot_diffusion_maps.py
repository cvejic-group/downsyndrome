import gc
import os
import sys
import scanpy as sc
import pegasus as pg
import scvelo as scv
import matplotlib.pyplot as plt
import seaborn as sns
import matplotlib.backends.backend_pdf as mpdf


PROJ_DIR = "/lustre/scratch119/casm/team-cvejic/haoliang/results/DownSyndrome/"
IN_DIR = f"{PROJ_DIR}annotated_data/"
OUT_DIR = f"{PROJ_DIR}traj_res_final/"
COLOUR_PALETTE = {"Schwann cells": "#808080",
                  "Fibroblasts": "#ffc2d1",
                  "Myofibroblasts": "#ffe5ec",
                  "Pericytes": "#e6afb9",
                  "Mesenchymal cells 1": "#cbc3e3",
                  "Mesenchymal cells 2": "#da8ee7",
                  "Cycling CAR cells": "#bec1d4",
                  "CAR cells": "#4169e1",
                  "LEPR+ CAR cells": "#00bfff",
                  "Osteoprogenitors": "#ff8c00",
                  "Osteoblasts": "#880808",
                  "Immature endothelial cells": "#abffb1",
                  "Vascular endothelial cells": "#056608",
                  "Transitioning endothelial cells": "#22ee8f",
                  "Sinusoidal endothelial cells": "#9abb0a"}
CTYPE_LST = ["Schwann cells", "Osteoblasts", "Osteoprogenitors", "Sinusoidal endothelial cells", "Vascular endothelial cells",
             "CAR cells", "LEPR+ CAR cells", "Cycling CAR cells", "Mesenchymal cells 1", "Mesenchymal cells 2",
             "Transitioning endothelial cells", "Immature endothelial cells", "Fibroblasts", "Myofibroblasts", "Pericytes"]
if not os.path.exists(OUT_DIR):
    print("Creating output folder:", OUT_DIR, file=sys.stderr)
    os.mkdir(OUT_DIR)


def restructure(adata2restruct):
    # Parse latest clustring results and remove previous versions
    a = [col[8:] for col in adata2restruct.obs.columns if 'leiden_v' in col]
    max_num = max([int(x) for x in a if x.isdigit()])
    col_name = f"leiden_v{str(max_num)}"
    print(f"=====> Using {col_name} as the latest clustering column.",
          file=sys.stderr)
    adata2restruct.obs["batch"] = adata2restruct.obs["batch"].astype("str")
    adata2restruct.obs.rename(columns={col_name: "leiden_latest",
                                       "batch": "sample batch"},
                              inplace=True)
    # Only keep relevant columns in observations and variables
    adata2restruct.obs.drop([col
                             for col in adata2restruct.obs.columns
                             if ("leiden" in col) and (col != "leiden_latest")],
                            axis=1, inplace=True)
    adata2restruct.var = adata2restruct.var[["Ensembl", "Chromosome"]]
    print(f"Cell number: {adata2restruct.n_obs}",
          file=sys.stderr)


if __name__ == "__main__":
    pdf = mpdf.PdfPages(f"{OUT_DIR}diffusion_map_allstromal.pdf")
    for env in ["Healthy_Femur", "DownSyndrome_Femur"]:
        # Scanpy for selecting cell types
        adata = sc.read_h5ad(f"{IN_DIR}10X_{env}.h5ad")
        restructure(adata)
        adata = adata[adata.obs["leiden_latest"].isin(CTYPE_LST), :].copy()
        sc.tl.paga(adata, groups="leiden_latest")
        gc.collect()
        adata.write_h5ad(f"{OUT_DIR}{env}_stromalcells.h5ad")
        del adata
        gc.collect()
        # Pegasus for computing diffusion map
        adata = pg.read_input(f"{OUT_DIR}{env}_stromalcells.h5ad")
        pg.neighbors(adata, random_state=0, rep="pca_harmonize")
        pg.diffmap(adata, n_components=30, rep="pca_harmonize", random_state=0)
        pg.fle(adata, rep="diffmap", K=15, random_state=0, target_steps=8000)
        pg.write_output(adata, f"{OUT_DIR}{env}_stromalcells.fle.h5ad")
        del adata
        gc.collect()
        # Scanpy again for plotting diffusion map
        adata = sc.read_h5ad(f"{OUT_DIR}{env}_stromalcells.fle.h5ad")
        sns.set(rc={"figure.figsize": (9, 7)})
        sns.set_theme(style="white", font_scale=1)
        scv.pl.paga(adata, basis="fle", color="leiden_latest", size=25, title=env, show=False,
                    palette=COLOUR_PALETTE, node_size_scale=0, edge_width_scale=1, threshold=0.05)
        pdf.savefig()
        sns.set_theme(style="white", font_scale=1)
        _, axes = plt.subplots(3, 5, figsize=(15, 9))
        for i, ct in enumerate(adata.obs["leiden_latest"].unique()):
            scv.pl.scatter(adata, color="leiden_latest", size=25, basis="fle",
                           legend_loc="none", groups=[ct], title=f"{env}-{ct}",
                           show=False, ax=axes[int(i/5), i%5])
        for i in range(len(adata.obs["leiden_latest"].unique()), 15):
            axes[int(i/5), i%5].set_axis_off()
        plt.tight_layout()
        pdf.savefig()
    pdf.close()
