import gc
import sys
import scanpy as sc
import scvelo as scv
import cellrank as cr
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns


PROJ_DIR = "/lustre/scratch119/casm/team-cvejic/haoliang/results/DownSyndrome/"
OUT_DIR = f"{PROJ_DIR}traj_res_final/"
COLOUR_PALETTE = {"Schwann cells": "#808080",
                  "Fibroblasts": "#ffc2d1",
                  "Myofibroblasts": "#ffe5ec",
                  "Pericytes": "#e6afb9",
                  "Mesenchymal cells 1": "#cbc3e3",
                  "Mesenchymal cells 2": "#da8ee7",
                  "Cycling CAR cells": "#bec1d4",
                  "CAR cells": "#4169e1",
                  "LEPR+ CAR cells": "#00bfff",
                  "Osteoprogenitors": "#ff8c00",
                  "Osteoblasts": "#880808",
                  "Immature endothelial cells": "#abffb1",
                  "Vascular endothelial cells": "#056608",
                  "Transitioning endothelial cells": "#22ee8f",
                  "Sinusoidal endothelial cells": "#9abb0a"}
CTYPE_LN = ["CAR cells", "LEPR+ CAR cells", "Osteoprogenitors", "Osteoblasts"]


if __name__ == "__main__":
    for env in ["Healthy_Femur", "DownSyndrome_Femur"]:
        nstates, lin = (2, "Osteoblasts") if env == "Healthy_Femur" else (1, "Osteoprogenitors")
        print(f"Dealing with {env} with terminal state number = {nstates} of {lin} lineage...",
              file=sys.stderr)
        adata_env = sc.read_h5ad(f"{OUT_DIR}{env}_stromalcells.fle.h5ad")
        # Plot PAGA result on the diffusion map
        scv.pl.paga(adata_env, basis="fle", color="leiden_latest", size=25, title=env.replace("_", " "),
                    node_size_scale=0.5, edge_width_scale=1, threshold=0.05, legend_loc="right",
                    palette=COLOUR_PALETTE, show=False, dpi=600, figsize=(11, 8))
        plt.tight_layout()
        plt.savefig(f"{OUT_DIR}{env}_fle_diffmap.png", dpi=600, bbox_inches="tight", facecolor="white")
        # Select cell lineage
        adata_env = adata_env[adata_env.obs["leiden_latest"].isin(CTYPE_LN), :].copy()
        gc.collect()
        # Find root cell
        tmp_array = pd.DataFrame(adata_env.obsm["X_fle"])
        tmp_array = tmp_array.loc[list(adata_env.obs["leiden_latest"] == "CAR cells"), :].copy()
        gc.collect()
        idx_root = tmp_array.index[tmp_array[0] == tmp_array[0].min()][0]
        adata_env.uns["iroot"] = idx_root
        sc.pp.neighbors(adata_env, n_neighbors=15, n_pcs=adata_env.obsm["X_pca_harmonize"].shape[1],
                        use_rep="X_pca_harmonize", knn=True, method="umap", metric="euclidean", random_state=0)
        sc.tl.dpt(adata_env)
        # CellRank setting
        scv.settings.verbosity = 3
        scv.settings.set_figure_params("scvelo")
        cr.settings.verbosity = 2
        # CellRank preprocessing
        ##  Filter, normalize total counts and log-transform
        sc.pp.filter_genes(adata_env, min_cells=10)
        scv.pp.normalize_per_cell(adata_env)
        sc.pp.log1p(adata_env)
        ##  HVG annotation
        sc.pp.highly_variable_genes(adata_env) # don't add batch_key because some samples only have several cells within osteolineage
        print(f"This detected {adata_env.var['highly_variable'].sum()} highly variable genes.")
        # use scVelo's `moments` function for imputation - note that hack we're using here:
        #       we're copying our `.X` matrix into the layers because that's where `scv.tl.moments`
        #       expects to find counts for imputation
        adata_env.layers["spliced"] = adata_env.X
        adata_env.layers["unspliced"] = adata_env.X
        scv.pp.moments(adata_env, n_pcs=adata_env.obsm["X_pca_harmonize"].shape[1], n_neighbors=15)
        # Prepare kernel and estimator
        psd = cr.tl.kernels.PseudotimeKernel(adata_env, time_key="dpt_pseudotime")
        psd.compute_transition_matrix(threshold_scheme="soft")
        psd.compute_projection(basis="fle")
        g_fwd = cr.tl.estimators.GPCCA(psd)
        g_fwd.compute_schur(n_components=50)
        g_fwd.compute_macrostates(n_states=nstates, cluster_key="leiden_latest")
        g_fwd.compute_terminal_states()
        g_fwd.compute_absorption_probabilities()
        # Plot velocity stream, terminal states, and pseudotime
        sns.set_theme(style="white", font_scale=1)
        _, axes = plt.subplots(1, 3, figsize=(17, 5))
        scv.pl.velocity_embedding_stream(adata_env, color="leiden_latest", vkey="T_fwd", basis="fle", 
                                         legend_loc="right", show=False, size=30,
                                         ax=axes[0], title="Velocity Stream")      
        g_fwd.plot_terminal_states(discrete=True, size=30, basis="fle", legend_loc="on data", 
                                   title="CellRank Terminal States", show=False, ax=axes[1])
        scv.pl.scatter(adata_env, color="dpt_pseudotime", size=30, basis="fle",
                       title="Cell Pseudotime", show=False, ax=axes[2], cmap="magma")
        plt.tight_layout()
        plt.savefig(f"{OUT_DIR}{env}_vstream_tstates_ptime_fle_osteolineage.png",
                    dpi=600, bbox_inches="tight", facecolor="white")
        # Plot interested genes
        genes2plt = ["FOXC1", "PDGFRA", "RUNX2", "IBSP", "SP7", "BGLAP", "CXCL12", "SOX9", "MKI67"]
        sns.set_theme(style="white", font_scale=1)
        cr.pl.heatmap(adata_env, model=cr.ul.models.GAM(adata_env),
                      lineages=[lin], genes=genes2plt,
                      data_key="X", time_key="dpt_pseudotime", show_all_genes=True,
                      cluster_key=["leiden_latest"], figsize=(25, 10), cbar=True)
        plt.savefig(f"{OUT_DIR}{env}_gtrends_heatmaps.png", dpi=600, bbox_inches="tight", facecolor="white")
        sns.set(rc={"figure.figsize": (5, 5)})
        sns.set_theme(style="white", font_scale=1)
        scv.pl.scatter(adata_env, color=genes2plt, size=30, basis="fle", cmap="magma", ncols=3, show=False)
        plt.savefig(f"{OUT_DIR}{env}_genes_exp_on_fle_osteolineage.png", dpi=600, bbox_inches="tight", facecolor="white")
        # Extract driver genes
        os_drivers = g_fwd.compute_lineage_drivers(lineages=lin, cluster_key="leiden_latest", use_raw=False)
        os_drivers_sig = os_drivers.loc[os_drivers[f"{lin}_qval"] < 0.05, :].copy()
        os_drivers_sig.to_csv(f"{OUT_DIR}{env}_significant_driver_osteolineage.csv")
        print(f"{env} successfully processed!", file=sys.stderr)
