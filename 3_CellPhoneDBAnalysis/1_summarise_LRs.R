rm(list = ls())

library(dplyr)
library(ggplot2)

Sys.setlocale(locale = "C")

indir <- "~/Projects/DownSyndrome/cellphoneDB_results/cellphoneDB_output/"
blood_ct_lst <- c("HSCs/MPPs")
stromal_ct_lst <- c("Activated stellate cells", "Hepatic stellate cells", "Hepatocytes",
                    "LSECs", "Vascular endothelial cells")
outdir <- "~/Projects/DownSyndrome/cellphoneDB_results/fin_res/"

if (!dir.exists(outdir)) {
    dir.create(outdir)
    cat("Creating output directory...\n")
}

parse_blood_stromal <- function(input_df, blood_ct_lst) {
    input_df$BloodCell <- apply(input_df, MARGIN = 1,
                                FUN = function(x) {
                                    ifelse(x["Sender"] %in% blood_ct_lst,
                                           yes = x["Sender"],
                                           no = x["Receiver"])})
    input_df$BloodGene <- apply(input_df, MARGIN = 1,
                                FUN = function(x) {
                                    ifelse(x["Sender"] %in% blood_ct_lst,
                                           yes = x["Ligand"],
                                           no = x["Receptor"])})
    input_df$StromalCell <- apply(input_df, MARGIN = 1,
                                  FUN = function(x) {
                                      ifelse(x["Sender"] %in% blood_ct_lst,
                                             yes = x["Receiver"],
                                             no = x["Sender"])})
    input_df$StromalGene <- apply(input_df, MARGIN = 1,
                                  FUN = function(x) {
                                      ifelse(x["Sender"] %in% blood_ct_lst,
                                             yes = x["Receptor"],
                                             no = x["Ligand"])})
    return(input_df)
}

cphdb_res.h <- read.table(paste0(indir, "Healthy_Liver.processed_output.txt"),
                          sep = "\t", header = T) %>%
    filter(P < 0.001) %>%
    filter(Sender %in% blood_ct_lst | Receiver %in% blood_ct_lst) %>%
    filter(Sender %in% stromal_ct_lst | Receiver %in% stromal_ct_lst) %>%
    parse_blood_stromal(blood_ct_lst = blood_ct_lst) %>%
    dplyr::select(c(BloodCell, BloodGene, StromalGene, StromalCell, P, MeanExpr)) %>%
    dplyr::mutate(dataset = "Healthy")

cphdb_res.ds <- read.table(paste0(indir, "DownSyndrome_Liver.processed_output.txt"),
                           sep = "\t", header = T) %>%
    filter(P < 0.001) %>%
    filter(Sender %in% blood_ct_lst | Receiver %in% blood_ct_lst) %>%
    filter(Sender %in% stromal_ct_lst | Receiver %in% stromal_ct_lst) %>%
    parse_blood_stromal(blood_ct_lst = blood_ct_lst) %>%
    dplyr::select(c(BloodCell, BloodGene, StromalGene, StromalCell, P, MeanExpr)) %>%
    dplyr::mutate(dataset = "Ts21")

cphdb_res <- rbind(cphdb_res.h, cphdb_res.ds)

plt <- ggplot(cphdb_res) +
    geom_bar(aes(x = StromalCell, fill = dataset), colour = NA,
             position = position_dodge()) +
    scale_fill_manual(values = c("Healthy" = "royalblue", "Ts21" = "orange")) +
    xlab("") +
    ylab("number of L-R pairs") +
    theme_bw() +
    theme(text = element_text(size = 15))
ggsave(plt, filename = paste0(outdir, "1_barplot_LRpair_number.pdf"),
       width = 12, height = 9)
