from os import system as sys

#environments = ["DownSyndrome_Liver", "DownSyndrome_Femur", "Healthy_Liver", "Healthy_Femur"]
#environments = ["DownSyndrome_Femur", "Healthy_Liver", "Healthy_Femur", "DownSyndrome_Liver"]
#conditions = ["counts_original_stratified10", "counts_integrated_normed_stratified10"]
#conditions = ["original_stratified10", "original_stratified20", "original_stratified30", "original_stratified40"]
#conditions = ["original_stratified50", "original_stratified60"]
#subs = [3000, 5000, 8000, 10000, 15000, 20000, 30000]

#environments = ["DownSyndrome_Femur", "Healthy_Femur"]
#conditions = ["original"]
#environments = ["DownSyndrome_Liver", "Healthy_Liver"]
environments = ["DownSyndrome_Liver"]
conditions = ["original_stratified40"]
subs = [30000]
for env in environments:
    for cond in conditions:
        for sub in subs:
            fname = f"CellPhoneDB_10pc_{env}_{cond}_sub{sub}"
            fl = open(fname+".sh", 'w')
            fl.write("cd /nfs/research/gerstung/nelson/downsyndrome/PythonPipeline/CellPhoneDB/Results/Cell_types/\n")
            fl.write("conda activate minimal_env\n")
            fl.write(f"cellphonedb method statistical_analysis {env}_meta_{cond}.txt {env}_counts_{cond}.txt  --project-name={env}_CellPhoneDB_{cond}_sub{sub}_10pc --threads=64 --threshold=0.1 --debug-seed=42 --subsampling --subsampling-log false --subsampling-num-cells {sub} --counts-data=gene_name\n")

            print('bsub -n 1 -q bigmem -R "rusage[mem=1000000]" -M 1000000 -o {0}.txt -e {0} bash {0}.sh'.format(fname))
            sys('bsub -n 1 -q bigmem -R "rusage[mem=1000000]" -M 1000000 -o {0}.txt -e {0} bash {0}.sh'.format(fname))
