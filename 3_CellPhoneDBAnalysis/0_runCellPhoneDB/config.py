foetal_config = {
        "DownSyndrome_Liver": {
            "remove": ["X", "Cholangiocytes"],
            "name": "leiden_v10"
        },
        "DownSyndrome_Femur": {
            "remove": ["Unknown", "Striated muscle cells", "Odd PTPRC+ cells (to remove)", "34,0 (to remove)", "Odd NK cells (to remove)", "Pre pro B cells,4 (to remove)"],
            "name": "leiden_v13"
        },
        "Healthy_Liver": {
            "remove": ["Cholangiocytes", "38,0", "31,0", "Erythroid cells,2,1", "34", "Megakaryocytes,2,0", "36,1", "36,0,0", "36,0,1", "38,1", "Megakaryocytes,2,1"],
            "name": "leiden_v7"
        },
        "Healthy_Femur": {
            "remove": ["Striated muscle cells", "Unknown 1", "Unknown 2", "Cycling muscle cells", "0 (to remove)", "Osteoblasts,4 (to remove)", "Megakaryocytes,3 (to remove)"],
            "name": "leiden_v9"
        }
}

