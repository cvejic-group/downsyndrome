#!/usr/bin/env python
# coding: utf-8
import warnings
warnings.simplefilter("ignore")
import sys
import os

import matplotlib
import numpy as np
import scanpy as sc
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt

from math import log
from scipy import sparse
from matplotlib import cm
from matplotlib import colors

sys.path.append("/nfs/research/gerstung/nelson/downsyndrome/Functions/")
from scRNA_functions import scRNA_functions
fc = scRNA_functions()

from config import foetal_config

myColors = ['#e6194b', '#3cb44b', '#ffe119', '#4363d8', '#f58231',
            '#911eb4', '#46f0f0', '#f032e6', '#bcf60c', '#fabebe',
            '#008080', '#e6beff', '#9a6324', '#fffac8', '#800000',
            '#aaffc3', '#808000', '#ffd8b1', '#000075', '#808080',
            '#307D7E', '#000000', "#DDEFFF", "#000035", "#7B4F4B",
            "#A1C299", "#300018", "#C2FF99", "#0AA6D8", "#013349",
            "#00846F", "#8CD0FF", "#3B9700", "#04F757", "#C8A1A1",
            "#1E6E00", "#DFFB71", "#868E7E", "#513A01", "#CCAA35"]

myColorTissues = ['#ffe119', '#3cb44b', '#e6194b']
colors2 = plt.cm.Reds(np.linspace(0, 1, 128))
colors3 = plt.cm.Greys_r(np.linspace(0.7,0.8,20))
colorsComb = np.vstack([colors3, colors2])
mymap = colors.LinearSegmentedColormap.from_list('my_colormap', colorsComb)
sns.set_style("white")
in_dir = "/nfs/research/gerstung/nelson/data/downsyndrome/scRNAseq/outputs/"

def main(environment="DownSyndrome_Liver", stratified=False, integrated=False):
    # # Loading data
    inputs = in_dir+ "10X_{}_counts_integrated_norm.h5ad".format(environment) if integrated else in_dir+"10X_{}.h5ad".format(environment)
    out_dir = "/nfs/research/gerstung/nelson/downsyndrome/PythonPipeline/CellPhoneDB/"
    adata = sc.read_h5ad(inputs)
    annotations = "Cell labels" if integrated else foetal_config[environment]["name"]
    to_remove = foetal_config[environment]["remove"]
    adata = adata[~adata.obs[annotations].isin(to_remove),:]

    if not integrated:
        adata = adata.copy()
        counts = adata.X.toarray()
        adata.X = counts
        sc.pp.normalize_per_cell(adata, counts_per_cell_after=1e4)
        adata.layers["norm"] = adata.X
    the_counts = "counts_integrated_norm" if integrated else "norm"
    if not os.path.exists(out_dir+"Results/Cell_types/"):
        os.makedirs(out_dir+"Results/Cell_types/")

    # Stratify by sample combination (10 % default)
    if stratified:
        adatas = list()
        for s in adata.obs["sample"].unique():
            adata_t = adata[adata.obs["sample"]==s,:]
            sc.pp.subsample(adata_t, fraction=0.1)
            adatas.append(adata_t)
        adata = adatas[0].concatenate(adatas[1:], join="outer")

    extra = "_stratified10" if stratified else ""
    counts = pd.DataFrame(data=adata.layers[the_counts], index=adata.obs.index.tolist(), columns=adata.var.index.tolist()).T
    counts.index.name = "Gene"
    if integrated:
        counts.to_csv(out_dir+"Results/Cell_types/{0}_counts_integrated_normed{1}.txt".format(environment, extra), sep="\t")
    else:
        counts.to_csv(out_dir+"Results/Cell_types/{0}_counts_original{1}.txt".format(environment, extra), sep="\t")

    meta = pd.DataFrame(data=adata.obs[annotations].tolist(), index=adata.obs.index.tolist(), columns=["Cell type"])
    meta.index.name = "Cell"
    if integrated:
        meta.to_csv(out_dir+"Results/Cell_types/{0}_meta_integrated_normed{1}.txt".format(environment, extra), sep="\t")
    else:
        meta.to_csv(out_dir+"Results/Cell_types/{0}_meta_original{1}.txt".format(environment, extra), sep="\t")

if __name__=="__main__":
    # Adding an argument parser
    from argparse import ArgumentParser
    parser = ArgumentParser()
    parser.add_argument("-e", "--environment", type=str, help="Specifiy data-set and biological conditions",default="DownSyndrome_Liver")
    parser.add_argument("-s", "--stratified", action="store_true", help="Stratify by sample or not")
    parser.add_argument("-s", "--integrated", action="store_true", help="Select the integrated and normalised data")

    options = parser.parse_args()

    # Defining dictionary to be passed to the main function
    option_dict = dict( (k, v) for k, v in vars(options).items() if v is not None)
    print(option_dict)
    main(**option_dict)
