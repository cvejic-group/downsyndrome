#!/usr/bin/env python
# coding: utf-8
import warnings
warnings.simplefilter("ignore")
import sys
import os

import matplotlib
import numpy as np
import scanpy as sc
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt

from math import log
from scipy import sparse
from matplotlib import cm
from matplotlib import colors

import plotly.express as px
import plotly.offline as py

sys.path.append("/nfs/research/gerstung/nelson/downsyndrome/Functions/")
from scRNA_functions import scRNA_functions
fc = scRNA_functions()


myColors = ['#e6194b', '#3cb44b', '#ffe119', '#4363d8', '#f58231',
            '#911eb4', '#46f0f0', '#f032e6', '#bcf60c', '#fabebe',
            '#008080', '#e6beff', '#9a6324', '#fffac8', '#800000',
            '#aaffc3', '#808000', '#ffd8b1', '#000075', '#808080',
            '#307D7E', '#000000', "#DDEFFF", "#000035", "#7B4F4B",
            "#A1C299", "#300018", "#C2FF99", "#0AA6D8", "#013349",
            "#00846F", "#8CD0FF", "#3B9700", "#04F757", "#C8A1A1",
            "#1E6E00", "#DFFB71", "#868E7E", "#513A01", "#CCAA35"]

myColorTissues = ['#ffe119', '#3cb44b', '#e6194b']
colors2 = plt.cm.Reds(np.linspace(0, 1, 128))
colors3 = plt.cm.Greys_r(np.linspace(0.7,0.8,20))
colorsComb = np.vstack([colors3, colors2])
mymap = colors.LinearSegmentedColormap.from_list('my_colormap', colorsComb)
sns.set_style("white")

# Chemokines only -- this list should be updated to refect the top-ranking ligand-receptor pairs from the
# CellPhoneDB analysis
chemokines = ['XCR1','CEL','DARC','CXCR4','XCL1','PF4','ACKR2','CXCR5','CCL7','OPRD1','CCL3L3','CXCR6','CX3CL1',
              'CCR4','CCRL2','SDC1','CCL4','CXCL2','VASP','CXCL16','CXCL12','CCL8','CCL11','CCR7','CX3CR1','CCL1','CCL22','CXCL13',
              'CXCR2','CD4','DEFB103A','TGFB1','CCL25','ZG16B','PPBP','CCL2','CXCL5','CCR10','CCL23','CXCL1','CCL21','CCR3','XCL2',
              'CCL14','RARRES2','CCL20','ORM1','CCL27','IL8','CXCL6','CCR8','CCL28','CCL16','CXCR1','IL16','ACKR3','CCL3','YARS',
              'DEFB4A','SDC4','CCL18','CCL5','DEFB103B','CCR6','C14orf1','CXCL10','CCL24','CXCL9','CCL15','DEFB1','ITGB1','CMKLR1',
              'CCL17','CXCL11','DEFB4B','CXCR3','CCL3L1','CCL13','CCL26','CCR1','GNAI2','CCR2','HTR2A','CXCL3','CCL19','CCR9','CCR5',
              'GPR75','ACKR4','HRH4']

# Might change chemokines to just the relevant list of top scoring L-R pairs. Should initially run without these
stratification=60
outputs = [
    [
        "DownSyndrome_Femur_CellPhoneDB_original_sub30000_10pc",
        "Healthy_Femur_CellPhoneDB_original_sub30000_10pc",
    ],
    [
        "DownSyndrome_Liver_CellPhoneDB_original_stratified40_sub30000_10pc",
        "Healthy_Liver_CellPhoneDB_original_stratified40_sub30000_10pc",
    ]
]

out_dir = "/nfs/research/gerstung/nelson/downsyndrome/PythonPipeline/CellPhoneDB/"
for out in outputs:
    print(out)
    dfs_final = []
    for g in out:
        mean = pd.read_csv("Results/Cell_types/out/%s/means.txt"%(g), sep='\t')
        pval = pd.read_csv("Results/Cell_types/out/%s/pvalues.txt"%(g), sep='\t')

        # Get the number of cell types
        N_cellTypes = int(np.sqrt(mean.shape[1]-11))

        mean = mean[~(mean["interacting_pair"].str.contains("HLA") | mean["interacting_pair"].str.contains("complex"))]
        mean.reset_index(drop=True, inplace=True)

        pval = pval[~(pval["interacting_pair"].str.contains("HLA") | pval["interacting_pair"].str.contains("complex"))]
        pval.reset_index(drop=True, inplace=True)

        columns = ["interacting_pair"] + mean.columns[-N_cellTypes**2:].tolist()

        df_means = mean[columns]
        df_pvals = pval[columns]

        df_means.sort_values(by="interacting_pair", ascending=True, inplace=True)
        df_means.reset_index(inplace=True, drop=True)

        df_pvals.sort_values(by="interacting_pair", ascending=True, inplace=True)
        df_pvals.reset_index(inplace=True, drop=True)

        keep_idx = list()
        for i in df_pvals["interacting_pair"].tolist():
            tmp = df_pvals[df_pvals["interacting_pair"]==i]
            pvalues = tmp.T[tmp.index.tolist()[0]].tolist()[1:]
            if all(pvalues) > 0.01:
                pass
            else:
                keep_idx.append(tmp.index.tolist()[0])

        df_means = df_means[df_means.index.isin(keep_idx)]
        df_pvals = df_pvals[df_pvals.index.isin(keep_idx)]

        dfs = list()
        for col in df_means.columns[1:]:
            tmp = df_means[["interacting_pair"]]
            tmp["interacting_group"] = col
            tmp["color"] = df_means[col].tolist()
            tmp["size"] = df_pvals[col].tolist()
            dfs.append(tmp)

        df = pd.concat(dfs, axis=0)
        df = df.rename(columns={"interacting_group": "Cell group",
                                "interacting_pair": "L-R pair",
                                "size":"Corrected p-value",
                                "color":"log(1+expression)"})
        corrected = 0.01/len(df) # We will be conservative and apply the Bonferroni correction

        df["log(1+expression)"] = round(np.log1p(df["log(1+expression)"]), 3)
        df = df[df["Corrected p-value"]<=corrected]
        df = df[df["log(1+expression)"]>=1.0]
        df["tissue"] = g
        df["Combination"] = df["L-R pair"]+": "+df["Cell group"]

        dfs_final.append(df)
        df_sorted = df.sort_values(by="log(1+expression)",ascending=False)
        df_sorted.reset_index(drop=True, inplace=True)
        del df_sorted["Combination"]
        df_sorted.to_csv(out_dir+"Results/csvs/10pc/{}_filtered_pairs.csv".format(g))
    '''
    df = pd.concat(dfs_final)
    df_ds = df[df["tissue"].str.contains("DownSyndrome")]
    df_h = df[df["tissue"].str.contains("Healthy")]
    combs_ds, combs_h = df_ds["Combination"].tolist(), df_h["Combination"].tolist()

    combs_diff = [x for x in combs_ds if x not in combs_h]
    df_ds = df_ds[df_ds["Combination"].isin(combs_diff)]
    del df_ds["Combination"]
    df_ds.sort_values(by="log(1+expression)",ascending=False, inplace=True)
    df_ds.reset_index(drop=True, inplace=True)
    df_ds.to_csv("Results/csvs/DS_only_Healthy_all_used_{}_filtered_pairs.csv".format("_".join(list(g)[0].split("_")[1:])))
    '''
