# CellPhoneDB

These scripts run the full CellPhoneDB procedure on the ```h5ad``` gene expression matrices. The user simply needs to provide this
input, together with the cell type annotations, and the code takes care of the rest. Before running CellPhoneDB it's necessary to
first pre-process the data into a more appropriate format. In addition, for very large data-sets, running CellPhoneDB on more than
O(10^6) cells is computationally intractable ... so don't do that. Instead, we need to first perform a stratified sampling (stratified with
respect to the technical and biological replicate samples) of 10/20/30/40%. This data is then pre-processed for the CellPhoneDB algorithm.

## 1. Preprocessing and sampling the gene expression data
The data processing, and stratified sampling, is handled by ```data_creation.py```: 

```
usage: data_creation.py [-h] [-e ENVIRONMENT] [-s] [-i]

optional arguments:
  -h, --help            show this help message and exit
  -e ENVIRONMENT, --environment ENVIRONMENT
                        Specifiy data-set and biological conditions
  -s, --stratified      Stratify by sample or not
  -i, --integrated      Select the integrated and normalised data
```

Running ```data_creation.py``` is computationally intensive, so I recommend running ```run_data_creation.py```, which is a wrapper
around the above script. This takes as input different environments and stratifications, calls ```data_creation.py```, and submits
to a ```bigmem``` batch farm a la EBI. 

## 2. Run CellPhoneDB
The next step is to run CellPhoneDB on the different processed data. CellPhoneDB works best by performing its own subsampling on the
data, and this can be specified by the ```--subsampling``` flag when running the ```cellphonedb``` command (see line 13 of 
```run_CellPhoneDB.py```. This ```run_CellPhoneDB.py``` can be called directly with python, and submits CellPhoneDB jobs to the 
batch farm. For more detailed information on the individual CellPhoneDB commands, please read here: https://github.com/ventolab/CellphoneDB#readme

One may then call and submit the CellPhoneDB jobs in the usual fashion:

```
python run_CellPhoneDB.py
```

This will output directories containing csvs which summarise the significant Ligand-Receptor pairs (extracted from the CellPhoneDB database) for the
cell type annotations specified in your data, and also detail the co-expression and associated adjusted p-values for each pair. Having this information
is highly relevent for any downstream analysis which filters on p-values as a means of assessing statistically significant interactors. 

## 3. Plotting and filtering
```plot.py``` is a slight misnomer. I had originally envishioned one master plotting script, but concluded that CellPhoneDB outputs don't make for the
most informative plots. Instead, ```plot.py``` takes as input the separate Down syndrome and healthy outputs, filters the relevant
Ligand-Receptor pairs according to their adjusted p-value and mean co-expresson values, and stores the results in csv files. It makes csv files for
the significant interactors in each environment, and an additional csv file for the interactors found in only the Down syndrome  environment. The output of this
script is esseneitally the final set of relevant results. 

## 4. Check similarity between results
Something which occured to me during the writing of these scripts was that CellPhoneDB can work with different levels of data stratification (10 vs 40%) and 
indeed different levels of subsampling of the data (see documentation for more deets on that). I found this rather concerning: would subsampling 10 vs. 40% give
us the same results? If not, then the method's essentially unstable and we would need to test many different levels of stratification until above a threshold which
does yield stable results. By stable, I mean the final filtered L-R lists being indentical/close to identical. To address this problem, the script
```run_similarity_score.py``` takes different stratified outputs for the same environment (e.g. Down syndrome), and calculates the cosine similarity between the
combined cell types and L-R pairs in the ranked list. It then plots this similarity as a function of the degree of stratification and subsampling, such that
a region on the phase space where the cosine similarity is both high (close to 1) and stable (flat wrt samples) can be established. More on the cosine 
similarity can be found here: https://scikit-learn.org/stable/modules/metrics.html#cosine-similarity. 

```run_similarity_score.py``` is called with several different arguments:

```
usage: run_similarity_score.py [-h] [-e ENVIRONMENT] [-r REFERENCE]

optional arguments:
  -h, --help            show this help message and exit
  -e ENVIRONMENT, --environment ENVIRONMENT
                        Specifiy data-set and biological conditions
  -r REFERENCE, --reference REFERENCE
                        Specifiy the reference subsampling strategy
```
