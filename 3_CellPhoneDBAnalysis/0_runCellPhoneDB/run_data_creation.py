from os import system as sys

'''
# All combinations:
environments = ["DownSyndrome_Liver", "DownSyndrome_Femur", "Healthy_Liver", "Healthy_Femur"]
conditions = ["stratified", "integrated", "stratified_integrated", ""]
'''
environments = ["DownSyndrome_Femur", "Healthy_Femur"]
conditions = ["stratified"]
strats = [0.5, 0.6]
for env in environments:
    fname = f"DataCreation_FULL_{env}"
    fl = open(fname+".sh", 'w')
    fl.write("cd /nfs/research/gerstung/nelson/downsyndrome/PythonPipeline/CellPhoneDB/\n")
    fl.write("conda activate minimal_env\n")
    fl.write('python data_creation.py --environment="{0}"\n'.format(env))

    print('bsub -n 1 -q bigmem -R "rusage[mem=1000000]" -M 1000000 -o {0}.txt -e {0} bash {0}.sh'.format(fname))
    sys('bsub -n 1 -q bigmem -R "rusage[mem=1000000]" -M 1000000 -o {0}.txt -e {0} bash {0}.sh'.format(fname))
    '''
    for cond in conditions:
        for strat in strats:
            fname = f"DataCreation_{env}_{cond}_{strat}"
            fl = open(fname+".sh", 'w')
            fl.write("cd /nfs/research/gerstung/nelson/downsyndrome/PythonPipeline/CellPhoneDB/\n")
            fl.write("conda activate minimal_env\n")
            if cond=="stratified":
                fl.write('python data_creation.py --environment="{0}" --stratified --strat_frac={1}\n'.format(env, strat))
            elif cond=="integrated":
                fl.write('python data_creation.py --environment="{0}" --integrated\n'.format(env))
            elif cond=="stratified_integrated":
                fl.write('python data_creation.py --environment="{0}" --stratified --integrated\n'.format(env))
            else:
                fl.write('python data_creation.py --environment="{0}"\n'.format(env))

            print('bsub -n 1 -q bigmem -R "rusage[mem=300000]" -M 300000 -o {0}.txt -e {0} bash {0}.sh'.format(fname))
            sys('bsub -n 1 -q bigmem -R "rusage[mem=300000]" -M 300000 -o {0}.txt -e {0} bash {0}.sh'.format(fname))
    '''
