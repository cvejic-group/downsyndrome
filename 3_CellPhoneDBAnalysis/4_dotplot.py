import gc
import os
import sys
import scanpy as sc
import matplotlib.pyplot as plt

PROJ_DIR = "/lustre/scratch119/casm/team-cvejic/haoliang/results/DownSyndrome/"
IN_DIR = f"{PROJ_DIR}annotated_data/"
OUT_DIR = f"{PROJ_DIR}cellphoneDB_res/"
if not os.path.exists(OUT_DIR):
    print(f"Createing output folder: {OUT_DIR}", file=sys.stderr)
    os.mkdir(OUT_DIR)

def restructure(adata2restruct):
    # Parse latest clustring results and remove previous versions
    a = [col[8:] for col in adata2restruct.obs.columns if 'leiden_v' in col]
    max_num = max([int(x) for x in a if x.isdigit()])
    col_name = f"leiden_v{str(max_num)}"
    print(f"=====> Using {col_name} as the latest clustering column.",
          file=sys.stderr)
    adata2restruct.obs["batch"] = adata2restruct.obs["batch"].astype("str")
    adata2restruct.obs.rename(columns={col_name: "leiden_latest",
                                       "batch": "sample batch"},
                              inplace=True)
    # Only keep relevant columns in observations and variables
    adata2restruct.obs.drop([col
                             for col in adata2restruct.obs.columns
                             if ("leiden" in col) and (col != "leiden_latest")],
                            axis=1, inplace=True)
    adata2restruct.var = adata2restruct.var[["Ensembl", "Chromosome"]]
    print(f"Cell number: {adata2restruct.n_obs}",
          file=sys.stderr)


if __name__ == "__main__":
    adata_h = sc.read_h5ad(f"{IN_DIR}10X_Healthy_Liver.h5ad")
    restructure(adata_h)
    adata_ds = sc.read_h5ad(f"{IN_DIR}10X_DownSyndrome_Liver.h5ad")
    restructure(adata_ds)
    adata = adata_h.concatenate(adata_ds, join="outer",
                                batch_key="dataset",
                                batch_categories=["Healthy", "Ts21"])
    sc.pp.filter_genes(adata, min_cells=10)
    sc.pp.normalize_total(adata, target_sum=1e4)
    sc.pp.log1p(adata)
    adata = adata[adata.obs["leiden_latest"].isin(["HSCs/MPPs",
                                                   "Hepatocytes",
                                                   "Vascular endothelial cells"]), :].copy()
    gc.collect()
    adata.obs["ctype-ds"] = (adata.obs["leiden_latest"].astype("str") + "-" + adata.obs["dataset"].astype("str")).astype("category")
    adata.obs["ctype-ds"] = adata.obs["ctype-ds"].cat.set_categories(["HSCs/MPPs-Healthy", "HSCs/MPPs-Ts21",
                                                                      "Hepatocytes-Healthy", "Hepatocytes-Ts21",
                                                                      "Vascular endothelial cells-Healthy",
                                                                      "Vascular endothelial cells-Ts21"])
    sc.pl.dotplot(adata, var_names=["VEGFB", "FLT1", "ANGPT1", "TEK", "NOTCH1", "JAG1", "TFRC", "TF"],
                  groupby="ctype-ds", figsize=(9, 5), standard_scale="var", show=False, swap_axes=False)
    plt.savefig(f"{OUT_DIR}cellphoneDB_dotplots_new.pdf", bbox_inches="tight")
