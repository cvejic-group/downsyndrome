foetal_config = {
        "DownSyndrome_Liver": {
            "remove": ["X", "Cholangiocytes"],
            "name": "leiden_v10",
            "spatial": ["spaceranger122_count_36337_OT_F_Visium9720395_GRCh38-3_0_0", "spaceranger122_count_36961_OT_F_Visium9998361_GRCh38-3_0_0", "spaceranger122_count_38823_OT_F_Visium9880863_GRCh38-3_0_0",
                        "spaceranger122_count_38825_OT_F_Visium9880673_GRCh38-3_0_0", "spaceranger122_count_36337_OT_F_Visium9720396_GRCh38-3_0_0", "spaceranger122_count_38822_OT_F_Visium9880766_GRCh38-3_0_0",
                        "spaceranger122_count_38824_OT_F_Visium9880576_GRCh38-3_0_0", "spaceranger122_count_39397_OT_F_Visium10405238_GRCh38-3_0_0", "spaceranger122_count_36338_OT_F_Visium9720491_GRCh38-3_0_0",
                        "spaceranger122_count_38822_OT_F_Visium9880767_GRCh38-3_0_0", "spaceranger122_count_38824_OT_F_Visium9880577_GRCh38-3_0_0",  "spaceranger122_count_39397_OT_F_Visium10405239_GRCh39-3_0_0",
                        "spaceranger122_count_36338_OT_F_Visium9720492_GRCh38-3_0_0", "spaceranger122_count_38822_OT_F_Visium9880768_GRCh38-3_0_0", "spaceranger122_count_38825_OT_F_Visium9880670_GRCh38-3_0_0",
                        "spaceranger122_count_39397_OT_F_Visium10405240_GRCh38-3_0_0", "spaceranger122_count_36644_OT_F_Visium9879893_GRCh38-3_0_0", "spaceranger122_count_38822_OT_F_Visium9880769_GRCh38-3_0_0",
                        "spaceranger122_count_38825_OT_F_Visium9880671_GRCh38-3_0_0", "spaceranger122_count_39397_OT_F_Visium10405241_GRCh38-3_0_0", "spaceranger122_count_36644_OT_F_Visium9879894_GRCh38-3_0_0",
                        "spaceranger122_count_38823_OT_F_Visium9880862_GRCh38-3_0_0", "spaceranger122_count_38825_OT_F_Visium9880672_GRCh38-3_0_0"]
        },
        "DownSyndrome_Femur": {
            "remove": ["Odd PTPRC+ cells (to remove)", "34,0 (to remove)", "Odd NK cells (to remove)", "Pre pro B cells,4 (to remove)"],
            "name": "leiden_v12",
            "spatial": [] # No femur Visium
        },
        "Healthy_Liver": {
            "remove": ["Cholangiocytes", "38,0", "31,0", "Erythroid cells,2,1", "34", "Megakaryocytes,2,0", "36,1", "36,0,0", "36,0,1", "38,1", "Megakaryocytes,2,1"],
            "name": "leiden_v7",
            "spatial": ["spaceranger122_count_36337_OT_F_Visium9720393_GRCh38-3_0_0", "spaceranger122_count_36338_OT_F_Visium9720490_GRCh38-3_0_0", "spaceranger122_count_36961_OT_F_Visium9998362_GRCh38-3_0_0",
                        "spaceranger122_count_38823_OT_F_Visium9880865_GRCh38-3_0_0", "spaceranger122_count_36337_OT_F_Visium9720394_GRCh38-3_0_0", "spaceranger122_count_36644_OT_F_Visium9879895_GRCh38-3_0_0",
                        "spaceranger122_count_36961_OT_F_Visium9998363_GRCh38-3_0_0", "spaceranger122_count_36338_OT_F_Visium9720489_GRCh38-3_0_0", "spaceranger122_count_36644_OT_F_Visium9879896_GRCh38-3_0_0",
                        "spaceranger122_count_38823_OT_F_Visium9880864_GRCh38-3_0_0"]
        },
        "Healthy_Femur": {
            "remove": ["0 (to remove)", "Osteoblasts,4 (to remove)", "Megakaryocytes,3 (to remove)"],
            "name": "leiden_v9",
            "spatial": [] # No femur Visium
        }
}

