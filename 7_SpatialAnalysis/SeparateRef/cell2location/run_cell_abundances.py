import sys
import scanpy as sc
import anndata
import pandas as pd
import numpy as np
import os
import gc

import cell2location
import scvi
import torch
import matplotlib as mpl
from matplotlib import rcParams
import matplotlib.pyplot as plt
import seaborn as sns
import matplotlib.backends.backend_pdf as mpdf

def select_slide(adata, s, s_col='sample'):
    """ This function selects the data for one slide from the spatial anndata object.
        :param adata: Anndata object with multiple spatial experiments
        :param s: name of selected experiment
        :param s_col: column in adata.obs listing experiment name for each location
    """

    slide = adata[adata.obs[s_col].isin([s]), :]
    s_keys = list(slide.uns['spatial'].keys())
    s_spatial = np.array(s_keys)[[s in k for k in s_keys]][0]
    slide.uns['spatial'] = {s_spatial: slide.uns['spatial'][s_spatial]}

    return slide

seed = 42
torch.manual_seed(seed)
device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")

outDir = "/lustre/scratch118/opentargets/opentargets/OTAR2060/nelson/outputs/downsyndrome/cell2location/spatial_model/"
def main(environment="DownSyndrome_Liver"):
    # Load spatial model
    adata_vis = sc.read_h5ad(f"{outDir}{environment}/sp.h5ad")

    # Add 5% quantile, representing confident cell abundance, 'at least this amount is present',
    # to adata.obs with nice names for plotting
    adata_vis.obs[adata_vis.uns['mod']['factor_names']] = adata_vis.obsm['q05_cell_abundance_w_sf']
    cell_types = adata_vis.uns['mod']['factor_names']
    pdf = mpdf.PdfPages(f"{outDir}{environment}/Cell2location_{environment}_Abundances.pdf")
    for sample in adata_vis.obs["sample"].unique():
        slide = select_slide(adata_vis, sample)

        # Plot in spatial coordinates
        with mpl.rc_context({"axes.facecolor":"black", "figure.figsize": [4.5, 5]}):
            sc.pl.spatial(
                    slide,
                    cmap="magma",
                    color=cell_types,
                    ncols=4, size=1.3,
                    img_key="hires",
                    # Limit color scale at 99.2% quantile of cell abundance
                    vmin=0,
                    vmax="p99.2"
            )
        pdf.savefig()
    pdf.close()

    # Now run the colocation model
    from cell2location import run_colocation
    res_dict, adata_vis = run_colocation(
                                adata_vis,
                                model_name="CoLocatedGroupsSklearnNMF",
                                train_args={
                                    "n_fact": np.arange(3, 40),
                                    "sample_name_col": "sample",
                                    "n_restarts": 5 # Number of training restarts
                                },
                                export_args={'path': f"{outDir}{environment}/colocation_model/"}
    )

if __name__=="__main__":
    # Adding an argument parser
    from argparse import ArgumentParser
    parser = ArgumentParser()
    parser.add_argument("-e", "--environment", type=str, help="Select DS or healthy environment",default="DownSyndrome_Liver")

    options = parser.parse_args()

    # Defining dictionary to be passed to the main function
    option_dict = dict( (k, v) for k, v in vars(options).items() if v is not None)
    print(option_dict)
    main(**option_dict)
