import sys
import scanpy as sc
import pandas as pd
import numpy as np

import matplotlib as mpl
import matplotlib.pyplot as plt
import seaborn as sns


def select_slide(adata2sel, s, s_col='sample'):
    """ 
    Select the data for one slide from the spatial anndata object.

    :param adata2sel: Anndata object with multiple spatial experiments
    :param s: name of selected experiment
    :param s_col: column in adata2sel.obs listing experiment name for each location
    """
    idx2sel = adata2sel.obs[s_col].isin([s])
    slide = adata2sel.copy()
    for layer in slide.layers:
        slide.layers[layer] = slide.layers[layer][idx2sel, :]
    slide = slide[idx2sel, :].copy()
    s_keys = list(slide.uns["spatial"].keys())
    s_spatial = np.array(s_keys)[[s in k for k in s_keys]][0]
    slide.uns['spatial'] = {s_spatial: slide.uns['spatial'][s_spatial]}
    return slide


def plot_spatial(adata2plot, pdf, color_by=None, ncols=None):
    """
    Plot VISIUM's spatial results on the given column.
    """
    if color_by == None or any([x not in adata2plot.obs.columns for x in list(color_by)]):
        print("ERROR! Invalide color_by argument:", color_by, file=sys.stderr)
        sys.exit(1)
    if ncols == None and isinstance(color_by, list) and len(color_by) >= 3:
        ncols = 3
    elif ncols == None and isinstance(color_by, list):
        ncols = len(color_by)
    elif ncols == None:
        ncols = 1
    with mpl.rc_context({'figure.figsize': [6, 7], 'axes.facecolor': 'white'}):
        sc.pl.spatial(adata2plot, color=color_by,
                      img_key="hires",
                      cmap="magma",
                      ncols=ncols,
                      library_id=list(adata2plot.uns['spatial'].keys())[0],
                      size=1.3,
                      vmin=0, vmax="p99.2")
        pdf.savefig()
        plt.close()


def plot_boxes(df, x, y, y_order, fig_path=None, pdf=None, groupby=None, w=15, h=12, fscale=1.5, legend_loc="best"):
    plt.figure(figsize=(w, h))
    sns.set(font_scale=fscale, style="white")
    ax = sns.boxplot(y=y, x=x, data=df, order=y_order, hue=groupby, orient="h")
    sns.swarmplot(y=y, x=x, data=df, order=y_order, hue=groupby, palette="dark:black",
                  size=3, dodge=(groupby is not None))
    handles, labels = ax.get_legend_handles_labels()
    plt.legend(handles[:2], labels[:2], loc=legend_loc)
    plt.ylabel(None)
    plt.tight_layout()
    if pdf is not None:
        pdf.savefig()
    elif fig_path is not None:
        plt.savefig(fig_path)
    else:
        print("Error! fig_path and pdf in plot_boxes cannot be None at the same time!",
              file=sys.stderr)
        sys.exit(1)
    plt.close()
