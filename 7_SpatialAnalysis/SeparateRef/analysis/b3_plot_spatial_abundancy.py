import sys
import scanpy as sc
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import matplotlib.backends.backend_pdf as mpdf

from scipy.spatial.distance import pdist, squareform
from scipy.cluster.hierarchy import linkage, dendrogram

from _functions import select_slide, plot_spatial

ABD_FIELD = "means_cell_abundance_w_sf"
ENV_LST = ["Healthy_Liver", "DownSyndrome_Liver"]
GENE_COUNT_THRESH = 800
CTYPE_TO_PLOT = ["LSECs", "Hepatic stellate cells", "Activated stellate cells", "Vascular endothelial cells",
                 "Hepatocytes", "HSCs/MPPs"]

OUT_DIR = f"/lustre/scratch119/casm/team-cvejic/haoliang/results/DownSyndrome/STx_results_{GENE_COUNT_THRESH}/"


if __name__ == "__main__":
    sns.set_theme(font_scale=1.5, style="white")
    for i, env in enumerate(ENV_LST):
        # Load cleaned data, transform counts, and harmonisation
        adata = sc.read_h5ad(f"{OUT_DIR}{env}_sp_cleaned.h5ad")
        all_ctypes = adata.uns["mod"]["factor_names"].tolist()
        # Compute cell type percentage
        adata.obs[adata.obsm[ABD_FIELD].columns] = adata.obsm[ABD_FIELD]
        adata.obs.rename(columns={col: col.replace(f"{ABD_FIELD.replace('_cell', 'cell')}_", "")
                                  for col in adata.obsm[ABD_FIELD].columns}, inplace=True)
        # Plot spatial abundancies
        pdf = mpdf.PdfPages(f"{OUT_DIR}spatial_abundancies_{env}.pdf")
        for sec in adata.obs["sample"].unique():
            adata_sec = select_slide(adata, sec, "sample")
            plot_spatial(adata_sec, pdf=pdf,
                         color_by=CTYPE_TO_PLOT,
                         ncols=3)
        pdf.close()
