import sys
import scanpy as sc
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import matplotlib.backends.backend_pdf as mpdf

from scipy.stats import pearsonr
from scipy.stats import spearmanr
from sklearn.feature_selection import mutual_info_regression
from scipy.stats import mannwhitneyu
from statsmodels.stats.multitest import fdrcorrection

from _functions import select_slide
from _functions import plot_boxes

ENV_LST = ["Healthy_Liver", "DownSyndrome_Liver"]
MODE = "Pearson"

IN_DIR = "/lustre/scratch118/opentargets/opentargets/OTAR2060/nelson/outputs/downsyndrome/cell2location/spatial_model/"
LR_DIR = "/lustre/scratch119/casm/team-cvejic/haoliang/results/DownSyndrome/cellphoneDB_analysis/"
OUT_DIR = f"/lustre/scratch119/casm/team-cvejic/haoliang/results/DownSyndrome/coexpression_on_clusters/"

LR_LST = [("VEGFB", "FLT1"), ("ANGPT1", "TEK"), ("ANGPT2", "TEK"),
          ("NOTCH1", "JAG1"), ("KIT", "KITLG"), ("EFNA4", "EPHA2"),
          ("EFNA4", "EPHA4"), ("TFRC", "TF"), ("TFR2", "TF"),
          ("TNFRSF1A", "GRN"), ("TNFRSF1B", "GRN"), ("CD74", "MIF")]


def transform_counts(adata2trans):
    adata2trans.layers["raw"] = adata2trans.X.todense().copy()
    # Analytic Pearson residuals
    sc.experimental.pp.normalize_pearson_residuals(adata2trans)
    adata2trans.layers["APR"] = adata2trans.X.copy()
    return adata2trans


def clustering(adata2clustering, leiden_res=1):
    adata2clustering.X = adata2clustering.layers["APR"]
    # HVG detection
    sc.pp.highly_variable_genes(adata2clustering, batch_key="sample",
                                min_mean=0.00125, max_mean=3, min_disp=0.5)
    # PCA
    sc.tl.pca(adata2clustering, n_comps=50, svd_solver="auto",
              use_highly_variable=True, random_state=42)
    # Compute neighborhood graph and do clustering
    sc.pp.neighbors(adata2clustering, n_pcs=adata2clustering.obsm["X_pca"].shape[1],
                    use_rep="X_pca", knn=True, random_state=42,
                    method="umap", metric="euclidean")
    sc.tl.leiden(adata2clustering, resolution=leiden_res, random_state=42,
                 key_added=f"leiden_res{leiden_res}")
    sc.tl.umap(adata2clustering, random_state=42, n_components=2,
               init_pos="random")
    return adata2clustering


def plot_clusters(adata, gene_counts, cluster_col, pdf):
    sns.set(rc={"figure.figsize": (9, 9)})
    sns.set_theme(style="white", font_scale=2)
    _, axes = plt.subplots(1, 2, figsize=(15, 6))
    sc.pl.umap(adata, color=cluster_col, size=50, palette="coolwarm",
               na_in_legend=False, components="1,2", use_raw=False,
               legend_loc="on data", ax=axes[0], show=False, title="")
    sc.pl.spatial(adata, color=cluster_col, palette="coolwarm",
                  img_key="hires", library_id=list(adata.uns['spatial'].keys())[0],
                  size=1.3, legend_loc=None, ax=axes[1], title="", show=False)
    plt.suptitle(adata.obs["sample"][0], y=0.9, fontsize="medium")
    plt.tight_layout()
    pdf.savefig()
    plt.close()
    _, axes = plt.subplots(nrows=2, ncols=6, figsize=(54, 18))
    for iplot, lr in enumerate(LR_LST):
        sns.scatterplot(data=gene_counts, x=lr[0], y=lr[1], hue="index",
                        s=200, palette="coolwarm", legend=None,
                        ax=axes[int(iplot / 6), iplot % 6])
        for i in gene_counts.index:
            axes[int(iplot / 6), iplot % 6].annotate(xy=(gene_counts.loc[i, lr[0]],
                                                         gene_counts.loc[i, lr[1]]),
                                                     text=gene_counts.loc[i, "index"])
        corr_p = np.round(pearsonr(gene_counts[lr[0]], gene_counts[lr[1]])[0], 2)
        corr_s = np.round(spearmanr(gene_counts[lr[0]], gene_counts[lr[1]])[0], 2)
        mi = np.round(mutual_info_regression(X=gene_counts[[lr[0]]],
                                             y=gene_counts[lr[1]],
                                             random_state=42)[0], 2)
        axes[int(iplot / 6), iplot % 6].text(x=max(gene_counts[lr[0]]), y=min(gene_counts[lr[1]]),
                                             s=f"Pearson Corr.: {corr_p}\nSpearman Corr.: {corr_s}\nMI: {mi}",
                                             va="bottom", ha="right")
    plt.tight_layout()
    pdf.savefig()
    plt.close()


if __name__ == "__main__":
    # Load and make all LR list
    lr_pair_all = None
    for env in ENV_LST:
        lr_pair = pd.read_table(f"{LR_DIR}data/{env}.processed_output.txt")
        lr_pair["gene1"] = lr_pair[["Ligand", "Receptor"]].min(axis=1)
        lr_pair["gene2"] = lr_pair[["Ligand", "Receptor"]].max(axis=1)
        lr_pair = lr_pair.loc[lr_pair["Sender"].isin(["HSCs/MPPs"]) | lr_pair["Receiver"].isin(["HSCs/MPPs"]),
                              ["gene1", "gene2"]].drop_duplicates()
        lr_pair[f"found in {env}"] = True
        lr_pair.set_index(["gene1", "gene2"], inplace=True)
        lr_pair_all = pd.concat([lr_pair_all, lr_pair],
                                axis=1, join="outer").fillna(False)
    lr_pair_all.reset_index(inplace=True)
    lr_pair_all.to_csv(f"{LR_DIR}summarised_LRs_HSCsMPPs.csv")
    # Genes to consider
    all_genes = list(set(lr_pair_all[["gene1", "gene2"]].stack().values))
    # Estimate L-R coexpression by section and plot clusters to check
    for leiden_res in [1, 2, 5, 10]:
        coexp = {}
        pdf = mpdf.PdfPages(f"{OUT_DIR}clusterplots_res{leiden_res}.pdf")
        for env in ENV_LST:
            adata = sc.read_h5ad(f"{IN_DIR}{env}/sp.h5ad")
            for sec in adata.obs["sample"].unique():
                # select the section and transform counts
                adata_sub = select_slide(adata, sec, "sample")
                transform_counts(adata_sub)
                # Identify clusters with different resolutions
                clustering(adata_sub, leiden_res)
                adata_sub.X = adata_sub.layers["raw"]
                print(f"Cluster number with resolution {leiden_res}: ",
                      adata_sub.obs[f"leiden_res{leiden_res}"].astype("int").max() + 1,
                      file=sys.stderr)
                # Extract gene counts
                gene_counts = {}
                for g in all_genes:
                    gene_counts[g] = {grp: pd.DataFrame(adata_sub[adata_sub.obs[f"leiden_res{leiden_res}"].isin([grp]), g].X)[0].mean()
                                      for grp in adata_sub.obs[f"leiden_res{leiden_res}"].unique()}
                gene_counts = pd.DataFrame.from_dict(gene_counts,
                                                     orient="columns").reset_index()
                # plot clusters for checking
                plot_clusters(adata_sub, gene_counts, f"leiden_res{leiden_res}", pdf)
                # Compute L-R coexpression
                coexp[sec] = {"environment": env}
                for i in lr_pair_all.index:
                    g1, g2 = lr_pair_all[["gene1", "gene2"]].loc[i]
                    if len(gene_counts[g1].unique()) > 1 and len(gene_counts[g2].unique()) > 1:
                        if MODE == "Pearson":
                            coexp[sec][f"{g1}|{g2}"] = pearsonr(gene_counts[g1], gene_counts[g2])[0]
                        elif MODE == "Spearman":
                            coexp[sec][f"{g1}|{g2}"] = spearmanr(gene_counts[g1], gene_counts[g2])[0]
                        elif MODE == "MutualInfo":
                            coexp[sec][f"{g1}|{g2}"] = mutual_info_regression(X=gene_counts[[g1]], 
                                                                              y=gene_counts[g2],
                                                                              random_state=42)[0]
                        else:
                            print(f"ERROR: unknown mode {MODE}.", file=sys.stderr)
        pdf.close()
        coexp = pd.DataFrame.from_dict(coexp, orient="index")
        coexp.to_csv(f"{OUT_DIR}spatial_coexpression_on_spotclusters_res{leiden_res}.csv")
        # Do statistical tests
        pvals = {}
        for lr in coexp.columns:
            if lr != "environment":
                x = coexp[lr].loc[coexp["environment"] == ENV_LST[0]].tolist()
                y = coexp[lr].loc[coexp["environment"] == ENV_LST[1]].tolist()
                if x and y:  # to exclude cell types in single environment
                    U1, p = mannwhitneyu(x, y, method="exact", nan_policy="omit")
                    pvals[lr] = p
        pval_res = pd.DataFrame.from_dict(pvals, orient="index", columns=["p.raw"])
        pval_res["p.adj (FDR)"] = fdrcorrection(pval_res["p.raw"], alpha=0.05, method="indep", is_sorted=False)[1]
        pval_res.sort_values(by=["p.adj (FDR)", "p.raw"], inplace=True)
        pval_res.to_csv(f"{OUT_DIR}wilcoxon_FDRs_spatial_coexpression_on_spotclusters_res{leiden_res}.csv")
        # Plot boxes
        coexp = coexp.reset_index().rename(columns={"index": "sample"}).melt(id_vars=["sample", "environment"],
                                                                             var_name="LR pair",
                                                                             value_name="Pearson Corr")
        pdf = mpdf.PdfPages(f"{OUT_DIR}boxplots_spatial_coexp_on_spotclusters_res{leiden_res}_{MODE}.pdf")
        plot_boxes(coexp, x="Pearson Corr", y="LR pair", y_order=pval_res.index, pdf=pdf, 
                   groupby="environment", w=15, h=50)
        pdf.close()
