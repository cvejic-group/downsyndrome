import sys
import scanpy as sc
import pandas as pd
import numpy as np

import matplotlib as mpl
import matplotlib.pyplot as plt
import seaborn as sns


def select_slide(adata, s, s_col='sample'):
    """ 
    Select the data for one slide from the spatial anndata object.

    :param adata: Anndata object with multiple spatial experiments
    :param s: name of selected experiment
    :param s_col: column in adata.obs listing experiment name for each location
    """
    idx2sel = adata.obs[s_col].isin([s])
    slide = adata.copy()
    for layer in slide.layers:
        slide.layers[layer] = slide.layers[layer][idx2sel, :]
    slide = slide[idx2sel, :].copy()
    s_keys = list(slide.uns["spatial"].keys())
    s_spatial = np.array(s_keys)[[s in k for k in s_keys]][0]
    slide.uns['spatial'] = {s_spatial: slide.uns['spatial'][s_spatial]}
    return slide


def pixels_to_matrix(adata, cell_type):
    """
    Create a matrix from adata[["array_row", "array_col", f"{cell_type} in spot"]]
    The matrix contains 3 possible values:
        -1  means empty pixel
         0  means absence of the cell type
         1  means presence of the cell type

    :return: a matrix in which the (i, j) element indicates the given cell type's presence
    """
    if f"{cell_type} in spot" not in adata.obs.columns:
        print("ERROR! adata object should first be parsed by binarize_abundance.",
              file=sys.stderr)
        exit(1)
    nrows = np.max(adata.obs["array_row"]) + 1
    ncols = np.max(adata.obs["array_col"]) + 1
    mat_res = np.ones(shape=(nrows, ncols)) * -1
    for i in range(adata.n_obs):
        mat_res[adata.obs["array_row"].iloc[i],
                adata.obs["array_col"].iloc[i]] = adata.obs[f"{cell_type} in spot"].iloc[i]
    return mat_res


def pixels_to_matrix2(adata, obs_col):
    """
    Create a matrix from adata[["array_row", "array_col", obs_col]]
    Please note that the obs_col should not contain any negative value!
    The matrix contains values:
        -1                means empty pixel
        positive value    means the value in corresponding obs_col component

    :return: a matrix in which the (i, j) element indicates the given value in obs_col
    """
    if obs_col not in adata.obs.columns:
        print(f"ERROR! adata.obs does not contain a {obs_col} column.",
              file=sys.stderr)
        exit(1)
    if adata.obs[obs_col].min() < 0:
        print("ERROR! the obs_col should not contain any negative value.",
              file=sys.stderr)
        exit(1)
    nrows = np.max(adata.obs["array_row"]) + 1
    ncols = np.max(adata.obs["array_col"]) + 1
    mat_res = -np.ones(shape=(nrows, ncols))  # initialise all spots with -1
    for i in range(adata.n_obs):
        mat_res[adata.obs["array_row"].iloc[i],
                adata.obs["array_col"].iloc[i]] = adata.obs[obs_col].iloc[i]
    return mat_res


def get_neighbors(center_i, center_j, i_max, j_max, consider_center=True):
    """
    Compute neighbor pixels' coordinates with a given center

    :param center_i: row coordinate of the given center
    :param center_j: column coordinate of the given center
    :param i_max: maximum row number of the matrix
    :param j_max: maximum column number of the matrix

    :return: a list of neighbor pixels' coordinates
    """
    if center_i < 0 or center_i >= i_max or center_j < 0 or center_j >= j_max:
        print("ERROR! Invalid center coordinates ({center_i}, {center_j})!",
              file=sys.stderr)
        sys.exit(1)
    if consider_center:
        neighbors = [(center_i, center_j)]
    else:
        neighbors = []
    if center_j - 2 >= 0:
        neighbors += [(center_i, center_j - 2)]
    if center_i + 1 < i_max and center_j - 1 >= 0:
        neighbors += [(center_i + 1, center_j - 1)]
    if center_i + 1 < i_max and center_j + 1 < j_max:
        neighbors += [(center_i + 1, center_j + 1)]
    if center_j + 2 < j_max:
        neighbors += [(center_i, center_j + 2)]
    if center_i - 1 >= 0 and center_j + 1 < j_max:
        neighbors += [(center_i - 1, center_j + 1)]
    if center_i - 1 >= 0 and center_j - 1 >= 0:
        neighbors += [(center_i - 1, center_j - 1)]
    return neighbors


def search_links(cell_1, cell_2, consider_center=True):
    """
    Search links between cell_1 and cell_2.
    A link of cell_2 to cell_1 is considered same as that of cell_1 to cell_2.

    :param cell_1: cell matrix of the first cell type
    :param cell_2: cell matrix of the second cell type

    :return: interaction DataFrame with 3 columns:
                - xy_1: Coordinates x_y in cell_1
                - xy_2: Coordiantes x_y in cell_2
                - An extra "drop_by" column for removing double counting
    """
    i_max, j_max = cell_1.shape  # will need to traverse cell_1 matrix
    links = pd.DataFrame(columns=["xy_1", "xy_2", "drop_by"])
    for i in range(i_max):
        for j in range(j_max):
            # do nothing if cell 1 is absent (can be 0 or -1)
            if cell_1[i, j] != 1:
                continue
            for nb in get_neighbors(i, j, i_max, j_max, consider_center):
                if cell_2[nb[0], nb[1]] == 1:
                    xy_1 = str(i) + "_" + str(j)
                    xy_2 = str(nb[0]) + "_" + str(nb[1])

                    if i < nb[0]:
                        drop_by = xy_1 + "_" + xy_2
                    elif i == nb[0] and j <= nb[1]:
                        drop_by = xy_1 + "_" + xy_2
                    else:
                        drop_by = xy_2 + "_" + xy_1

                    links = pd.concat([links, pd.DataFrame([[xy_1, xy_2, drop_by]], columns=links.columns)],
                                      ignore_index=True)
    links.drop_duplicates(subset=["drop_by"], inplace=True)
    return links


def search_surroundings(adata, cell_1, cell_2, ctype_1, ctype_2):
    """
    Search surrounding cells for either cell_1 and cell_2.
    The adata object is modified in place, with two extra columns added:
        - f"ratio {ctype_2} around {ctype_1}"
        - f"ratio {ctype_1} around {ctype_2}"
        - f"any {ctype_2} around {ctype_1}"
        - f"any {ctype_1} around {ctype_2}"
    """
    num_1_around_2 = []
    num_2_around_1 = []
    i_max, j_max = cell_1.shape
    for i in range(adata.obs.shape[0]):
        center_i, center_j = adata.obs["array_row"].iloc[i], adata.obs["array_col"].iloc[i]
        neighbors = get_neighbors(center_i, center_j, i_max, j_max)
        if cell_1[center_i, center_j] == 1:
            num_2_around_1.append(sum([cell_2[x, y] == 1
                                       for x, y in neighbors]))
        else:
            num_2_around_1.append(0)
        if cell_2[center_i, center_j] == 1:
            num_1_around_2.append(sum([cell_1[x, y] == 1
                                       for x, y in neighbors]))
        else:
            num_1_around_2.append(0)
    adata.obs[f"ratio {ctype_2} around {ctype_1}"] = [x / len(neighbors)
                                                      for x in num_2_around_1]
    adata.obs[f"any {ctype_2} around {ctype_1}"] = [1 if x > 0 else 0
                                                    for x in num_2_around_1]
    adata.obs[f"ratio {ctype_1} around {ctype_2}"] = [x / len(neighbors)
                                                      for x in num_1_around_2]
    adata.obs[f"any {ctype_1} around {ctype_2}"] = [1 if x > 0 else 0
                                                    for x in num_1_around_2]


def plot_spatial(adata, pdf, color_by=None, ncols=None):
    """
    Plot VISIUM's spatial results on the given column.
    """
    if color_by == None or any([x not in adata.obs.columns for x in list(color_by)]):
        print("ERROR! Invalide color_by argument:", color_by, file=sys.stderr)
        sys.exit(1)
    if ncols == None and isinstance(color_by, list) and len(color_by) >= 3:
        ncols = 3
    elif ncols == None and isinstance(color_by, list):
        ncols = len(color_by)
    elif ncols == None:
        ncols = 1
    with mpl.rc_context({'figure.figsize': [6, 7], 'axes.facecolor': 'white'}):
        sc.pl.spatial(adata, color=color_by,
                      img_key="hires",
                      cmap="magma",
                      ncols=ncols,
                      library_id=list(adata.uns['spatial'].keys())[0],
                      size=1.3,
                      vmin=0, vmax="p99.2")
        pdf.savefig()
        plt.close()


def plot_bars(df, x, y, y_order, fig_path=None, pdf=None, groupby=None, w=15, h=12, fscale=1.5):
    plt.figure(figsize=(w, h))
    sns.set(font_scale=fscale)
    sns.barplot(y=y, x=x, data=df, order=y_order, hue=groupby,
                errcolor="black", capsize=0.2, ci=None)
    sns.stripplot(y=y, x=x, data=df, order=y_order, hue=groupby, color="black",
                  size=3, jitter=0, dodge=(groupby is not None))
    plt.ylabel(None)
    plt.tight_layout()
    if pdf is not None:
        pdf.savefig()
    elif fig_path is not None:
        plt.savefig(fig_path)
    else:
        print("Error! fig_path and pdf in plot_boxes cannot be None at the same time!",
              file=sys.stderr)
        sys.exit(1)
    plt.close()


def plot_boxes(df, x, y, y_order, fig_path=None, pdf=None, groupby=None, w=15, h=12, fscale=1.5, legend_loc="best"):
    plt.figure(figsize=(w, h))
    sns.set(font_scale=fscale, style="white")
    ax = sns.boxplot(y=y, x=x, data=df, order=y_order, hue=groupby, orient="h")
    sns.stripplot(y=y, x=x, data=df, order=y_order, hue=groupby, color="black",
                  size=3, jitter=0, dodge=(groupby is not None))
    handles, labels = ax.get_legend_handles_labels()
    plt.legend(handles[:2], labels[:2], loc=legend_loc)
    plt.ylabel(None)
    plt.tight_layout()
    if pdf is not None:
        pdf.savefig()
    elif fig_path is not None:
        plt.savefig(fig_path)
    else:
        print("Error! fig_path and pdf in plot_boxes cannot be None at the same time!",
              file=sys.stderr)
        sys.exit(1)
    plt.close()
