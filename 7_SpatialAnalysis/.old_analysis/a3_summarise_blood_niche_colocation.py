import sys
import pandas as pd
import scanpy as sc
import matplotlib.backends.backend_pdf as mpdf

from scipy.stats import mannwhitneyu
from statsmodels.stats.multitest import fdrcorrection

from _functions import plot_boxes


ENV_LST = ["Healthy_Liver", "DownSyndrome_Liver"]
GENE_COUNT_THRESH = 800
CELL_ABD_FIELD = "means_cell_abundance_w_sf"
BLOOD_CT = "HSCs/MPPs"
NICHE_CT_LST = ["Activated stellate cells", "Hepatic stellate cells",  "Hepatocytes",
                "LSECs", "Vascular endothelial cells"]

OUT_DIR = f"/lustre/scratch119/casm/team-cvejic/haoliang/results/DownSyndrome/STx_results_new_{GENE_COUNT_THRESH}/"


def summarise_colocalisation(adata):
    env_ct_lst = [x for x in adata.uns["mod"]["factor_names"] if x != BLOOD_CT]
    # Add colocalised abundance across spots for each section
    colocal_abd = {smp: {f"{BLOOD_CT}|{ct}": 0
                         for ct in env_ct_lst}
                   for smp in adata.obs["sample"].unique()}
    blood_tot = {smp: 0 for smp in adata.obs["sample"].unique()}
    for spot in adata.obs.index:  # all spots across different sections
        smp = adata.obs["sample"].loc[spot]
        blood_abd = adata.obsm[CELL_ABD_FIELD][f"{CELL_ABD_FIELD.replace('_cell', 'cell')}_{BLOOD_CT}"].loc[spot]
        blood_tot[smp] += blood_abd
        env_abd_dict = {ct: adata.obsm[CELL_ABD_FIELD][f"{CELL_ABD_FIELD.replace('_cell', 'cell')}_{ct}"].loc[spot]
                        for ct in env_ct_lst}
        env_abd_sum = sum(env_abd_dict.values())
        for env_ct, env_ct_abd in env_abd_dict.items():
            colocal_abd[smp][f"{BLOOD_CT}|{env_ct}"] += (blood_abd * env_ct_abd / env_abd_sum)
    # compute percentage from abundance
    colocal_pct = pd.DataFrame.from_dict(colocal_abd, orient="index")
    sum_df = colocal_pct.sum(axis=1)  # for check
    for smp in colocal_pct.index:
        colocal_pct.loc[smp, :] *= (100 / blood_tot[smp])
        # check accumulated error by iterations
        if abs(blood_tot[smp] - sum_df[smp]) > 1E-5:
            print("[Warning] difference between row sum and interative sum:",
                  blood_tot[smp], sum_df[smp], file=sys.stderr)
    print("Check computed percent sum - max absolute difference:", (colocal_pct.sum(axis=1) - 100).abs().max(),
          file=sys.stderr)
    colocal_pct = colocal_pct.reset_index().rename(columns={"index": "sample"})
    return colocal_pct


if __name__ == "__main__":
    # Summarise cell type colocolisation table for both environments
    colcl_allsmps = None
    for env in ENV_LST:
        print(env, file=sys.stderr)
        adata = sc.read_h5ad(f"{OUT_DIR}{env}_sp_cleaned.h5ad")
        colocal_pct = summarise_colocalisation(adata).melt(id_vars="sample",
                                                           var_name="colocalisation",
                                                           value_name="percent")
        colocal_pct["environment"] = env
        colcl_allsmps = pd.concat([colcl_allsmps, colocal_pct],
                                  axis=0, ignore_index=True)
    # Print table size for checking
    print("Cell type frequency table size:",
          colcl_allsmps.shape, file=sys.stderr)
    for env in ENV_LST:
        print("\t-", env, colcl_allsmps.loc[colcl_allsmps["environment"] == env, :].shape,
              file=sys.stderr)
    # Wilcoxon rank sum test for cell type frequency per spot
    pvals = {}
    for oc in colcl_allsmps["colocalisation"].unique():
        x = colcl_allsmps.loc[(colcl_allsmps["colocalisation"] == oc) & (colcl_allsmps["environment"] == ENV_LST[0]),
                              "percent"].tolist()
        y = colcl_allsmps.loc[(colcl_allsmps["colocalisation"] == oc) & (colcl_allsmps["environment"] == ENV_LST[1]),
                              "percent"].tolist()
        if x and y:  # to exclude cell types in single environment
            U1, p = mannwhitneyu(x, y, method="exact")
            pvals[oc] = p
    pval_res = pd.DataFrame.from_dict(pvals, orient="index", columns=["p.raw"])
    pval_res["p.adj (FDR)"] = fdrcorrection(pval_res["p.raw"],
                                            alpha=0.05, method="indep", is_sorted=False)[1]
    pval_res.sort_values(by=["p.adj (FDR)", "p.raw"], inplace=True)
    pval_res.to_csv(f"{OUT_DIR}wilcoxon_pvalues_on_{BLOOD_CT.replace('/', '').replace(' ', '_')}_colocalisation.csv")
    # plot blood-niche interactions
    pdf = mpdf.PdfPages(f"{OUT_DIR}boxplots_{BLOOD_CT.replace('/', '').replace(' ', '_')}_colocalisation_by_environment.pdf")
    plot_boxes(df=colcl_allsmps,
               x=f"percent", y="colocalisation",
               y_order=pval_res.index[pval_res.index.isin([f"{BLOOD_CT}|{ct}" for ct in NICHE_CT_LST])],
               pdf=pdf, groupby="environment", w=15, h=4, fscale=1.2, legend_loc="lower right")
    pdf.close()
