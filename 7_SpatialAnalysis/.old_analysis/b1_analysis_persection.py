import os
import sys
import scanpy as sc
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import matplotlib.backends.backend_pdf as mpdf

from scipy.stats import pearsonr
from scipy.stats import spearmanr
from sklearn.feature_selection import mutual_info_regression
from scipy.stats import mannwhitneyu
from statsmodels.stats.multitest import fdrcorrection

from _functions import select_slide
from _functions import plot_boxes

ENV_LST = ["Healthy_Liver", "DownSyndrome_Liver"]
MODE = "Pearson"
ABD_FIELD = "means_cell_abundance_w_sf"

IN_DIR = "/lustre/scratch118/opentargets/opentargets/OTAR2060/nelson/outputs/downsyndrome/cell2location/spatial_model/"
LR_DIR = "/lustre/scratch119/casm/team-cvejic/haoliang/results/DownSyndrome/cellphoneDB_analysis/"
OUT_DIR = "/lustre/scratch119/casm/team-cvejic/haoliang/results/DownSyndrome/coexpression_on_clusters/"

if not os.path.exists(f"{OUT_DIR}celltype_abundancies/"):
    os.makedirs(f"{OUT_DIR}celltype_abundancies/")

LR_LST = [("VEGFB", "FLT1"), ("ANGPT1", "TEK"), ("ANGPT2", "TEK"),
          ("NOTCH1", "JAG1"), ("KIT", "KITLG"), ("EFNA4", "EPHA2"),
          ("EFNA4", "EPHA4"), ("TFRC", "TF"), ("TFR2", "TF"),
          ("TNFRSF1A", "GRN"), ("TNFRSF1B", "GRN"), ("CD74", "MIF")]


def load_ligands_receptors():
    lr_pair_all = None
    for env in ENV_LST:
        lr_pair = pd.read_table(f"{LR_DIR}data/{env}.processed_output.txt")
        lr_pair["gene1"] = lr_pair[["Ligand", "Receptor"]].min(axis=1)
        lr_pair["gene2"] = lr_pair[["Ligand", "Receptor"]].max(axis=1)
        lr_pair = lr_pair.loc[lr_pair["Sender"].isin(["HSCs/MPPs"]) | lr_pair["Receiver"].isin(["HSCs/MPPs"]),
                              ["gene1", "gene2"]].drop_duplicates()
        lr_pair[f"found in {env}"] = True
        lr_pair.set_index(["gene1", "gene2"], inplace=True)
        lr_pair_all = pd.concat([lr_pair_all, lr_pair],
                                axis=1, join="outer").fillna(False)
    lr_pair_all.reset_index(inplace=True)
    return lr_pair_all


def transform_counts(adata2trans):
    adata2trans.layers["raw"] = adata2trans.X.todense().copy()
    # Analytic Pearson residuals
    sc.experimental.pp.normalize_pearson_residuals(adata2trans)
    adata2trans.layers["APR"] = adata2trans.X.copy()
    return adata2trans


def clustering(adata2clustering, r=1):
    # HVG detection
    sc.pp.highly_variable_genes(adata2clustering, batch_key="sample",
                                min_mean=0.00125, max_mean=3, min_disp=0.5)
    # PCA
    sc.tl.pca(adata2clustering, n_comps=50, svd_solver="auto",
              use_highly_variable=True, random_state=42)
    # Compute neighborhood graph and do clustering
    sc.pp.neighbors(adata2clustering, n_pcs=adata2clustering.obsm["X_pca"].shape[1],
                    use_rep="X_pca", knn=True, random_state=42,
                    method="umap", metric="euclidean")
    sc.tl.leiden(adata2clustering, resolution=r, random_state=42,
                 key_added=f"leiden_res{r}")
    sc.tl.umap(adata2clustering, random_state=42, n_components=2,
               init_pos="random")
    return adata2clustering


def plot_clusters(adata, gene_counts, cluster_col, pdf):
    sns.set(rc={"figure.figsize": (9, 9)})
    sns.set_theme(style="white", font_scale=2)
    _, axes = plt.subplots(1, 2, figsize=(15, 6))
    sc.pl.umap(adata, color=cluster_col, size=50, palette="Spectral",
               na_in_legend=False, components="1,2", use_raw=False,
               legend_loc="on data", ax=axes[0], show=False, title="")
    sc.pl.spatial(adata, color=cluster_col, palette="Spectral",
                  img_key="hires", library_id=list(adata.uns['spatial'].keys())[0],
                  size=1.3, legend_loc=None, ax=axes[1], title="", show=False)
    plt.suptitle(adata.obs["sample"][0], y=0.9, fontsize="medium")
    plt.tight_layout()
    pdf.savefig()
    plt.close()
    _, axes = plt.subplots(nrows=2, ncols=6, figsize=(54, 18))
    for iplot, lr in enumerate(LR_LST):
        sns.scatterplot(data=gene_counts, x=lr[0], y=lr[1], hue="index",
                        s=200, palette="Spectral", legend=None,
                        ax=axes[int(iplot / 6), iplot % 6])
        for i in gene_counts.index:
            axes[int(iplot / 6), iplot % 6].annotate(xy=(gene_counts.loc[i, lr[0]],
                                                         gene_counts.loc[i, lr[1]]),
                                                     text=gene_counts.loc[i, "index"])
        corr_p = np.round(pearsonr(gene_counts[lr[0]],
                                   gene_counts[lr[1]])[0], 2)
        corr_s = np.round(spearmanr(gene_counts[lr[0]],
                                    gene_counts[lr[1]])[0], 2)
        mi = np.round(mutual_info_regression(X=gene_counts[[lr[0]]],
                                             y=gene_counts[lr[1]],
                                             random_state=42)[0], 2)
        axes[int(iplot / 6), iplot % 6].text(x=max(gene_counts[lr[0]]), y=min(gene_counts[lr[1]]),
                                             s=f"Pearson Corr.: {corr_p}\nSpearman Corr.: {corr_s}\nMI: {mi}",
                                             va="bottom", ha="right")
    plt.tight_layout()
    pdf.savefig()
    plt.close()


def plot_celltype_compos(adata2plot, title, pdf):
    ctype_abd = adata2plot.obsm[ABD_FIELD].rename(columns={col: col.replace(f"{ABD_FIELD.replace('_cell', 'cell')}_", "")
                                                           for col in adata2plot.obsm[ABD_FIELD].columns})
    ctype_abd = pd.concat([adata2plot.obs[f"leiden_res{r}"], ctype_abd],
                          axis=1, join="outer")
    ctype_pct_cluster = ctype_abd.groupby(by=f"leiden_res{r}").sum()
    percluster_sum = ctype_pct_cluster.sum(axis=1)
    for col in ctype_pct_cluster.columns:
        ctype_pct_cluster[col] *= (100 / percluster_sum)
    print(f"\tchecking cell type percentage = {(ctype_pct_cluster.sum(axis=1) - 100).abs().max()}.",
          file=sys.stderr)
    g = sns.clustermap(data=ctype_pct_cluster.T, cmap=sns.light_palette("brown", as_cmap=True),
                       figsize=(1.5 * ctype_pct_cluster.shape[0], 20), dendrogram_ratio=(0.2, 0), linewidth=1,
                       col_cluster=False, standard_scale=0,
                       annot=ctype_pct_cluster.T, cbar_pos=None)
    for tick_label in g.ax_heatmap.axes.get_yticklabels():
        tick_text = tick_label.get_text()
        if tick_text == "HSCs/MPPs":
            tick_label.set_color("red")
        elif tick_text in ["Hepatocytes", "Vascular endothelial cells", "LSECs",
                           "Hepatic stellate cells", "Activated stellate cells"]:
            tick_label.set_color("blue")
        else:
            tick_label.set_color("black")
    plt.title(title)
    pdf.savefig()
    plt.tight_layout()
    plt.close()
    return ctype_abd
    #sns.set_theme(style="white", font_scale=2)
    #_, axes = plt.subplots(1, 2, figsize=(15, 6))
    # sc.pl.umap(adata2plot, color=f"leiden_res{r}", size=50, palette="Spectral",
    #           na_in_legend=False, components="1,2", use_raw=False,
    #           groups=list(ctype_pct_cluster.loc[ctype_pct_cluster["HSCs/MPPs"] >= 4, :].index),
    #           legend_loc="on data", ax=axes[0], show=False, title="")
    # sc.pl.spatial(adata2plot, color=f"leiden_res{r}", palette="Spectral",
    #              groups=list(ctype_pct_cluster.loc[ctype_pct_cluster["HSCs/MPPs"] >= 4, :].index),
    #              img_key="hires", library_id=list(adata2plot.uns['spatial'].keys())[0],
    #              size=1.2, legend_loc=None, ax=axes[1], title="", show=False)
    # plt.tight_layout()
    # pdf.savefig()
    # plt.close()


def compute_section_coexpression(gene_counts, gene_pairs):
    coexp_sec = {}
    for i in gene_pairs.index:
        g1, g2 = gene_pairs.loc[i, :]
        if len(gene_counts[g1].unique()) > 1 and len(gene_counts[g2].unique()) > 1:
            if MODE == "Pearson":
                coexp_sec[f"{g1}|{g2}"] = pearsonr(gene_counts[g1],
                                                   gene_counts[g2])[0]
            elif MODE == "Spearman":
                coexp_sec[f"{g1}|{g2}"] = spearmanr(gene_counts[g1],
                                                    gene_counts[g2])[0]
            elif MODE == "MutualInfo":
                coexp_sec[f"{g1}|{g2}"] = mutual_info_regression(X=gene_counts[[g1]],
                                                                 y=gene_counts[g2],
                                                                 random_state=42)[0]
            else:
                print(f"ERROR: unknown mode {MODE}.", file=sys.stderr)
    return coexp_sec


def test_coexpression(coexp):
    pvals = {}
    for lr in coexp.columns:
        if lr != "environment":
            x = coexp[lr].loc[coexp["environment"] == ENV_LST[0]].tolist()
            y = coexp[lr].loc[coexp["environment"] == ENV_LST[1]].tolist()
            if x and y:  # to exclude cell types in single environment
                _, p = mannwhitneyu(x, y,
                                    method="exact", nan_policy="omit")
                pvals[lr] = p
    pval_res = pd.DataFrame.from_dict(pvals,
                                      orient="index", columns=["p.raw"])
    pval_res["p.adj (FDR)"] = fdrcorrection(pval_res["p.raw"], alpha=0.05,
                                            method="indep", is_sorted=False)[1]
    pval_res.sort_values(by=["p.adj (FDR)", "p.raw"], inplace=True)
    return pval_res


if __name__ == "__main__":
    # Load and make all LR list
    lr_pair_all = load_ligands_receptors()
    lr_pair_all.to_csv(f"{LR_DIR}summarised_LRs_HSCsMPPs.csv")
    all_genes = list(set(lr_pair_all[["gene1", "gene2"]].stack().values))
    # Estimate L-R coexpression by section and plot clusters to check
    for r in [1, 2, 5, 10]:
        coexp = {}
        pdf1 = mpdf.PdfPages(f"{OUT_DIR}per_section_plots_LR_res{r}.pdf")
        pdf2 = mpdf.PdfPages(
            f"{OUT_DIR}per_section_plots_cell_types_res{r}.pdf")
        for env in ENV_LST:
            adata = sc.read_h5ad(f"{IN_DIR}{env}/sp.h5ad")
            for sec in adata.obs["sample"].unique():
                # select the section and transform counts
                adata_sub = select_slide(adata, sec, "sample")
                transform_counts(adata_sub)
                # Identify clusters with different resolutions
                clustering(adata_sub, r)
                adata_sub.X = adata_sub.layers["raw"]
                print(f"{env} {sec}: cluster number with resolution {r} = ", adata_sub.obs[f"leiden_res{r}"].astype("int").max() + 1,
                      file=sys.stderr)
                # Extract gene counts
                gene_counts = {}
                for g in all_genes:
                    gene_counts[g] = {grp: pd.DataFrame(adata_sub[adata_sub.obs[f"leiden_res{r}"].isin([grp]), g].X)[0].mean()
                                      for grp in adata_sub.obs[f"leiden_res{r}"].unique()}
                gene_counts = pd.DataFrame.from_dict(gene_counts,
                                                     orient="columns").reset_index()
                # Plot clusters
                plot_clusters(adata_sub, gene_counts, f"leiden_res{r}", pdf1)
                # Cell type composition
                if r <= 2:
                    plot_celltype_compos(adata_sub,
                                         f"{env}:{sec}",
                                         pdf2).to_csv(f"{OUT_DIR}celltype_abundancies/{sec}_res{r}.csv")
                # Compute L-R coexpression
                coexp[sec] = compute_section_coexpression(gene_counts,
                                                          lr_pair_all[["gene1", "gene2"]])
                coexp[sec]["environment"] = env
        pdf1.close()
        pdf2.close()
        # Summarise co-expression
        coexp = pd.DataFrame.from_dict(coexp, orient="index")
        coexp.to_csv(f"{OUT_DIR}spatial_coexp_res{r}.csv")
        # Test co-expression
        pval_res = test_coexpression(coexp)
        pval_res.to_csv(f"{OUT_DIR}FDRs_spatial_coexp_res{r}.csv")
        pdf = mpdf.PdfPages(
            f"{OUT_DIR}boxplots_spatial_coexp_res{r}_{MODE}.pdf")
        # Plot boxes
        coexp = coexp.reset_index().rename(columns={"index": "sample"}).melt(id_vars=["sample", "environment"],
                                                                             var_name="LR pair",
                                                                             value_name="Pearson Corr")
        plot_boxes(coexp, x="Pearson Corr", y="LR pair", y_order=pval_res.index, pdf=pdf,
                   groupby="environment", w=15, h=50)
        pdf.close()
