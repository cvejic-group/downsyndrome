import os
import sys
import pandas as pd
import scanpy as sc
import matplotlib.backends.backend_pdf as mpdf

from scipy.stats import mannwhitneyu
from statsmodels.stats.multitest import fdrcorrection

from _functions import select_slide
from _functions import plot_spatial


ENV_LST = ["Healthy_Liver", "DownSyndrome_Liver"]
GENE_COUNT_THRESH = 800
CELL_ABD_FIELD = "means_cell_abundance_w_sf"

OUT_DIR = f"/lustre/scratch119/casm/team-cvejic/haoliang/results/DownSyndrome/STx_results_new_{GENE_COUNT_THRESH}/"


def plot_sample_spatial(adata, pdf):
    all_cts = adata.uns["mod"]["factor_names"]
    adata.obs[[f"{ct} mean abundance" 
               for ct in all_cts]] = adata.obsm[CELL_ABD_FIELD][[f"{CELL_ABD_FIELD.replace('_cell', 'cell')}_{ct}"
                                                                 for ct in all_cts]]
    for smp in adata.obs["sample"].unique():
        adata_sub = select_slide(adata, smp, "sample")
        plot_spatial(adata_sub, pdf, color_by=[f"{ct} mean abundance" for ct in all_cts], ncols=4)


if __name__ == "__main__":
    pdf = mpdf.PdfPages(f"{OUT_DIR}spatialplots_celltype_abd.pdf")
    for env in ENV_LST:
        print(env, file=sys.stderr)
        adata = sc.read_h5ad(f"{OUT_DIR}{env}_sp_cleaned.h5ad")
        plot_sample_spatial(adata, pdf)
    pdf.close()
