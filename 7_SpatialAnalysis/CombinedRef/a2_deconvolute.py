# The script is formed referring to https://cell2location.readthedocs.io/en/latest/notebooks/cell2location_tutorial.html
import gc
import os
import sys
import anndata
import scanpy as sc
import numpy as np
import matplotlib.pyplot as plt
import scvi
import cell2location

from matplotlib import rcParams
rcParams["pdf.fonttype"] = 42 # enables correct plotting of text for PDFs

scvi.settings.seed = 0

WK_DIR = "/lustre/scratch126/casm/team-cvejic/haoliang/pj_DownSyndrome/results/"
VISIUM_DATA_FOLDER = "/lustre/scratch126/casm/team-cvejic/data/SpaceRanger_Ts21_Ctrl/"
CTYPE2RM = {"t21": ["X", "Cholangiocytes"],
            "disomy": ["Cholangiocytes", "38,0", "31,0", "Erythroid cells,2,1", "34",
                       "Megakaryocytes,2,0", "36,1", "36,0,0", "36,0,1", "38,1", "Megakaryocytes,2,1"],
            "public": []}
DS2NAME = {"t21": "10X_DownSyndrome_Liver.h5ad", "disomy": "10X_Healthy_Liver.h5ad",
           "t21_1": "subset_data/DownSyndrome.subset1.h5ad", "t21_2": "subset_data/DownSyndrome.subset2.h5ad",
           "disomy_1": "subset_data/Healthy.subset1.h5ad", "disomy_2": "subset_data/Healthy.subset2.h5ad",
           "combined_1": "subset_data/combined.subset1.h5ad", "combined_2": "subset_data/combined.subset2.h5ad",
           "public": "pub_atlas/Foetal_Liver_Atlas.h5ad"}
DS2FILES = {"t21": ["spaceranger122_count_36337_OT_F_Visium9720395_GRCh38-3_0_0", "spaceranger122_count_36961_OT_F_Visium9998361_GRCh38-3_0_0",
                    "spaceranger122_count_38823_OT_F_Visium9880863_GRCh38-3_0_0", "spaceranger122_count_38825_OT_F_Visium9880673_GRCh38-3_0_0", 
                    "spaceranger122_count_36337_OT_F_Visium9720396_GRCh38-3_0_0", "spaceranger122_count_38822_OT_F_Visium9880766_GRCh38-3_0_0",
                    "spaceranger122_count_38824_OT_F_Visium9880576_GRCh38-3_0_0", "spaceranger122_count_39397_OT_F_Visium10405238_GRCh38-3_0_0", 
                    "spaceranger122_count_36338_OT_F_Visium9720491_GRCh38-3_0_0", "spaceranger122_count_38822_OT_F_Visium9880767_GRCh38-3_0_0", 
                    "spaceranger122_count_38824_OT_F_Visium9880577_GRCh38-3_0_0", "spaceranger122_count_39397_OT_F_Visium10405239_GRCh39-3_0_0",
                    "spaceranger122_count_36338_OT_F_Visium9720492_GRCh38-3_0_0", "spaceranger122_count_38822_OT_F_Visium9880768_GRCh38-3_0_0", 
                    "spaceranger122_count_38825_OT_F_Visium9880670_GRCh38-3_0_0", "spaceranger122_count_39397_OT_F_Visium10405240_GRCh38-3_0_0",
                    "spaceranger122_count_36644_OT_F_Visium9879893_GRCh38-3_0_0", "spaceranger122_count_38822_OT_F_Visium9880769_GRCh38-3_0_0",
                    "spaceranger122_count_38825_OT_F_Visium9880671_GRCh38-3_0_0", "spaceranger122_count_39397_OT_F_Visium10405241_GRCh38-3_0_0", 
                    "spaceranger122_count_36644_OT_F_Visium9879894_GRCh38-3_0_0", "spaceranger122_count_38823_OT_F_Visium9880862_GRCh38-3_0_0",
                    "spaceranger122_count_38825_OT_F_Visium9880672_GRCh38-3_0_0"],
            "disomy": ["spaceranger122_count_36337_OT_F_Visium9720393_GRCh38-3_0_0", "spaceranger122_count_36338_OT_F_Visium9720490_GRCh38-3_0_0",
                       "spaceranger122_count_36961_OT_F_Visium9998362_GRCh38-3_0_0", "spaceranger122_count_38823_OT_F_Visium9880865_GRCh38-3_0_0",
                       "spaceranger122_count_36337_OT_F_Visium9720394_GRCh38-3_0_0", "spaceranger122_count_36644_OT_F_Visium9879895_GRCh38-3_0_0",
                       "spaceranger122_count_36961_OT_F_Visium9998363_GRCh38-3_0_0", "spaceranger122_count_36338_OT_F_Visium9720489_GRCh38-3_0_0",
                       "spaceranger122_count_36644_OT_F_Visium9879896_GRCh38-3_0_0", "spaceranger122_count_38823_OT_F_Visium9880864_GRCh38-3_0_0"]}


def load_process_visium_data(h5ad_name):
    adata_vis = sc.read_visium(f"{VISIUM_DATA_FOLDER}{h5ad_name}",
                               count_file="filtered_feature_bc_matrix.h5", load_images=True)
    adata_vis.obs["sample"] = list(adata_vis.uns["spatial"].keys())[0]
    adata_vis.var["SYMBOL"] = adata_vis.var_names
    adata_vis.var.set_index("gene_ids", drop=True, inplace=True, verify_integrity=True)
    # find mitochondria-encoded (MT) genes
    adata_vis.var["MT_gene"] = [gene.startswith("MT-") for gene in adata_vis.var["SYMBOL"]]
    # remove MT genes for spatial mapping (keeping their counts in the object)
    adata_vis.obsm["MT"] = adata_vis[:, adata_vis.var["MT_gene"].values].X.toarray()
    adata_vis = adata_vis[:, ~adata_vis.var["MT_gene"].values]
    return adata_vis

def load_process_scRNAseq_data(dataset):
    adata_ref = sc.read_h5ad(f"{WK_DIR}annotated_data/{DS2NAME[dataset]}")
    if dataset in ["t21", "disomy"]:
        # extract the latest annotation
        a = [col[8:] for col in adata_ref.obs.columns if "leiden_v" in col]
        max_num = max([int(x) for x in a if x.isdigit()])
        col_name = f"leiden_v{str(max_num)}"
        print(f"* Using {col_name} as the latest clustering column.", file=sys.stderr)
        adata_ref.obs.rename(columns={col_name: "latest_annot"}, inplace=True)
        adata_ref.obs.drop([x for x in adata_ref.obs.columns if "leiden" in x or "atac" in x],
                        axis=1, inplace=True)
        # drop the cell types to remove
        adata_ref = adata_ref[~adata_ref.obs["latest_annot"].isin(CTYPE2RM[dataset]), :].copy()
        # add the disease state as dataset to separate same cell types in different data sets
        adata_ref.obs["latest_annot"] = adata_ref.obs["latest_annot"].astype("str") + f"-{dataset}"
        # follow the processing indicated in tutorial
        adata_ref.var["SYMBOL"] = adata_ref.var.index
        adata_ref.var.set_index("Ensembl", drop=True, inplace=True, verify_integrity=True)
        # delete unnecessary raw slot (to be removed in a future version of the tutorial)
        del adata_ref.raw
        gc.collect()
    return adata_ref

def train_ref_model(adata2tr, out_folder, max_epochs=250):
    # permissive genes selection as recommanded
    selected = cell2location.utils.filtering.filter_genes(adata2tr, cell_count_cutoff=5,
                                                          cell_percentage_cutoff2=0.03, nonz_mean_cutoff=1.12)
    adata2tr = adata2tr[:, selected].copy()
    # prepare anndata for the regression model
    cell2location.models.RegressionModel.setup_anndata(adata=adata2tr,
                                                       batch_key="patient_sample",
                                                       labels_key="latest_annot")
    mod = cell2location.models.RegressionModel(adata2tr)
    mod.train(max_epochs=max_epochs, use_gpu=True)
    # ELBO loss history during training, removing first 20 epochs from the plot
    _, ax = plt.subplots(1, 1)
    mod.plot_history(20, ax=ax)
    plt.savefig(f"{out_folder}ELBO_loss_history.png", facecolor="white", bbox_inches="tight")
    plt.close()
    # export the estimated cell abundance (summary of the posterior distribution).
    adata2tr = mod.export_posterior(adata2tr,
                                    sample_kwargs={"num_samples": 1000, "batch_size": 2500, "use_gpu": True})
    # save model
    mod.save(out_folder, overwrite=True)
    # save anndata object with results
    if "batch" in adata2tr.obs.columns:
        del adata2tr.obs["batch"]
    adata2tr.write(f"{out_folder}sc.h5ad")

def spatial_deconvolution(adata2dec, out_folder, detect_alpha=20):
    # reload model and h5ad
    adata_ref = sc.read_h5ad(f"{ref_run_name}sc.h5ad")
    mod = cell2location.models.RegressionModel.load(ref_run_name, adata_ref)
    # export estimated expression in each cluster
    if "means_per_cluster_mu_fg" in adata_ref.varm.keys():
        inf_aver = adata_ref.varm["means_per_cluster_mu_fg"][[f"means_per_cluster_mu_fg_{i}"
                                                              for i in adata_ref.uns["mod"]["factor_names"]]].copy()
    else:
        inf_aver = adata_ref.var[[f"means_per_cluster_mu_fg_{i}" for i in adata_ref.uns["mod"]["factor_names"]]].copy()
    inf_aver.columns = adata_ref.uns["mod"]["factor_names"]
    # find shared genes and subset both anndata and reference signatures
    intersect = np.intersect1d(adata2dec.var_names, inf_aver.index)
    adata2dec = adata2dec[:, intersect].copy()
    inf_aver = inf_aver.loc[intersect, :].copy()
    # prepare anndata for cell2location model
    cell2location.models.Cell2location.setup_anndata(adata=adata2dec, batch_key="sample")
    # create and train the model
    mod = cell2location.models.Cell2location(adata2dec, cell_state_df=inf_aver,
                                             N_cells_per_location=10, detection_alpha=detect_alpha)
    mod.train(max_epochs=30000, batch_size=None, train_size=1, use_gpu=True)
    # plot ELBO loss history during training, removing first 1000 epochs from the plot
    _, ax = plt.subplots(1, 1)
    mod.plot_history(1000, ax=ax)
    plt.savefig(f"{out_folder}ELBO_loss_history.png", facecolor="white", bbox_inches="tight")
    plt.close()
    # export the estimated cell abundance (summary of the posterior distribution).
    adata2dec = mod.export_posterior(adata2dec,
                                     sample_kwargs={"num_samples": 1000, "batch_size": mod.adata.n_obs, "use_gpu": True})
    # save model
    mod.save(out_folder, overwrite=True)
    # save anndata object with results
    adata2dec.write(f"{out_folder}sp.h5ad")


if __name__ == "__main__":
    assert len(sys.argv) == 3
    ref_mode, dec_mode = sys.argv[1:3]
    assert (ref_mode in list(DS2NAME.keys()) + ["combined"]) and (dec_mode in ["disomy", "t21", "combined"])

    # Make output folder
    res_folder = f"{WK_DIR}STx_compare_combinedVSseparate_newPip/{ref_mode}_to_{dec_mode}/"
    ref_run_name = f"{res_folder}reference_signatures/"
    run_name = f"{res_folder}cell2location_map/"
    if not os.path.exists(res_folder):
        os.makedirs(ref_run_name)
        os.makedirs(run_name)
        print(f"Output folder {ref_run_name} and {run_name} created...", file=sys.stderr)
    
    # Build reference by mode
    if ref_mode == "combined":
        print("* Reference by combined annotations.", file=sys.stderr)
        adata_ref_comb = anndata.concat([load_process_scRNAseq_data("t21"), load_process_scRNAseq_data("disomy")],
                                        axis=0, join="inner", label="dataset", keys=["t21", "disomy"])
        gc.collect()
        print(f"* Combined scRNA-seq anndata:\n", adata_ref_comb, "\n", adata_ref_comb.X[adata_ref_comb.X > 5], file=sys.stderr)
        train_ref_model(adata_ref_comb, ref_run_name, 500)
    else:
        print(f"* Reference with separate annotations: {ref_mode}", file=sys.stderr)
        adata_ref = load_process_scRNAseq_data(ref_mode)
        print(f"* Reference by {ref_mode} scRNA-seq anndata:\n", adata_ref, "\n", adata_ref.X[adata_ref.X > 5], file=sys.stderr)
        train_ref_model(adata_ref, ref_run_name, 250)

    # Build visium data from deconvolution by mode
    if dec_mode == "combined":
        adata_vis_comb = anndata.concat([load_process_visium_data(fname) for fname in DS2FILES["t21"] + DS2FILES["disomy"]],
                                        label="batch", uns_merge="unique",
                                        keys=DS2FILES["t21"] + DS2FILES["disomy"], index_unique="-")
        del adata_vis_comb.obs["batch"]
        print(f"* To deconvolute combined visium anndata:\n", adata_vis_comb, "\n", adata_vis_comb.X[adata_vis_comb.X > 5], file=sys.stderr)
        spatial_deconvolution(adata_vis_comb, run_name, 20)
    else:
        adata_vis = anndata.concat([load_process_visium_data(fname) for fname in DS2FILES[dec_mode]],
                                   label="batch", uns_merge="unique", keys=DS2FILES[dec_mode], index_unique="-")
        del adata_vis.obs["batch"]
        print(f"* To deconvolute {dec_mode} visium anndata:\n", adata_vis, "\n", adata_vis.X[adata_vis.X > 5], file=sys.stderr)
        spatial_deconvolution(adata_vis, run_name, 20)
