import sys
import scanpy as sc
import matplotlib.pyplot as plt
import seaborn as sns

from scipy.spatial.distance import pdist
from scipy.cluster.hierarchy import linkage, dendrogram


WKDIR = "/lustre/scratch126/casm/team-cvejic/haoliang/pj_DownSyndrome/results/STx_compare_combinedVSseparate_newPip/"
ABD_FIELD = "means_cell_abundance_w_sf"
GENE_COUNT_THRESH = 800
METHOD, METRIC = "ward", "correlation"
T21_SECTIONS = ["spaceranger122_count_36337_OT_F_Visium9720395_GRCh38-3_0_0", "spaceranger122_count_36961_OT_F_Visium9998361_GRCh38-3_0_0",
                "spaceranger122_count_38823_OT_F_Visium9880863_GRCh38-3_0_0", "spaceranger122_count_38825_OT_F_Visium9880673_GRCh38-3_0_0", 
                "spaceranger122_count_36337_OT_F_Visium9720396_GRCh38-3_0_0", "spaceranger122_count_38822_OT_F_Visium9880766_GRCh38-3_0_0",
                "spaceranger122_count_38824_OT_F_Visium9880576_GRCh38-3_0_0", "spaceranger122_count_39397_OT_F_Visium10405238_GRCh38-3_0_0", 
                "spaceranger122_count_36338_OT_F_Visium9720491_GRCh38-3_0_0", "spaceranger122_count_38822_OT_F_Visium9880767_GRCh38-3_0_0", 
                "spaceranger122_count_38824_OT_F_Visium9880577_GRCh38-3_0_0", "spaceranger122_count_39397_OT_F_Visium10405239_GRCh39-3_0_0",
                "spaceranger122_count_36338_OT_F_Visium9720492_GRCh38-3_0_0", "spaceranger122_count_38822_OT_F_Visium9880768_GRCh38-3_0_0", 
                "spaceranger122_count_38825_OT_F_Visium9880670_GRCh38-3_0_0", "spaceranger122_count_39397_OT_F_Visium10405240_GRCh38-3_0_0",
                "spaceranger122_count_36644_OT_F_Visium9879893_GRCh38-3_0_0", "spaceranger122_count_38822_OT_F_Visium9880769_GRCh38-3_0_0",
                "spaceranger122_count_38825_OT_F_Visium9880671_GRCh38-3_0_0", "spaceranger122_count_39397_OT_F_Visium10405241_GRCh38-3_0_0", 
                "spaceranger122_count_36644_OT_F_Visium9879894_GRCh38-3_0_0", "spaceranger122_count_38823_OT_F_Visium9880862_GRCh38-3_0_0",
                "spaceranger122_count_38825_OT_F_Visium9880672_GRCh38-3_0_0"]
CTRL_SECTIONS = ["spaceranger122_count_36337_OT_F_Visium9720393_GRCh38-3_0_0", "spaceranger122_count_36338_OT_F_Visium9720490_GRCh38-3_0_0",
                 "spaceranger122_count_36961_OT_F_Visium9998362_GRCh38-3_0_0", "spaceranger122_count_38823_OT_F_Visium9880865_GRCh38-3_0_0",
                 "spaceranger122_count_36337_OT_F_Visium9720394_GRCh38-3_0_0", "spaceranger122_count_36644_OT_F_Visium9879895_GRCh38-3_0_0",
                 "spaceranger122_count_36961_OT_F_Visium9998363_GRCh38-3_0_0", "spaceranger122_count_36338_OT_F_Visium9720489_GRCh38-3_0_0",
                 "spaceranger122_count_36644_OT_F_Visium9879896_GRCh38-3_0_0", "spaceranger122_count_38823_OT_F_Visium9880864_GRCh38-3_0_0"]


def plot_dendro(df, metric, method, ax, title):
    dist = pdist(df.T, metric=metric)
    dendrogram(linkage(dist, method=method, metric=metric),
               labels=df.columns, orientation="left",
               leaf_font_size=12, ax=ax)
    xlbls = ax.get_ymajorticklabels()
    for lbl in xlbls:
        if lbl.get_text() in ["HSCs/MPPs", "Cycling HSCs/MPPs"]:
            lbl.set_color("red")
        elif lbl.get_text() in ["Hepatocytes", "Hepatic stellate cells", "Activated stellate cells", "LSECs", "Vascular endothelial cells"]:
            lbl.set_color("blue")
        else:
            lbl.set_color("black")
    ax.spines.right.set_visible(False)
    ax.spines.top.set_visible(False)
    ax.spines.left.set_visible(False)
    ax.spines.bottom.set_visible(True)
    ax.tick_params(left=False, right=False, labelleft=False,
                   labelbottom=True, bottom=True)
    ax.set_title(title, horizontalalignment="left")


if __name__ == "__main__":
    case_lst = sys.argv[1:]
    assert len(case_lst) == 2
    mode = "_and_".join(case_lst)
    assert case_lst[0].endswith("to_disomy") and case_lst[1].endswith("to_t21")
    print(f"* Analysing {case_lst}.", file=sys.stderr)

    adata_lst = [sc.read_h5ad(f"{WKDIR}{case}/analysis_res/sp_cleaned.h5ad") for case in case_lst]
    print(adata_lst[0], file=sys.stderr)
    print(adata_lst[1], file=sys.stderr)

    # Plot dendrograms
    sns.set_theme(font_scale=1.5, style="white")
    _, axes = plt.subplots(1, 2, figsize=(15, 6))
    # Load cleaned data, transform counts, and harmonisation
    for i, adata in enumerate(adata_lst):
        all_ctypes = [col.replace(" abd", "") for col in adata.obs.columns if " abd" in col]
        ctype_pct_spot = adata.obs[[f"{x} abd" for x in all_ctypes]].copy()
        ctype_pct_spot.rename(columns={x: x.replace(" abd", "") for x in ctype_pct_spot.columns}, inplace=True)
        perspot_sum = ctype_pct_spot.sum(axis=1)
        for col in ctype_pct_spot.columns:
            ctype_pct_spot[col] *= (100 / perspot_sum)
        # Plot dendrogram for cell type colocalisation
        plot_dendro(ctype_pct_spot, METRIC, METHOD, axes[i], case_lst[i])
        del ctype_pct_spot
    plt.tight_layout()
    plt.savefig(f"{WKDIR}celltype_colocalisation.{mode}.pdf", facecolor="white", bbox_inches="tight")
