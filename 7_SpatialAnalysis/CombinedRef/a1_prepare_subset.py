import gc
import sys
import scanpy as sc
import anndata
import pandas as pd

WKDIR = "/lustre/scratch126/casm/team-cvejic/haoliang/pj_DownSyndrome/results/annotated_data/"
SMP_SET_DICT = {"Healthy": ["15633 L15633R", "15633 L15633B", "15781 L15781A", "15657 L15657A"],
                "DownSyndrome": ["15636 L15636Q", "15724 15724C", "15636 L15636D", "15667 15667F",
                                 "15669 15669H", "15619 15619A", "15636 L15636C", "15582 15582E",
                                 "15646 15646B", "15582 15582F", "15593 L15593F", "15593 L15593E",
                                 "15667 15667E", "15646 15646A", "15636 L15636B", "15619 15619C",
                                 "15559 T21 15559 C", "15656 15656D", "15559 T21 15559 B", 
                                 "15532 T21 15532 C", "15582 T21 15582 B", "15619 15619B",
                                 "15734 L15734A", "15562 T21 15562 B"]}
SMP_SET_DICT["combined"] = SMP_SET_DICT["Healthy"] + SMP_SET_DICT["DownSyndrome"]
CTYPE2RM = {"DownSyndrome": ["X", "Cholangiocytes"],
            "Healthy": ["Cholangiocytes", "38,0", "31,0", "Erythroid cells,2,1", "34",
                        "Megakaryocytes,2,0", "36,1", "36,0,0", "36,0,1", "38,1", "Megakaryocytes,2,1"]}

if __name__ == "__main__":
    # Load and preprocessing of disomy and t21 datasets
    adata_lst = {dset: sc.read_h5ad(f"{WKDIR}10X_{dset}_Liver.h5ad") for dset in ["Healthy", "DownSyndrome"]}
    for dset in adata_lst.keys():
        print("=====>", dset, file=sys.stderr)
        # extract the latest annotation
        a = [col[8:] for col in adata_lst[dset].obs.columns if "leiden_v" in col]
        max_num = max([int(x) for x in a if x.isdigit()])
        col_name = f"leiden_v{str(max_num)}"
        print(f"Using {col_name} as the latest clustering column.", file=sys.stderr)
        adata_lst[dset].obs.rename(columns={col_name: "latest_annot"}, inplace=True)
        adata_lst[dset].obs.drop([x for x in adata_lst[dset].obs.columns if "leiden" in x or "atac" in x],
                                 axis=1, inplace=True)
        # drop the cell types to remove
        adata_lst[dset] = adata_lst[dset][~adata_lst[dset].obs["latest_annot"].isin(CTYPE2RM[dset]), :].copy()
        # add the disease state as dataset to separate same cell types in different data sets
        adata_lst[dset].obs["latest_annot"] = adata_lst[dset].obs["latest_annot"].astype("str") + f"-{'disomy' if dset == 'Healthy' else 't21'}"
        # follow the processing indicated in tutorial
        adata_lst[dset].var["SYMBOL"] = adata_lst[dset].var.index
        adata_lst[dset].var.set_index("Ensembl", drop=True, inplace=True, verify_integrity=True)
        # delete unnecessary raw slot (to be removed in a future version of the tutorial)
        del adata_lst[dset].raw, adata_lst[dset].obs["batch"]
        gc.collect()
    
    # Make a combined dataset and add to the processing list
    adata_lst["combined"] = anndata.concat(adata_lst.values(), join="inner", label="dataset", keys=adata_lst.keys())

    # Subset for each case
    for dset, adata in adata_lst.items():
        print(adata_lst[dset], "\n", file=sys.stderr)
        print(adata_lst[dset].shape, "\n", file=sys.stderr)
        adata1 = adata[adata.obs["patient_sample"].isin(SMP_SET_DICT[dset]), :].copy()
        adata2 = adata[~adata.obs["patient_sample"].isin(SMP_SET_DICT[dset]), :].copy()
        print(adata1.shape, adata2.shape, "\n", file=sys.stderr)
        print(pd.concat([adata1.obs["latest_annot"].value_counts(), adata2.obs["latest_annot"].value_counts()],
                        axis=1, join="outer"), "\n",
              file=sys.stderr)
        adata1.write_h5ad(f"{WKDIR}subset_data/{dset}.subset1.h5ad")
        adata2.write_h5ad(f"{WKDIR}subset_data/{dset}.subset2.h5ad")
        del adata1, adata2
        gc.collect()
