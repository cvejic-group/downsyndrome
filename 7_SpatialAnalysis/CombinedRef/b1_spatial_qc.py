import os
import gc
import sys
import scanpy as sc
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import matplotlib.backends.backend_pdf as mpdf

from _functions import select_slide
from _functions import plot_spatial

WKDIR = "/lustre/scratch126/casm/team-cvejic/haoliang/pj_DownSyndrome/results/STx_compare_combinedVSseparate_newPip/"
GENE_COUNT_THRESH = 800
ABD_FIELD = "means_cell_abundance_w_sf"


def clean_adata(adata2clean, var_qc="total_gene_counts"):
    spot_num = {}
    spot_num["before"] = {sec: adata2clean[adata2clean.obs["sample"].isin([sec]), :].n_obs
                          for sec in adata2clean.obs["sample"].unique()}
    adata2clean = adata2clean[adata2clean.obs[var_qc] >= GENE_COUNT_THRESH, :].copy()
    spot_num["after"] = {sec: adata2clean[adata2clean.obs["sample"].isin([sec]), :].n_obs
                         for sec in adata2clean.obs["sample"].unique()}
    qc_summary = pd.DataFrame.from_dict(spot_num, orient="columns")
    qc_summary["kept percent"] = qc_summary["after"] / qc_summary["before"] * 100
    return adata2clean, qc_summary


def plot_hists(adata2plot, vars=["total_gene_counts", "total_cell_counts"], pdf=None):
    n_subplots = len(vars)
    _, axes = plt.subplots(n_subplots, 1, sharex=False, sharey=False, figsize=(16, n_subplots*5))
    sns.set_theme(font_scale=1.5, style="white")
    for i, var in enumerate(vars):
        ax_twin = axes[i].twinx()
        sns.histplot(adata2plot.obs[var], bins=100, ax=axes[i])
        sns.ecdfplot(adata2plot.obs[var], ax=ax_twin, color="tomato")
        axes[i].set_ylabel("spot count")
    plt.suptitle(f"{adata2plot.obs['sample'].unique()[0]}")
    plt.tight_layout()
    if pdf is not None:
        pdf.savefig()
        plt.close()


if __name__ == "__main__":
    assert len(sys.argv) == 2 or len(sys.argv) == 3
    case_lst = sys.argv[1:]
    print(f"* Summarising on {case_lst}.", file=sys.stderr)

    for dset in case_lst:
        in_dir = f"{WKDIR}{dset}/cell2location_map/"
        out_dir = f"{WKDIR}{dset}/analysis_res/"
        if not os.path.exists(out_dir):
            print("Creating output folder:", out_dir, file=sys.stderr)
            os.mkdir(out_dir)

        adata = sc.read_h5ad(f"{in_dir}sp.h5ad")
        print("* Loaded anndata:", adata, file=sys.stderr)
        
        # Estimated cell type abundance by cell2location
        abd_mat = adata.obsm[ABD_FIELD][[f"{ABD_FIELD.replace('_cell', 'cell')}_{ct}" for ct in adata.uns["mod"]["factor_names"]]].T
        abd_mat["cell type"] = [x.replace(f"{ABD_FIELD.replace('_cell', 'cell')}_", "").replace("-disomy", "").replace("-t21", "")
                                for x in abd_mat.index]
        abd_mat = abd_mat.groupby("cell type").sum().T
        abd_mat.rename(columns={col: f"{col} abd" for col in abd_mat.columns}, inplace=True)
        idx_ori = adata.obs_names.copy()
        adata.obs = pd.concat([adata.obs, abd_mat], axis=1, verify_integrity=True)
        assert (adata.obs_names == idx_ori).all()
        
        # Expression matrix and QC metrics
        exp_mat = pd.DataFrame.sparse.from_spmatrix(adata.X, columns=adata.var.index, index=adata.obs.index)
        adata.obs["total_gene_counts"] = exp_mat.sum(axis=1)
        adata.obs["total_cell_counts"] = adata.obsm[ABD_FIELD].sum(axis=1)
        
        # Clean data
        adata, qc_summary = clean_adata(adata)
        print("shape of adata:", adata.shape, file=sys.stderr)
        print("shape of adata.obs:", adata.obs.shape, file=sys.stderr)
        print("shape of adata.obsm:", adata.obsm[ABD_FIELD].shape, file=sys.stderr)
        adata.write(f"{out_dir}sp_cleaned.h5ad", compression="gzip")
        
        # Per section plot and write QC summary
        pdf = mpdf.PdfPages(f"{out_dir}qc_plots_persection.pdf")
        for sec in adata.obs["sample"].unique():
            adata_sub = select_slide(adata, sec, "sample")
            # QC histogram
            plot_hists(adata_sub, pdf=pdf)
            # Filtered spatial cell type abundancies
            plot_spatial(adata_sub, pdf, color_by=list(abd_mat.columns), ncols=4)
            del adata_sub
            gc.collect()
        pdf.close()
        qc_summary.sort_values(by="kept percent").to_csv(f"{out_dir}QC_summary.csv")
