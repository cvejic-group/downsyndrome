import sys
import pandas as pd
import numpy as np
import scanpy as sc
import matplotlib.backends.backend_pdf as mpdf

from scipy.stats import mannwhitneyu
from statsmodels.stats.multitest import multipletests

from _functions import select_slide
from _functions import plot_boxes

GENE_COUNT_THRESH = 800
CELL_ABD_FIELD = "means_cell_abundance_w_sf"

WKDIR = "/lustre/scratch126/casm/team-cvejic/haoliang/pj_DownSyndrome/results/STx_compare_combinedVSseparate_newPip/"

T21_SECTIONS = ["spaceranger122_count_36337_OT_F_Visium9720395_GRCh38-3_0_0", "spaceranger122_count_36961_OT_F_Visium9998361_GRCh38-3_0_0",
                "spaceranger122_count_38823_OT_F_Visium9880863_GRCh38-3_0_0", "spaceranger122_count_38825_OT_F_Visium9880673_GRCh38-3_0_0", 
                "spaceranger122_count_36337_OT_F_Visium9720396_GRCh38-3_0_0", "spaceranger122_count_38822_OT_F_Visium9880766_GRCh38-3_0_0",
                "spaceranger122_count_38824_OT_F_Visium9880576_GRCh38-3_0_0", "spaceranger122_count_39397_OT_F_Visium10405238_GRCh38-3_0_0", 
                "spaceranger122_count_36338_OT_F_Visium9720491_GRCh38-3_0_0", "spaceranger122_count_38822_OT_F_Visium9880767_GRCh38-3_0_0", 
                "spaceranger122_count_38824_OT_F_Visium9880577_GRCh38-3_0_0", "spaceranger122_count_39397_OT_F_Visium10405239_GRCh39-3_0_0",
                "spaceranger122_count_36338_OT_F_Visium9720492_GRCh38-3_0_0", "spaceranger122_count_38822_OT_F_Visium9880768_GRCh38-3_0_0", 
                "spaceranger122_count_38825_OT_F_Visium9880670_GRCh38-3_0_0", "spaceranger122_count_39397_OT_F_Visium10405240_GRCh38-3_0_0",
                "spaceranger122_count_36644_OT_F_Visium9879893_GRCh38-3_0_0", "spaceranger122_count_38822_OT_F_Visium9880769_GRCh38-3_0_0",
                "spaceranger122_count_38825_OT_F_Visium9880671_GRCh38-3_0_0", "spaceranger122_count_39397_OT_F_Visium10405241_GRCh38-3_0_0", 
                "spaceranger122_count_36644_OT_F_Visium9879894_GRCh38-3_0_0", "spaceranger122_count_38823_OT_F_Visium9880862_GRCh38-3_0_0",
                "spaceranger122_count_38825_OT_F_Visium9880672_GRCh38-3_0_0"]
CTRL_SECTIONS = ["spaceranger122_count_36337_OT_F_Visium9720393_GRCh38-3_0_0", "spaceranger122_count_36338_OT_F_Visium9720490_GRCh38-3_0_0",
                 "spaceranger122_count_36961_OT_F_Visium9998362_GRCh38-3_0_0", "spaceranger122_count_38823_OT_F_Visium9880865_GRCh38-3_0_0",
                 "spaceranger122_count_36337_OT_F_Visium9720394_GRCh38-3_0_0", "spaceranger122_count_36644_OT_F_Visium9879895_GRCh38-3_0_0",
                 "spaceranger122_count_36961_OT_F_Visium9998363_GRCh38-3_0_0", "spaceranger122_count_36338_OT_F_Visium9720489_GRCh38-3_0_0",
                 "spaceranger122_count_36644_OT_F_Visium9879896_GRCh38-3_0_0", "spaceranger122_count_38823_OT_F_Visium9880864_GRCh38-3_0_0"]


def summarise_frequency(adata):
    all_cts = [x.replace("-disomy", "").replace("-t21", "") for x in adata.uns["mod"]["factor_names"]]
    freq_tab = {}
    for sec in adata.obs["sample"].unique():
        adata_sub = select_slide(adata, sec, "sample")
        freq_tab[sec] = {ct: adata_sub.obs[f"{ct} abd"].sum() for ct in all_cts}
    freq_tab = pd.DataFrame.from_dict(freq_tab, orient="index")
    freq_env_sum = freq_tab.sum(axis=1)
    for ct in freq_tab.columns:
        freq_tab[ct] *= (100 / freq_env_sum)
    print("Cell type percentage sum check:", (freq_tab.sum(axis=1) - 100).abs().max(),
          file=sys.stderr)
    freq_tab = freq_tab.reset_index().rename(columns={"index": "sample"})
    return freq_tab


if __name__ == "__main__":
    assert len(sys.argv) == 2 or len(sys.argv) == 3
    case_lst, mode = sys.argv[1:], "_and_".join(sys.argv[1:])
    print(f"* Summarising on {case_lst} as {mode}.", file=sys.stderr)

    freq_all = None
    for dset in case_lst:
        print(f"* Dealing with {dset}...", file=sys.stderr)
        adata = sc.read_h5ad(f"{WKDIR}{dset}/analysis_res/sp_cleaned.h5ad")
        freq_dset = summarise_frequency(adata)
        freq_dset = freq_dset.melt(id_vars="sample", var_name="cell type", value_name="percent")
        freq_all = pd.concat([freq_all, freq_dset], axis=0, ignore_index=True)
    freq_all["environment"] = ["disomy" if x in CTRL_SECTIONS else "t21" for x in freq_all["sample"]]
    freq_all.to_csv(f"{WKDIR}cell_type_frequencies.{mode}.csv", index=False)
    freq_all.pivot_table(index=["sample", "environment"], 
                         values="percent",
                         columns="cell type",
                         fill_value=0).to_csv(f"{WKDIR}cell_type_frequencies.pivot.{mode}.csv", index=True)

    # Wilcoxon rank sum test for cell type frequency per spot
    pvals = {}
    for ct in freq_all["cell type"].unique():
        x = freq_all.loc[(freq_all["cell type"] == ct) & (freq_all["environment"] == "disomy"), "percent"].tolist()
        y = freq_all.loc[(freq_all["cell type"] == ct) & (freq_all["environment"] == "t21"), "percent"].tolist()
        if x and y:  # to exclude cell types in single environment
            pvals[ct] = {"median.disomy": np.median(x),
                         "median.t21": np.median(y),
                         "p.raw": mannwhitneyu(x, y, alternative="two-sided", method="exact")[1]}
    pval_res = pd.DataFrame.from_dict(pvals, orient="index")
    _, pval_res["B-H FDR"], _, _ = multipletests(pval_res["p.raw"], method="fdr_bh", is_sorted=False)
    _, pval_res["Bonferroni p.adj"], _, _ = multipletests(pval_res["p.raw"], method="bonferroni", is_sorted=False)
    pval_res.sort_values(by=["Bonferroni p.adj", "B-H FDR", "p.raw"], inplace=True)
    pval_res.to_csv(f"{WKDIR}wilcoxon_pvalues_on_celltype_frequency.{mode}.csv")

    # Plot frequency per spot for each cell type
    pdf = mpdf.PdfPages(f"{WKDIR}boxplots_celltype_frequency_by_environment.{mode}.pdf")
    plot_boxes(df=freq_all, x="percent", y="cell type", palette={"disomy": "skyblue", "t21": "orange"},
               y_order=list(pval_res.index) + list(freq_all["cell type"].loc[~freq_all["cell type"].isin(pval_res.index)].unique()),
               groupby="environment", pdf=pdf)
    pdf.close()
