import warnings
warnings.simplefilter("ignore")

import sys

#import bbknn
import numpy as np
import scanpy as sc
import pandas as pd
import seaborn as sns
#import scrublet as scr
import matplotlib.pyplot as plt
import matplotlib.backends.backend_pdf as mpdf

from math import log
from scipy import sparse
from matplotlib import cm
from matplotlib import colors
from scipy.sparse import issparse
from sklearn.utils import shuffle
from scipy.spatial import distance
from sklearn.preprocessing import normalize
from sklearn.model_selection import train_test_split

sys.path.append("/nfs/research1/gerstung/nelson/downsyndrome/Functions/")
# Get the global settings
#from global_settings import global_settings
#global_settings()

# Get the bespoke analysis functions
from scRNA_functions import scRNA_functions
fc = scRNA_functions()

# Global colour settings
myColors = ['#e6194b', '#3cb44b', '#ffe119', '#4363d8', '#f58231', '#911eb4', '#46f0f0', '#f032e6', '#bcf60c', '#fabebe',
            '#008080', '#e6beff', '#9a6324', '#fffac8', '#800000', '#aaffc3', '#808000', '#ffd8b1', '#000075', '#808080',
            '#307D7E', '#000000', '#DDEFFF', '#000035', '#7B4F4B', '#A1C299', '#300018', '#C2FF99', '#0AA6D8', '#013349',
            '#00846F', '#8CD0FF', '#3B9700', '#04F757', '#C8A1A1', '#1E6E00', '#DFFB71', '#868E7E', '#513A01', '#CCAA35',
            '#800080', '#DAA520', '#1E90FF', '#3CB371', '#9370DB', '#8FBC8F', '#00FF7F', '#0000CD', '#556B2F', '#FF00FF',
            '#CD853F', '#6B8E23', '#008000', '#6495ED', '#00FF00', '#DC143C', '#FFFF00', '#00FFFF', '#FF4500', '#4169E1',
            '#48D1CC', '#191970', '#9ACD32', '#FFA500', '#00FA9A', '#2E8B57', '#40E0D0', '#D2691E', '#66CDAA', '#FFEFD5',
            '#20B2AA', '#FF0000', '#EEE8AA', '#BDB76B', '#E9967A', '#AFEEEE', '#000080', '#FF8C00', '#B22222', '#5F9EA0',
            '#ADFF2F', '#FFE4B5', '#7B68EE', '#7FFFD4', '#0000FF', '#BA55D3', '#90EE90', '#FFDAB9', '#6A5ACD', '#8B0000',
            '#8A2BE2', '#CD5C5C', '#F08080', '#228B22', '#FFD700', '#006400', '#98FB98', '#00CED1', '#00008B', '#9400D3',
            '#9932CC', '#4B0082', '#F0E68C', '#483D8B', '#008B8B', '#8B008B', '#4682B4']
myColorTissues = ['#ffe119', '#3cb44b', '#e6194b']

# Define a nice colour map for gene expression
colors2 = plt.cm.Reds(np.linspace(0, 1, 128))
colors3 = plt.cm.Greys_r(np.linspace(0.7,0.8,20))
colorsComb = np.vstack([colors3, colors2])
mymap = colors.LinearSegmentedColormap.from_list('my_colormap', colorsComb)
sns.set_style("white")

# Inputs and Outputs
outDir = "/nfs/research1/gerstung/nelson/outputs/downsyndrome/3_clustering/"
inputs = '/nfs/research1/gerstung/nelson/data/downsyndrome/ScanpyObjects/merged_batchcorr/'
outputs = '/nfs/research1/gerstung/nelson/data/downsyndrome/ScanpyObjects/merged_batchcorr_clustered/'

def clustering_and_umap(adata, resolution, name):
    sc.settings.verbosity=3

    sc.pp.neighbors(adata,
                n_pcs=adata.obsm["X_pca_harmonize"].shape[1],
                use_rep="X_pca_harmonize",
                knn=True,
                random_state=42,
                method='umap',
                metric='euclidean')

    print("\n * Computing UMAP")
    sc.tl.umap(adata, random_state=10, n_components=3, init_pos='random')

    # # 1. Clustering
    # Run graphical clustering/connected community detection with the Leiden algorithm
    sc.tl.leiden(adata, resolution=resolution, key_added='leiden')
    adata.obs["leiden"].cat.categories

    # # 2. UMAP plots
    '''
    f, axs = plt.subplots(1,3,figsize=(60,16))
    sns.set(font_scale=1.5)
    sns.set_style("white")

    fc.plotUMAP(adata, variable="leiden", palette=myColors, components=["1,2"], ax=axs[0], pdf=pdf)
    fc.plotUMAP(adata, variable="leiden", palette=myColors, components=["1,3"], ax=axs[1], pdf=pdf)
    fc.plotUMAP(adata, variable="leiden", palette=myColors, components=["2,3"], ax=axs[2], pdf=pdf)

    #sns.despine(offset=10, trim=False)
    plt.tight_layout()
    pdf.savefig()
    plt.close()
    '''
    fc.plot3D(adata, group_by="leiden", space="X_umap", palette=myColors, write_to=outDir+"{0}_UMAP_Leiden".format(name))

    # # 3. Top 100 marker genes
    sc.tl.rank_genes_groups(adata,
                        groupby='leiden',
                        n_genes=100,
                        method='wilcoxon',
                        key_added="rank_genes_groups_leiden",
                        corr_method='bonferroni')


    # Marker genes filtering (in at least 30% of cells)
    sc.tl.filter_rank_genes_groups(adata, # min_fold_change=3,
                               min_in_group_fraction=0.3,
                               min_fold_change=0.0,
                               key="rank_genes_groups_leiden",
                               key_added="rank_genes_groups_leiden_filtered",
                               max_out_group_fraction=1)

    #plt.rcParams["figure.figsize"]=16,8
    #fc.plotGenesRankGroups(adata, n_genes=20, key="rank_genes_groups_leiden_filtered", pdf=pdf)

    # # 4. Heatmaps
    kwargs = dict()
    kwargs['vmin'] = -3
    kwargs['vmax'] = 3

    # Make and save a gene expression heatmap
    sc.pl.rank_genes_groups_heatmap(adata,
                                n_genes=30,
                                use_raw=False,
                                swap_axes=True,
                                dendrogram=False,
                                show_gene_labels=True,
                                cmap=cm.plasma,
                                figsize=(300,250),
                                save="{0}_clustering_Heatmap.png".format(name),
                                key="rank_genes_groups_leiden_filtered",
                                **kwargs)

    file = "/nfs/research1/gerstung/nelson/downsyndrome/MetaData/{0}_marker_genes_leiden.csv".format(name)
    pd.DataFrame(adata.uns["rank_genes_groups_leiden_filtered"]["names"]).to_csv(file)

def main(resolution=1, healthy=False, cells=""):
    #pdf = mpdf.PdfPages(outDir+"DS_clustering.pdf")
    # Loading samples
    inputs = '/nfs/research1/gerstung/nelson/data/downsyndrome/ScanpyObjects/merged_batchcorr/'
    outputs = '/nfs/research1/gerstung/nelson/data/downsyndrome/ScanpyObjects/merged_batchcorr_clustered/'
    if cells!="":
        inputs = '/nfs/research1/gerstung/nelson/data/downsyndrome/ScanpyObjects/merged_batchcorr_clustered/subtypes/'
        outputs = '/nfs/research1/gerstung/nelson/data/downsyndrome/ScanpyObjects/merged_batchcorr_clustered/subtypes/'
        adata = sc.read_h5ad(inputs+"10X_DownSyndrome_Liver_{0}.h5ad".format(cells))
        clustering_and_umap(adata=adata, resolution=resolution, name="DownSyndrome_{0}_Liver".format(cells))
        adata.write(outputs+"10X_DownSyndrome_Liver_{0}.h5ad".format(cells), compression="gzip")
    else:
        adatas = list()
        if not healthy:
            for s in [1,2,3]:
                adata = sc.read_h5ad(inputs+"10X_DownSyndrome_Split{0}.h5ad".format(s))
                adatas.append(adata)

        adata = sc.read_h5ad(inputs+"10X_Healthy_Full.h5ad") if healthy else adatas[0].concatenate(adatas[1:], join='outer', index_unique="-")
        del adatas
        adata_femur = adata[adata.obs.organ=="Femur",:]
        adata_liver = adata[adata.obs.organ=="Liver",:]
        name = "Healthy" if healthy else "DownSyndrome"
        clustering_and_umap(adata=adata_femur, resolution=resolution, name="{0}_Femur".format(name))
        clustering_and_umap(adata=adata_liver, resolution=resolution, name="{0}_Liver".format(name))

        # Save information on the known marker genes from the Leiden clustering
        #pdf.close()
        # # 5. Save object
        adata_femur.write(outputs+"10X_{0}_Femur.h5ad".format(name), compression="gzip")
        adata_liver.write(outputs+"10X_{0}_Liver.h5ad".format(name), compression="gzip")

if __name__=="__main__":
    # Adding an argument parser
    from argparse import ArgumentParser
    parser = ArgumentParser()
    parser.add_argument("-r", "--resolution", type=float, help="Specify resolution parameter in the Leiden clustering",default=1.0)
    parser.add_argument('-he', "--healthy", action="store_true", help="Run on healthy data")
    parser.add_argument("-c", "--cells", type=str, help="Specify the cell type",default="")

    options = parser.parse_args()

    # Defining dictionary to be passed to the main function
    option_dict = dict( (k, v) for k, v in vars(options).items() if v is not None)
    print(option_dict)
    main(**option_dict)
