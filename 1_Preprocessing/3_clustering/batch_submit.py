from os import system as sys

fname = "Dotplot_Healthy_Liver_v6"
f = open(fname+".sh", 'w')
f.write("cd /nfs/research1/gerstung/nelson/downsyndrome/PythonPipeline/3_clustering/\n")
f.write("conda activate minimal_env\n" )
#f.write("python run_clustering_heatmap.py --resolution=1.7 --healthy")
f.write("python run_preliminary_merging.py")

print("bsub -P bigmem -M 500000 -o {0}.txt -e {0} bash {0}.sh".format(fname))
sys("bsub -P bigmem -M 500000 -o {0}.txt -e {0} bash {0}.sh".format(fname))
