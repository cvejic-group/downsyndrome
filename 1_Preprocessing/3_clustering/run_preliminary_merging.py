import warnings
warnings.simplefilter("ignore")

import sys

#import bbknn
import numpy as np
import scanpy as sc
import pandas as pd
import seaborn as sns
#import scrublet as scr
import matplotlib.pyplot as plt
import matplotlib.backends.backend_pdf as mpdf

from math import log
from scipy import sparse
from matplotlib import cm
from matplotlib import colors
from scipy.sparse import issparse
from sklearn.utils import shuffle
from scipy.spatial import distance
from sklearn.preprocessing import normalize
from sklearn.model_selection import train_test_split

sys.path.append("/nfs/research1/gerstung/nelson/downsyndrome/Functions/")
# Get the global settings
#from global_settings import global_settings
#global_settings()

# Get the bespoke analysis functions
from scRNA_functions import scRNA_functions
fc = scRNA_functions()

# Global colour settings
myColors = ['#e6194b', '#3cb44b', '#ffe119', '#4363d8', '#f58231', '#911eb4', '#46f0f0', '#f032e6', '#bcf60c', '#fabebe',
            '#008080', '#e6beff', '#9a6324', '#fffac8', '#800000', '#aaffc3', '#808000', '#ffd8b1', '#000075', '#808080',
            '#307D7E', '#000000', '#DDEFFF', '#000035', '#7B4F4B', '#A1C299', '#300018', '#C2FF99', '#0AA6D8', '#013349',
            '#00846F', '#8CD0FF', '#3B9700', '#04F757', '#C8A1A1', '#1E6E00', '#DFFB71', '#868E7E', '#513A01', '#CCAA35',
            '#800080', '#DAA520', '#1E90FF', '#3CB371', '#9370DB', '#8FBC8F', '#00FF7F', '#0000CD', '#556B2F', '#FF00FF',
            '#CD853F', '#6B8E23', '#008000', '#6495ED', '#00FF00', '#DC143C', '#FFFF00', '#00FFFF', '#FF4500', '#4169E1',
            '#48D1CC', '#191970', '#9ACD32', '#FFA500', '#00FA9A', '#2E8B57', '#40E0D0', '#D2691E', '#66CDAA', '#FFEFD5',
            '#20B2AA', '#FF0000', '#EEE8AA', '#BDB76B', '#E9967A', '#AFEEEE', '#000080', '#FF8C00', '#B22222', '#5F9EA0',
            '#ADFF2F', '#FFE4B5', '#7B68EE', '#7FFFD4', '#0000FF', '#BA55D3', '#90EE90', '#FFDAB9', '#6A5ACD', '#8B0000',
            '#8A2BE2', '#CD5C5C', '#F08080', '#228B22', '#FFD700', '#006400', '#98FB98', '#00CED1', '#00008B', '#9400D3',
            '#9932CC', '#4B0082', '#F0E68C', '#483D8B', '#008B8B', '#8B008B', '#4682B4']
myColorTissues = ['#ffe119', '#3cb44b', '#e6194b']

# Define a nice colour map for gene expression
colors2 = plt.cm.Reds(np.linspace(0, 1, 128))
colors3 = plt.cm.Greys_r(np.linspace(0.7,0.8,20))
colorsComb = np.vstack([colors3, colors2])
mymap = colors.LinearSegmentedColormap.from_list('my_colormap', colorsComb)
sns.set_style("white")

# Inputs and Outputs
outDir = "/nfs/research1/gerstung/nelson/outputs/downsyndrome/3_clustering/"
#inputs = '/nfs/research1/gerstung/nelson/data/downsyndrome/ScanpyObjects/merged_batchcorr_clustered/10X_Healthy_Liver.h5ad'
#outputs = '/nfs/research1/gerstung/nelson/data/downsyndrome/ScanpyObjects/merged_batchcorr_clustered/10X_Healthy_Femur.h5ad'
inputs = '/nfs/research1/gerstung/nelson/data/downsyndrome/ScanpyObjects/merged_batchcorr_clustered/10X_DownSyndrome_Femur.h5ad'
outputs = '/nfs/research1/gerstung/nelson/data/downsyndrome/ScanpyObjects/merged_batchcorr_clustered/10X_DownSyndrome_Femur.h5ad'

def main():
    '''
    #pdf = mpdf.PdfPages(outDir+"DS_only_Broad_subclustered_summary.pdf")
    #pdf = mpdf.PdfPages(outDir+"Merged_Scanorama_summary.pdf")
    # Loading samples
    adata = sc.read_h5ad(inputs)
    name = "DS_Femur"

    v2_cells = adata.obs["leiden_v2"].values
    v4_cells = adata.obs["leiden_v4"].values

    # See below for liver fix ... run when you can
    adata.obs["leiden_v5_temp"] = np.array([v4_cells[i] if (v4_cells[i] != "Endothelial cells" and v4_cells[i] != "Hepatocytes" and v4_cells[i] != "Fibroblasts" and v2_cells[i] != "48,0" and v2_cells[i] != "48,1") else v2_cells[i] for i in range(len(v4_cells))])

    # Perform preliminary merging of some clusters
    from merging_dict import merging_dict
    fc.mergeClusters(adata, newName='leiden_v7', labels=merging_dict, cluster='leiden_v6', verbose=True)

    #del adata.obs["leiden_v6_temp"]
    #pd.DataFrame(adata.obs["annotations"].value_counts()).sort_index()

    # Perform subclustering of specific Leiden clusters with k-means
    #adata = fc.run_subclustering(adata, algorithm="kmeans",
    #                             restrict_to=("leiden_v3_temp",["36,0"]),
    #                             space="X_umap", n_clusters=[2], key_added="leiden_v3")
    #del adata.obs["leiden_v3_temp"]
    #adata = fc.run_subclustering(adata, algorithm="kmeans",
    #                             restrict_to=("leiden_v1",["Granulocyte progenitors", "Neutrophils", "Monocytes/macrophages", "B cells", "Progenitors 1", "Fibroblasts", "34", "35", "30", "40", "41", "14", "Endothelial cells"]),
    #                             space="X_umap", n_clusters=[2, 3, 5, 4, 3, 10, 2, 2, 2, 2, 2, 2, 6], key_added="leiden_v2")

    #adata.obs["leiden_with_subclusters"].astype('category')
    #adata.obs["leiden_broad"].astype('category')
    #adata.obs["leiden_with_subclusters"].cat.categories

    # 2D UMAP plots

    pdf_ct = mpdf.PdfPages(outDir+"UMAP_Scanorama_Merged_Cell_Types.pdf")
    f, axs = plt.subplots(1,3,figsize=(60,16))
    sns.set(font_scale=1.5)
    sns.set_style("white")

    fc.plotUMAP(adata, variable="annotations", palette=myColors, components=["1,2"], ax=axs[0], pdf=pdf_ct)
    fc.plotUMAP(adata, variable="annotations", palette=myColors, components=["1,3"], ax=axs[1], pdf=pdf_ct)
    fc.plotUMAP(adata, variable="annotations", palette=myColors, components=["2,3"], ax=axs[2], pdf=pdf_ct)

    plt.tight_layout()
    pdf_ct.savefig()
    plt.close()
    pdf_ct.close()


    pdf_env = mpdf.PdfPages(outDir+"UMAP_res95_Stratified_Merged_Environments.pdf")
    f, axs = plt.subplots(1,3,figsize=(60,16))
    sns.set(font_scale=1.5)
    sns.set_style("white")

    fc.plotUMAP(adata, variable="environment", palette=myColors, components=["1,2"], ax=axs[0], pdf=pdf_env)
    fc.plotUMAP(adata, variable="environment", palette=myColors, components=["1,3"], ax=axs[1], pdf=pdf_env)
    fc.plotUMAP(adata, variable="environment", palette=myColors, components=["2,3"], ax=axs[2], pdf=pdf_env)

    plt.tight_layout()
    pdf_env.savefig()
    plt.close()
    pdf_env.close()

    # 3D UMAP
    fc.plot3D(adata, group_by="leiden_v5", space="X_umap", palette=myColors, write_to=outDir+"{0}_UMAP_Leiden_v5".format(name))

    # Marker genes and heatmaps
    sc.tl.rank_genes_groups(adata,
                        groupby='leiden_v6',
                        n_genes=100,
                        method='wilcoxon',
                        key_added="rank_genes_groups_annotations",
                        corr_method='bonferroni')


    # Marker genes filtering (in at least 30% of cells)
    sc.tl.filter_rank_genes_groups(adata, # min_fold_change=3,
                               min_in_group_fraction=0.3,
                               min_fold_change=0.0,
                               key="rank_genes_groups_annotations",
                               key_added="rank_genes_groups_annotations_filtered",
                               max_out_group_fraction=1)

    plt.rcParams["figure.figsize"]=16,8
    #fc.plotGenesRankGroups(adata, n_genes=20, key="rank_genes_groups_annotations_filtered", pdf=pdf)

    kwargs = dict()
    kwargs['vmin'] = -3
    kwargs['vmax'] = 3

    # Make and save a gene expression heatmap
    sc.pl.rank_genes_groups_heatmap(adata,
                                n_genes=30,
                                use_raw=False,
                                swap_axes=True,
                                dendrogram=False,
                                show_gene_labels=True,
                                cmap=cm.plasma,
                                figsize=(300,250),
                                save="{0}_clustering_Heatmap_Leiden_v5.png".format(name),
                                key="rank_genes_groups_annotations_filtered",
                                **kwargs)


    # Save information on the known marker genes from the Leiden clustering
    file = "/nfs/research1/gerstung/nelson/downsyndrome/MetaData/{0}_marker_genes_leiden_v5.csv".format(name)
    pd.DataFrame(adata.uns["rank_genes_groups_annotations_filtered"]["names"]).to_csv(file)

    #pdf.close()
    # Save object

    adata.write(outputs, compression='gzip')

    # Dot plots for separate blood and niche components
    niche = ["LSECs", "Endothelial cells", "Hepatic stellate cells", "Activate hepatic stellate cells", "Hepatocytes"]
    blood = ["HSCs/MPPs", "Cycling HSCs", "MEMPs", "Erythroid progenitors", "Early Erythroid cells", "Late Erythroid cells", "Megakaryocytes",
             "Cycling megakaryocytes", "Mast cells", "Neutrophil progenitors", "Monocyte precursors", "Inflammatory macrophages", "Tolerogenic macrophages",
             "NK progenitors", "NK cells", "B cells", "Cycling B cells", "cDC2"]
    adata_n = adata[adata.obs["leiden_v5"].isin(niche), :]
    adata_b = adata[adata.obs["leiden_v5"].isin(blood), :]

    dict_dot_niche = {
        "LSECs": ["STAB1", "STAB2", "LYVE1", "CD32B"],
        "Endothelial cells": ["CDH5", "KDR"],
        "Hepatic stellate cells": ["DCN", "COL1A1", "COL3A1", "RBP1"],
        "Active hepatic stellate cells": ["ACTA2", "TAGLN", "MYL9"],
        "Hepatocytes": ["ALB", "APOE", "AFP"]
    }

    dict_dot_blood = {
        "HSCs/MPPs": ["CD34", "SPINK2",  "MLLT3"],
        "Cycling HSCs": ["MKi67"],
        "MEMPs": ["CTNNBL1", "TESPA1", "GATA1"],
        "Erythroid progenitors": ["KLF1", "TFRC", "AHSP"],
        "Early Erythroid cells":["ALAS2", "GYPA", "HBA1"],
        "Late Erythroid cells":["ALAS2", "GYPA", "HBA1"],
        "Megakaryocytes":["ITGA2B", "GP9", "PF4"],
        "Cycling megakaryocytes":["ITGA2B", "GP9", "PF4"],
        "Mast cells":["LMO4","HDC", "CPA3"],
        "Neutrophil progenitors":["LYZ", "MPO", "AZU1"],
        "Monocyte precursors": ["CD14", "CD68", "S100A9"],
        "Inflammatory macrophages": ["FCN", "MNDA", "VCAN"],
        "Tolerogenic macrophages": ["MARCO", "C1QA", "CD163"],
        "NK progenitors": ["NKG7", "PRF1", "GZMA"],
        "NK cells": ["NKG7", "PRF1", "GZMA"],
        "B cells": ["CD79A", "VPREB1", "PAX5"],
        "Cycling B cells": ["CD79A", "VPREB1", "PAX5"],
        "cDC2": ["CD1C", "CLEC4A", "CLEC10A"]
    }

    dict_markers = {
            "Smooth muscle cells": ["PDGFRB", "TAGLN", "ACTA2", "NOTCH3", "RGS5"],
            "Myofibroblasts":  ["PDGFRA", "CXCL12"],
            "Schwann cells":  ["MPZ", "NRXN1"],
            "Vascular endothelial cells": ["CDH5", "KDR"],
            "Sinusoidal endothelial cells": ["STAB2", "STAB1", "LYVE1"],
            "Striated muscle cells":  ["TNNT1"]
    }
    import matplotlib
    matplotlib.rcdefaults()
    ax = sc.pl.dotplot(adata,
                   dict_markers,
                   groupby="leiden_v1",
                   standard_scale='var',
                   smallest_dot=0.0,
                   dot_min=None,
                   dot_max=None,
                   color_map='Reds',
                   dendrogram=False,
                   figsize=(12,15),
                   show=True,
                   save="{0}_Leiden_v1.png".format(name),
                   linewidths=2,swap_axes=True)

    ax = sc.pl.dotplot(adata_n,
                   dict_dot_niche,
                   groupby="leiden_v5",
                   standard_scale='var',
                   smallest_dot=0.0,
                   dot_min=None,
                   dot_max=None,
                   color_map='Reds',
                   dendrogram=False,
                   figsize=(12,15),
                   show=True,
                   save="{0}_Niche_Leiden_v5.png".format(name),
                   linewidths=2,swap_axes=True)


    adata = sc.read_h5ad(outputs)
    cell_types = [
            "Fibroblasts,1",
            "Muscle cells",
            "Fibroblasts,7",
            "Fibroblasts,4",
            "Endothelial cells,2",
            "Endothelial cells,3",
            "Fibroblasts,3",
            "Endothelial cells,5",
            "Fibroblasts,8",
            "Fibroblasts,5",
            "Fibroblasts,0",
            "14,1",
            "Fibroblasts,2",
            "Endothelial cells,0",
            "Fibroblasts,6",
            "14,0",
            "Endothelial cells,1",
            "Endothelial cells,4",
            "30,1",
            "34,0",
            "40,0",
            "30,0",
            "Straited muscle (contamination)",
            "35,0",
            "Fibroblasts,9",
            "34,1",
            "41,0",
            "41,1",
            "40,1",
            "39",
            "35,1",
            "Unknown",
    ]
    adata = adata[adata.obs["leiden_v2"].isin(cell_types), :]
    name = "DS_Femur_Niche"
    #genes = [
    #        "CD34", "SPINK2", "CD52", "SELL", "MLLT3", "GATA1", "KLF1", "CTNNBL1", "TESPA1", "TFRC", "MPO", "AZU1",
    #        "LYZ", "SPI1", "GATA2", "HDC", "CPA3", "CD14", "CD163", "MARCO", "ITGA2B", "MKI67", "IL7R", "CD3E", "CD79A",
    #        "CD79B", "CD19", "PAX5"
    #]

    #genes = ["ALAS2", "HLA-DRB1", "AHSP", "GYPA", "BPGM"]
    #genes = ["DHFR", "UHRF1", "VPREB1", "RAG1", "DNTT", "MME", "CD38", "IGHM", "TRAC", "IL2RG", "RORC", "KIT", "SPIB", "IGLL1", "CD24", "CD68", "MPO", "AZU1", "LYZ"]
    #genes = ["CALCRL", "RAMP2", "F8", "PECAM1", "CD32B", "LYVE-1", "STAB2", "CD14", "VWF", "ENG", "VEGFR3", "ITGA2", "ITGA3", "ITGB4", "RBP1", "ACTA2", "TAGLN", "MYL9", "DCN", "COL1A1", "COL3A1"," COL1A2"]
    #genes = ["FCGR2B", "LYVE1", "FLT4"]
    #genes = ["SOX9", "EPCAM", "KRT19", "CD24", "CLDN10", "CLDN4", "CXCL1", "CFTR"]
    #genes = ["SOX9", "EPCAM", "KRT19", "CD24", "CLDN10", "CLDN4", "CXCL1", "CFTR"]
    #genes = ["CPE", "IBSP", "VCAN", "GGT5", "CSPG4", "EDIL3", "AEBP1", "FOXC1", "STAB1", "STAB2", "TSPAN7", "CCL14"]
    genes = ["TPM1", "NOTCH3", "DES", "VIM", "COL1A1", "CALD1", "CNN1", "ACTA2", "TAGLN", "RGS5", "TNNT1", "CDH15", "PAX7", "DLC1", "PDGFRA", "PDGFRB", "VCAM1", "CXCL12", "AVEN", "SOX9", "RUNX2", "CDH5", "KDR", "STAB1", "STAB2", "EMCN", "PDPN", "LYVE1", "CD68", "CDH11", "MPZ", "NRXN1"]

    adata = sc.read_h5ad(outputs)
    name = "Healthy_Liver_forNiche"
    dict_markers = {
            "Smooth muscle cells/myofibroblasts": ["TPM1", "NOTCH3", "DES", "VIM", "COL1A1", "CALD1", "CNN1", "ACTA2", "TAGLN", "RGS5", "PDGFRB"],
            "Chondrocytes": ["ACAN", "SOX9", "RUNX2", "SPP1", "EDIL3", "CSPG4", "PRRX1"],
            "CAR": ["PDGFRA", "CXCL12", "DCN"],
            "Endothelial cells": ["CDH5", "KDR", "PECAM1"],
            "Sinusoidal endothelial": ["STAB1", "STAB2", "LYVE1", "TSPAN7", "EMCN", "PDPN"],
            "Arterial endothelial": ["GJA5"],
            "Muscle cells": ["TNNT1", "CDH15", "PAX7", "MYOD1"],
            "Schwann cells": ["MPZ", "NRXN1"],
            "Osteoclasts": ["CD68", "CTSK", "ACP5", "MMP9", "SIGLEC15", "ANPEP"],
            "Osteoblasts": ["CDH11", "CPE", "IBSP", "BGLAP", "COL1A1", "SP7"],
            "Other": ["NES", "LEPR", "KITLG"],
            "Proliferating cells": ["MKI67"],
            "Osteochondral precursors": ["AEBP1", "FOXC1"]
    }
    genes = list()

    for key, value in dict_markers.items():
        for v in value:
            genes.append(str(v))

    #genes = ["CD34", "SPINK2", "CD52", "SELL", "MLLT3", "GATA1", "KLF1", "CTNNBL1", "TESPA1", "TFRC", "MPO", "AZU1", "LYZ", "SPI1", "GATA2", "HDC", "CPA3", "CD14", "CD163", "MARCO", "ITGA2B", "MKI67", "IL7R", "CD3E", "CD79A", "CD79B", "CD19", "PAX5", "ALAS2", "HBA1", "AHSP", "GYPA", "BPGM", "DHFR", "UHRF1", "VPREB1", "RAG1-", "DNTT", "MME", "CD38", "IGHM", "TRAC", "IL2RG", "RORC", "KIT", "SPIB", "IGLL1", "CD24"]
    #genes = ["CALCRL", "RAMP2", "F8", "PECAM1",
    #         "CD32B", "LYVE-1", "STAB2", "CD14", "VWF", "ENG", "VEGFR3", "ITGA2", "ITGA3", "ITGB4",
    #         "SOX9", "EPCAM", "KRT19", "CD24", "CLDN10", "CLDN4", "CXCL1", "CFTR", "CDH5", "KDR"
    #]
    #genes = ["CD34", "SPINK2", "CD52", "SELL", "MLLT3", "GATA1", "KLF1", "CTNNBL1", "TESPA1", "TFRC", "MPO", "AZU1", "LYZ", "SPI1", "GATA2", "HDC", "CPA3", "CD14", "CD163", "MARCO", "ITGA2B", "MKI67", "IL7R", "CD3E", "CD79A", "CD79B", "CD19", "PAX5", "ALAS2", "HBA1", "AHSP", "GYPA", "BPGM", "DHFR", "UHRF1", "VPREB1", "RAG1-", "DNTT", "MME", "CD38", "IGHM", "TRAC", "IL2RG", "RORC", "KIT", "SPIB", "IGLL1", "CD24"]
    adata = sc.read_h5ad(outputs)
    name = "Healthy_Femur"
    genes = ["TPM1", "NOTCH3", "DES", "VIM", "COL1A1", "CALD1", "CNN1", "ACTA2", "TAGLN", "RGS5", "PDGFRB",
             "ACAN", "SOX9", "RUNX2", "SPP1", "EDIL3", "CSPG4", "PRRX1",
             "PDGFRA", "CXCL12", "DCN",
             "CDH5", "KDR", "PECAM1",
             "STAB1", "STAB2", "LYVE1", "TSPAN7", "EMCN", "PDPN",
             "GJA5",
             "TNNT1", "CDH15", "PAX7", "MYOD1",
             "MPZ", "NRXN1",
             "CD68", "CTSK", "ACP5", "MMP9", "SIGLEC15", "ANPEP",
             "CDH11", "CPE", "IBSP", "BGLAP", "COL1A1", "SP7",
             "NES", "LEPR", "KITLG",
             "MKI67",
             "AEBP1", "FOXC1"]

    genes = ["PTPRC", "CD3E", "CD3D", "TRAC", "IL7R", "BCL11A", "IL2RG", "CLEC9A", "BATF3",
             "CD1C", "CLEC4A", "CLEC10A", "IRF4", "NKG7", "PRF1", "GZMA", "KLRB1", "GZMM",
             "IL2RB", "TRBC1", "MMP9", "CD79A", "IGHM", "IGLL", "LYZ", "CD14", "CD68", "MARCO",
             "CD163", "S100A9", "MNDA", "FCN1", "MPEG1", "MS4A7", "CTSB", "CPVC", "HMOX1",
             "DCN", "COL1A1", "COL3A1", "RBP1", "ACTA2", "TAGLN", "MYL9", "SOX9", "EPCAM",
             "KRT19", "CD24", "CLDN10", "CLDN4", "CXCL1", "CFTR", "ALB", "APOE", "AFP",
             "CDH5", "KDR"]
    rpdf = mpdf.PdfPages(outDir+"{0}_Ridge_Leiden_v2.pdf".format(name))
    #pdf = mpdf.PdfPages(outDir+"{0}_Violin_Leiden_v4_Extra2.pdf".format(name))
    for gene in genes:
        print(gene)
        if gene not in adata.var.index:
            print(gene, " NOT FOUND")
        else:
            #fc.checkMarkerGenesPerCluster(adata, [gene], group_by="leiden_v2")
            fc.ridge_plot(adata, genes=[gene], group_by="leiden_v2", height=0.4, palette=myColors, pdf=rpdf)
            plt.close()
        # Plot violin of all cell types and UMAP, for a specific gene, on a singl page
        #f, axs = plt.subplots(1,1,figsize=(32,8))
        #fc.plotViolinVariableGroup(adata, gene, pointSize=0, group_by="leiden_v2", ax=axs[0], use_raw=True, rotation=90, pdf=pdf)
        #fc.plotUMAP(adata, variable=gene, colormap=mymap, ax=axs[1], components=["2,3"], use_raw=True, pdf=pdf)
        #plt.tight_layout()
        #plt.close("all")
    #pdf.close()
    rpdf.close()
    #fc.plotPieCharts(adata, variable="annotations", group_by="leiden", cols=2, palette=myColors, normalise=True, write_to=outDir+"PieCharts_Scanorama_Merged_Annot_group_Leiden_withNorm")
    #fc.plotPieCharts(adata, variable="leiden", group_by="annotations", cols=2, palette=myColors, normalise=True, write_to=outDir+"PieCharts_Scanorama_Merged_Leiden_group_Annot_withNorm")

    adata = sc.read_h5ad(outputs)

    genes = ["CD34", "SPINK2", "CD52", "SELL", "MLLT3", "GATA1", "KLF1", "CTNNBL1", "TESPA1", "TFRC", "MPO", "AZU1", "SPI1", "LYZ", "GATA2", "HDC", "CPA3", "ITGA2B", "GP9", "PLEK",
             "AHSP", "ALAS2", "HBA1", "GYPA", "BPGM", "CD3E", "CD3D", "TRAC", "IL7R", "BCL11A", "IL2RG", "RORC", "KIT", "IL7R", "MPEG1", "JCHAIN", "IRF8", "IRF7", "CLEC4C", "IL3RA",
             "CLEC9A", "BATF3", "BTLA", "CD1C", "CLEC4A", "CLEC10A", "IRF4", "NKG7", "PRF1", "GZMA", "KLRB1", "GZMM", "IL2RB", "TRBC1", "IL7R", "DHFR", "UHRF1", "DNTT",
             "PAX5", "MME", "VPREB1", "RAG1", "RAG2", "CD38", "CD79A", "IGHM", "IGLL1", "SPIB", "CD24", "CD19", "CD14", "CD68", "MARCO", "CD163", "MPEG1", "MS4A7", "CTSB", "CD5L", "HMOX1",
             "S100A9", "MNDA", "FCN1", "COL1A1", "MKI67",
             "DCN", "COL1A1", "COL3A1", "RBP1", # Liver niche starts
             "ACTA2", "TAGLN", "MYL9", "SOX9", "EPCAM", "KRT19", "CD24", "CLDN10", "CLDN4", "CXCL1", "CFTR", "ALB", "APOE", "AFP", "GPC3", "CDH5", "KDR",
             "STAB1", "STAB2", "LYVE1", "FCGR2B", "KLF2", "CALCRL", "RAMP2"]

             #"TPM1", "NOTCH3", "DES", "VIM", "COL1A1", "CALD1", "CNN1", "ACTA2", "TAGLN", "RGS5", "PDGFRB", # Femur niche starts
             # "ACAN", "SOX9", "RUNX2", "SPP1", "EDIL3", "CSPG4", "PRRX1", "PDGFRA", "CXCL12", "DCN", "CDH5", "KDR", "PECAM1",
             # "STAB1", "STAB2", "LYVE1", "TSPAN7", "EMCN", "PDPN", "GJA5", "TNNT1", "CDH15", "PAX7", "MYOD1",
             # "MPZ", "NRXN1", "CD68", "CTSK", "ACP5", "MMP9", "SIGLEC15", "ANPEP", "CDH11", "CPE", "IBSP", "BGLAP", "COL1A1", "SP7",
             # "NES", "LEPR", "KITLG", "MKI67", "AEBP1", "FOXC1"]

    name = "DS_Liver"
    import matplotlib
    matplotlib.rcdefaults()
    ax = sc.pl.dotplot(adata,
                   genes,
                   groupby="leiden_v6",
                   standard_scale='var',
                   smallest_dot=0.0,
                   dot_min=None,
                   dot_max=None,
                   color_map='Reds',
                   dendrogram=False,
                   figsize=(12,25),
                   show=True,
                   save="{0}_Leiden_v6.png".format(name),
                   linewidths=2,swap_axes=True)

    rpdf = mpdf.PdfPages(outDir+"{0}_Ridge_Leiden_v5.pdf".format(name))
    for gene in genes:
        print(gene)
        if gene not in adata.var.index:
            print(gene, " NOT FOUND")
        else:
            fc.ridge_plot(adata, genes=[gene], group_by="leiden_v5", height=0.4, palette=myColors, pdf=rpdf)
            plt.close()
    rpdf.close()


    # Liver blood
    blood = [
             'Cycling HSCs/MPPs',
             'Early erythroid cells',
             'HSCs/MPPs',
             'MEMPs',
             'Megakaryocytes',
             'Mast cells',
             'Late erythroid cells',
             'cDC2',
             'pDCs',
             'Monocyte progenitors',
             'NK cells',
             'B cells',
             'Cycling pDCs',
             'NK progenitors',
             'Neutrophil progenitors',
             'Cycling megakaryocytes',
             'Inflammatory macrophages',
             'Cycling B cells',
             'Kupffer cells'
    ]

    # Femur blood
    blood = [
            "CAR cells",
            "Early erythroid cells",
            "Granulocyte progenitors",
            "Cycling neutrophils",
            "Neutrophils",
            "NK cells",
            "Inflammatory macrophages",
            "Tolerogenic macrophages",
            "cDC2",
            "Proliferating NK cells",
            "Early lymphoid cells",
            "Osteoclasts",
            "Collagen+ myeloid cells",
            "Mast cells",
            "Megakaryocytes",
            "MEMPs",
            "Erythroid progenitors",
            "Cycling MEMPs",
            "Cycling pDCs",
            "Pro B cells",
            "B cells",
            "Pre pro B cells",
            "Late erythroid cells",
    ]
    dict_markers_blood = {
            'HSCs/MPPs': ['CD34', 'SPINK2', 'MLLT3'],
            'MEMPs': ['GATA1', 'KLF1', 'TESPA1'],
            #'Cycling HSCs/MPPs': ['CD34', 'SPINK2', 'MLLT3'],
            'Cycling MEMPs': ['GATA1', 'KLF1', 'TESPA1'],
            'Early erythroid cells': ['AHSP', 'ALAS2', 'HBA1', 'GYPA'],
            'Late erythroid cells': ['AHSP', 'ALAS2', 'HBA1', 'GYPA'],
            #'Cycling erythroid cells': ['AHSP', 'ALAS2', 'HBA1', 'GYPA'],
            'Mast cells': ['GATA2', 'HDC', 'CPA3'],
            'Megakaryocytes': ['ITGA2B', 'GP9', 'PLEK'],
            #'Cycling Megakaryocytes': ['ITGA2B', 'GP9', 'PLEK'],
            'Granulocyte progenitors': ['MPO', 'AZU1', 'SPI1', 'LYZ'],
            'Monocyte progenitors': ['LYZ', 'CD14'],
            'Inflammatory macrophages': ['S100A9', 'MNDA', 'FCN1', 'CD68'],
            'Kupffer cells': ['CD163', 'MS4A7', 'CTSB', 'HMOX1'],
            'NK progenitors': ['NKG7', 'PRF1', 'GZMA'],
            'NK cells': ['NKG7', 'PRF1', 'GZMA'],
            'Pre pro B cells': ['IL7R', 'DHFR', 'PAX5', 'MME', 'IGLL1', 'CD79A', 'IGHM'],
            'Pro B cells': ['IL7R', 'DHFR', 'PAX5', 'MME', 'IGLL1', 'CD79A', 'IGHM'],
            #'Cycling B cells': ['IL7R', 'DHFR', 'PAX5', 'MME', 'IGLL1', 'CD79A', 'IGHM'],
            'B cells': ['IL7R', 'DHFR', 'PAX5', 'MME', 'IGLL1', 'CD79A', 'IGHM'],
            'pDCs': ['JCHAIN', 'IRF8', 'CLEC4C', 'IL3RA'],
            'Cycling pDCs': ['JCHAIN', 'IRF8', 'CLEC4C', 'IL3RA'],
            'Cycling pDCs': ['JCHAIN', 'IRF8', 'CLEC4C', 'IL3RA'],
            'cDC2': ['CD1C', 'CLEC4A', 'CLEC10A'],
            'Cycling': ['MKI67']
    }

    adata_b = adata[adata.obs["leiden_v5"].isin(blood), :]
    name = "DS_Femur_Blood"
    import matplotlib
    matplotlib.rcdefaults()
    ax = sc.pl.dotplot(adata_b,
                   dict_markers_blood,
                   groupby="leiden_v5",
                   standard_scale='var',
                   smallest_dot=0.0,
                   dot_min=None,
                   dot_max=None,
                   color_map='Reds',
                   dendrogram=False,
                   figsize=(12,25),
                   show=True,
                   save="{0}_Leiden_v5.png".format(name),
                   linewidths=2,swap_axes=True)

    # Liver niche
    niche = [
             'LSECs',
             'Hepatocytes',
             'Hepatic stellate cells',
             'Vascular endothelial cells',
             'Activated stellate cells'
    ]

    # Femur niche
    niche = [
        'Vascular endothelial cells',
        'Striated muscle cells',
        'Sinusoidal endothelial cells',
        'Fibroblasts',
        'Myofibroblasts',
        'Schwann cells',
        'Transitioning endothelial cells',
        'Smooth muscle cells',
    ]
    dict_markers_niche = {
            #'Hepatocytes': ['ALB', 'AFP'],
            #'Hepatic stellate cells': ['DCN', 'COL1A1', 'COL3A1', 'RBP1'],
            #'Activated stellate cells': ['ACTA2', 'TAGLN', 'MYL9'],
            'Vascular endothelial cells': ['CDH5', 'KDR', 'CD34'],
            'Sinusoidal endothelial cells': ['STAB1', 'STAB2', 'LYVE1'],
            'Fibroblasts': ["COLEC11", "SPARC", "ASPN"],
            #'Cholangiocytes': ['KRT19']
    }

    adata_n = adata[adata.obs["leiden_v5"].isin(niche), :]
    name = "DS_Femur_Niche"
    ax = sc.pl.dotplot(adata_n,
                   dict_markers_niche,
                   groupby="leiden_v5",
                   standard_scale='var',
                   smallest_dot=0.0,
                   dot_min=None,
                   dot_max=None,
                   color_map='Reds',
                   dendrogram=False,
                   figsize=(12,15),
                   show=True,
                   save="{0}_Leiden_v5.png".format(name),
                   linewidths=2,swap_axes=True)
    '''

    adata = sc.read_h5ad("/nfs/research1/gerstung/nelson/data/downsyndrome/ScanpyObjects/merged_batchcorr_clustered/10X_Healthy_Liver_v3.h5ad")
    '''
    liver_blood_ds = [
                "Cycling HSCs/MPPs",
                "Early erythroid cells",
                "HSCs/MPPs",
                "MEMPs",
                "Megakaryocytes",
                "Mast cells",
                "Late erythroid cells",
                "cDC2",
                "pDCs",
                "Monocyte progenitors",
                "NK cells",
                "B cells",
                "Cycling B cells",
                "NK progenitors",
                "Granulocyte progenitors",
                "Cycling megakaryocytes",
                "Cycling pDCs",
                "Inflammatory macrophages",
                "Kupffer cells"
    ]
    '''
    liver_blood_ds = [
            "Mast cells",
            "B cells",
            "NK cells",
            "cDC2",
            "Monocyte progenitors",
            "Megakaryocytes",
            "MEMPs",
            "NK progenitors",
            "Kupffer cells",
            "pDCs",
            "Pro B cells",
            "Pre pro B cells",
            "Hepatocytes",
            "Cycling erythroid cells",
            "Early erythroid cells",
            "Neutrophils",
            "Cycling MEMPs",
            "HSCs/MPPs",
            "Inflammatory macrophages",
    ]

    '''
    liver_niche_ds = [
               "LSECs",
               "Hepatocytes",
               "Hepatic stellate cells",
               "Vascular endothelial cells",
               "Activated stellate cells",
    ]
    '''

    liver_niche_ds = [
            "LSECs",
            "Granulocyte progenitors",
            "Vascular endothelial cells",
            "Late erythroid cells",
            "Activated stellate cells",
            "Hepatic stellate cells",
            "Cholangiocytes",
    ]

    genes_liver_blood_ds = ["CD34", "SPINK2", "MLLT3", "GATA1", "KLF1", "TESPA1", "AHSP", "ALAS2", "GYPA", "GATA2", "HDC", "CPA3", "ITGA2B", "GP9", "PLEK", "MPO", "AZU1", "SPI1", "LYZ", "LYZ", "SPI1", "CD14", "CD68", "S100A9", "MNDA", "FCN1", "CD163", "MS4A7", "CTSB", "HMOX1", "NKG7", "PRF1", "GZMA", "IL7R", "DHFR", "PAX5", "MME", "IGLL1", "IGHM", "CD79A", "CD19", "JCHAIN", "IRF8", "CLEC4C", "IL3RA", "CD1C", "CLEC4A", "CLEC10A", "MKI67"]
    genes_liver_niche_ds = ["ALB", "AFP", "DCN", "COL1A1", "COL3A1", "RBP1", "ACTA2", "TAGLN", "MYL9", "CDH5", "KDR", "CD34", "STAB1", "STAB2", "LYVE1", "KRT19"]

    import matplotlib
    matplotlib.rcdefaults()
    adata_ds_blood = adata[adata.obs["leiden_v6"].isin(liver_blood_ds),:]
    ax = sc.pl.dotplot(adata_ds_blood,
                        genes_liver_blood_ds,
                        groupby="leiden_v6",
                        standard_scale='var',
                        smallest_dot=0.0,
                        dot_min=None,
                        dot_max=None,
                        color_map='Reds',
                        dendrogram=False,
                        figsize=(12,25),
                        show=True,
                        save="Healthy_Liver_Blood_Leiden_v6.png",
                        linewidths=2,swap_axes=True)

    adata_ds_niche = adata[adata.obs["leiden_v6"].isin(liver_niche_ds),:]
    ax = sc.pl.dotplot(adata_ds_niche,
                        genes_liver_niche_ds,
                        groupby="leiden_v6",
                        standard_scale='var',
                        smallest_dot=0.0,
                        dot_min=None,
                        dot_max=None,
                        color_map='Reds',
                        dendrogram=False,
                        figsize=(12,25),
                        show=True,
                        save="Healthy_Liver_Niche_Leiden_v6.png",
                        linewidths=2,swap_axes=True)


if __name__=="__main__":
    main()
