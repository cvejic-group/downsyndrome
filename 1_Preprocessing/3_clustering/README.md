# Clustering and subclustering

```run_clustering_heatmap.py``` and ```run_preliminary_merging.py``` respectively take the batch-corrected ```h5ad``` outputs and perform
Leiden clustering on them. The Leiden clustering is specified by a ```resolution``` parameter, default set to 1.0. Increasing this parameter
will increase the number of clusters constructed from Leiden's community detection algorithm. Subclustering on these clusters can be further
applied using kNN, which is relevant when the data are underclustered in the cell type assignment. Once clustering has been performed, 
```run_clustering_heatmap.py``` also calculates key marker genes using a Wilcoxon Rank-Sum test, and these genes are plotted across
all relevant clusters as both Violin and Ridge plots. 

This part of the code framework is generally applied iteratively:
1) Run  ```run_clustering_heatmap.py``` to generate an initial clustering.
2) Iterate with an expert biologist, selecting several marker genes based on Ridge/Violin plots, and start naming clusters
3) Use the merging_dict to assign clusters cell names.
4) Use the kNN subclustering to split existing clusters further if e.g. a given cluster has a mixed expression signature of
different cell marker genes. 
5) Run ```run_subclustering_heatmap.py``` to apply steps 3) and 4).
6) Recalculate relevant marker genes for the new clusters, and repeat the checks with the expert biologist.


