# Down syndrome scRNA-Seq Analysis

This directory contains the core Python code for processing the scRNA-Seq foetal data. In addition to the baseline scRNA-Seq analysis
(merging matrices, performing basic QC and gene filtering, highly-variable gene selection, dimensionality reduction, batch correction, 
clustering), several directories also contain downstream analysis code for performing additional studies. This analysis
pipeline is build on the ```ScanPy``` framework (https://genomebiology.biomedcentral.com/articles/10.1186/s13059-017-1382-0). Let's briefly summarise the directory structure. 

## 1) Baseline single-cell RNA-sequencing analysis code
a) 0_emptyDrop --> applies empty drops removal (https://genomebiology.biomedcentral.com/articles/10.1186/s13059-019-1662-y) 
on individual CellRanger gene expression matrices, adding a new directory to the CellRanger input path with the updated matrix.

b) 1_mergingAnQualityControl --> takes the empty drops outputs, merges all matrices, and removes genes with low expression profiles, 
poor reads/cell profile, and also applies doublet removal with Scrublet (https://www.cell.com/cell-systems/pdfExtended/S2405-4712(18)30474-5). 
Outputs a filtered h5ad file with the combined matrix, containing healthy, background, and tumour data. 

c) 2_batchCorrection --> splits the merged data matrix into tumour and healthy+background, extracts highly-variable genes, performs PCA
and applies batch correction with Harmony (https://genomebiology.biomedcentral.com/articles/10.1186/s13059-019-1850-9, 
https://www.nature.com/articles/s41592-019-0619-0) to the top PCs.

d) 3_clustering --> applies Leiden clustering to the batch-corrected outputs, and generates final UMAPs used for cell type
annotations. Also calls several ```ScanPy``` functions, used to make heatmaps, call marker genes with a Wilcoxon Rank-Sum, and generate
some nice Violin and Ridge plots.
