import warnings
warnings.simplefilter("ignore")

import sys

import numpy as np
import scanpy as sc
import scanpy.external as sce
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
import matplotlib.backends.backend_pdf as mpdf

from math import log
from scipy import sparse
from matplotlib import cm
from matplotlib import colors
from scipy.sparse import issparse

import scanorama
import harmonypy as hm

sys.path.append("/nfs/research1/gerstung/nelson/downsyndrome/Functions/")
from scRNA_functions import scRNA_functions
fc = scRNA_functions()

sys.path.append("/nfs/research1/gerstung/nelson/downsyndrome/PythonPipeline/2_batchCorrection/")
from run_hvg_pca_harmony import runUMAP # Want the UMAP plotting method

myColors = ['#e6194b', '#3cb44b', '#ffe119', '#4363d8', '#f58231', '#911eb4', '#46f0f0', '#f032e6', '#bcf60c', '#fabebe',
            '#008080', '#e6beff', '#9a6324', '#fffac8', '#800000', '#aaffc3', '#808000', '#ffd8b1', '#000075', '#808080',
            '#307D7E', '#000000', '#DDEFFF', '#000035', '#7B4F4B', '#A1C299', '#300018', '#C2FF99', '#0AA6D8', '#013349',
            '#00846F', '#8CD0FF', '#3B9700', '#04F757', '#C8A1A1', '#1E6E00', '#DFFB71', '#868E7E', '#513A01', '#CCAA35',
            '#800080', '#DAA520', '#1E90FF', '#3CB371', '#9370DB', '#8FBC8F', '#00FF7F', '#0000CD', '#556B2F', '#FF00FF',
            '#CD853F', '#6B8E23', '#008000', '#6495ED', '#00FF00', '#DC143C', '#FFFF00', '#00FFFF', '#FF4500', '#4169E1',
            '#48D1CC', '#191970', '#9ACD32', '#FFA500', '#00FA9A', '#2E8B57', '#40E0D0', '#D2691E', '#66CDAA', '#FFEFD5',
            '#20B2AA', '#FF0000', '#EEE8AA', '#BDB76B', '#E9967A', '#AFEEEE', '#000080', '#FF8C00', '#B22222', '#5F9EA0',
            '#ADFF2F', '#FFE4B5', '#7B68EE', '#7FFFD4', '#0000FF', '#BA55D3', '#90EE90', '#FFDAB9', '#6A5ACD', '#8B0000',
            '#8A2BE2', '#CD5C5C', '#F08080', '#228B22', '#FFD700', '#006400', '#98FB98', '#00CED1', '#00008B', '#9400D3',
            '#9932CC', '#4B0082', '#F0E68C', '#483D8B', '#008B8B', '#8B008B', '#4682B4']

# Define a nice colour map for gene expression
colors2 = plt.cm.Reds(np.linspace(0, 1, 128))
colors3 = plt.cm.Greys_r(np.linspace(0.7,0.8,20))
colorsComb = np.vstack([colors3, colors2])
mymap = colors.LinearSegmentedColormap.from_list('my_colormap', colorsComb)
sns.set_style("white")

# Inputs and Outputs
outDir = "/nfs/research1/gerstung/nelson/outputs/downsyndrome/2_batchCorrection/"
inputs = '/nfs/research1/gerstung/nelson/data/downsyndrome/ScanpyObjets/merged/'
outputs = '/nfs/research1/gerstung/nelson/data/downsyndrome/ScanpyObjets/merged_batchcorr/'

'''
batch_dict = {
       "FCAImmP7277561": 0,
       "FCAImmP7528286": 1,
       "FCAImmP7528288": 2,
       "FCAImmP7528295": 3,
       "FCAImmP7579211": 4,
       "FCAImmP7579223": 5,
       "FCAImmP7579227": 6,
       "FCAImmP7277560": 7,
       "FCAImmP7292032": 8,
       "FCAImmP7528287": 9,
       "FCAImmP7528289": 10,
       "FCAImmP7579210": 11,
       "FCAImmP7579222": 12,
       "FCAImmP7579226": 13,
       "OT10xRNA8761787": 14,
       "OT10xRNA8761795": 15,
       "SIGAA2": 16,
       "SIGAB2": 17,
       "SIGAC2": 18,
       "SIGAD2": 19,
       "SIGAE2": 20,
       "OT10xRNA9469345": 21,
       "OT10xRNA9469346": 22,
       "OT10xRNA9469347": 23,
       "OT10xRNA9469362": 24,
       "OT10xRNA9469363": 25
    }
'''
batch_dict = {
       "OT10xRNA8761787": 0,
       "OT10xRNA8761795": 1,
       "SIGAA2": 2,
       "SIGAB2": 3,
       "SIGAC2": 4,
       "SIGAD2": 5,
       "SIGAE2": 6,
       "OT10xRNA9469345": 7,
       "OT10xRNA9469346": 8,
       "OT10xRNA9469347": 9,
       "OT10xRNA9469362": 10,
       "OT10xRNA9469363": 11,
       "15582A": 12,
       "15582B": 13
    }

def add_hvgs(adata, multiome=False):

    if multiome:
        # Recalculate based only on gene counts (this is relevant for multiome)
        adata = adata[:, adata.var["Ensembl"].astype(str).str.startswith("ENSG")]
        adata.X = adata.layers["counts"]
        sc.pp.normalize_per_cell(adata, counts_per_cell_after=1e4)
        adata.layers["norm"] = np.asarray(adata.X).copy()
        sc.pp.log1p(adata)
        adata.layers["log"] = np.asarray(adata.X.copy())
        adata.raw = adata.copy()
        sc.pp.scale(adata, max_value=10)
        adata.layers["scale"] = np.asarray(adata.X.copy())

    adata.X = adata.layers["log"]
    sc.pp.highly_variable_genes(adata, min_mean=0.00125, max_mean=3, min_disp=0.5, batch_key="batch")
    return adata

def main():
    # Data integration strategy
    # 1. Separately identify the HVGs in healthy and DS
    # 2. Combine all cells and HVGs
    # 3. Run PCA on the combined data
    # 4. Run Scanorama across all batches, combining batches across healthy and DS

    adata_rna = sc.read_h5ad(inputs+"10X_DownSyndrome.h5ad")
    adata_rna = add_hvgs(adata_rna)
    adata_rna.X = adata_rna.layers["log"]
    sc.pp.regress_out(adata_rna, ['total_counts', 'n_genes_by_counts'])

    adata_atac = sc.read_h5ad(inputs+"10X_Multiome_DownSyndrome.h5ad")
    adata_atac = add_hvgs(adata_atac, multiome=True)
    adata_atac.X = adata_atac.layers["log"]
    sc.pp.regress_out(adata_atac, ['total_counts', 'n_genes_by_counts'])

    # Combine the new data-sets
    adatas = [adata_rna, adata_atac]
    for adata in adatas:
        # PCA
        adata.X = adata.layers["scale"]
        sc.tl.pca(adata, n_comps=50, svd_solver='auto', use_highly_variable=True)

    # Apply Scanorama integration + Harmony for batch removal
    scanorama.integrate_scanpy(adatas)
    adata = adatas[0].concatenate(adatas[1], join='inner', index_unique="-") # Inner product for now
    adata.obs['batch'] = np.array([batch_dict[s] for s in adata.obs['sample']])
    adata.obs['batch'] = adata.obs['batch'].astype('category')
    adata.var['highly_variable'] = adata.var['highly_variable-0'] | adata.var['highly_variable-1']
    del adata.var['highly_variable-0']
    del adata.var['highly_variable-1']

    ho = hm.run_harmony(adata.obsm['X_scanorama'][:, :20], # Harmonize top 20 components
                    adata.obs,
                    ["batch", "patient", "sample"],
                    max_iter_kmeans=25,
                    max_iter_harmony=500)

    adata.obsm["X_scanorama_harmonize"] = ho.Z_corr.T

    sc.pp.neighbors(adata,
                n_pcs=adata.obsm["X_scanorama_harmonize"].shape[1],
                use_rep="X_scanorama_harmonize",
                knn=True,
                random_state=42,
                method='umap',
                metric='euclidean')

    runUMAP(adata,n_components=3, pdf=None, name_3D="DS_scRNAseq_scATACseq_AfterScanorama_HarmonyBatch")

    adata.write(outputs+"10X_DownSyndrome_Int_Multiome_RNAseq_Scanorama_Harmony.h5ad")

if __name__=="__main__":
    main()
