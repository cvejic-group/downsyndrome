# Batch correction

The ```run_hvg_pca_harmony.py``` can be run directly from the command line, and takes as input the ```h5ad``` merged output. Before performing
highly-variable gene selection, it is necessary to select the relevant environment by slicing out with the ```Down syndrome``` or  ```Healthy```
observations in ```adata.obs```. Following this, the code applies the usual gene selection, PCA, and batch selection.
By default ```run_hvg_pca_harmony.py``` runs batch correction with Harmony, but batch correction may be deactivated by using the ```ArgmentParser```:

```
python run_hvg_pca_harmony.py --harmony_off
```

A script called ```batch_submit.py``` is also provided for running on a ```bigmem``` batch farm. This is recommended for large data-sets as batch 
correction with Harmony is computationally intensive. 
