from os import system as sys

fname = "Healthy_Full_Batch"
f = open(fname+".sh", 'w')
f.write("cd /nfs/research1/gerstung/nelson/downsyndrome/PythonPipeline/2_batchCorrection/\n")
f.write("conda activate minimal_env\n" )
f.write("python run_hvg_pca_harmony.py")
#f.write("python run_hvg_pca_harmony.py --cells={0}".format(cells))
f.close()

print("bsub -P bigmem -M 1000000 -o {0}.txt -e {0} bash {0}.sh".format(fname))
sys("bsub -P bigmem -M 1000000 -o {0}.txt -e {0} bash {0}.sh".format(fname))
