#!/bin/bash

set -e 

infile="" # Initial value set as empty, further set with input file path
outdir="" # Initial value set as empty, further set with output folder path
gpu="" # Initial value set as empty, futher set as --cuda if with -g
learnrate="1e-4" # Initial value set as 1e-4 same of default value as CellBender 0.3.0

# Argument parsing
while getopts "i:o:r:g" opt
do
    case "$opt" in
        i) infile=${OPTARG};;
        o) outdir=${OPTARG};;
	r) learnrate=${OPTARG};;
        g) gpu="--cuda";;
	*) echo -e "\033[1;31mERROR: invalid option, only accept -i, -o, -r or -g.\033[0m" 1>&2; exit 1;;
    esac
done
if [ -z "$infile" ] || [ -z "$outdir" ]
then
	echo -e "\033[1;31mERROR: missing input or output.\033[0m" 1>&2
	exit 1
fi

# Execution information
echo "Input file: $infile"
echo "Output folder: $outdir"
if [ -z "$gpu" ]
then
	echo "GPU mode: False"
else
	echo "GPU mode: $gpu"
fi
echo "Learning rate: $learnrate"

# Actual workflow
mkdir -p $outdir
cellbender remove-background --learning-rate $learnrate $gpu --input $infile --output $outdir/corrected.h5
rm -f $outdir/corrected.pdf $outdir/corrected_cell_barcodes.csv \
      $outdir/corrected_posterior.h5 $outdir/corrected.h5 $outdir/ckpt.tar.gz # remove to save space
