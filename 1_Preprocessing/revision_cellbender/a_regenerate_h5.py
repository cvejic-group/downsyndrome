from numba.core.errors import NumbaDeprecationWarning
import warnings
warnings.simplefilter("ignore", category=NumbaDeprecationWarning)

import sys
import os
import pegasus as pg


if __name__ == "__main__":
    # Argument parsing
    if len(sys.argv) < 3:
        print("\033[1;31mERROR: missing input folder or output file.\033[0m")
        sys.exit(1)
    crfolder, outfile = sys.argv[1:3]
    print(f"Input: {crfolder}\nOutput: {outfile}\n")

    # Folder existence check
    if not os.path.exists(crfolder):
        print(f"\033[1;31mERROR: Cellranger output folder not found ({crfolder}).\033[0m",
              file=sys.stderr)
        sys.exit(1)
    infolder = f"{crfolder}/raw_feature_bc_matrix/"
    if not os.path.exists(infolder):
        print(f"\033[1;31mERROR: raw_feature_bc_matrix/ not found in input folder.\033[0m",
              file=sys.stderr)
        sys.exit(1)
    
    # Actual workflow
    data = pg.read_input(infolder, genome="GRCh38")
    if os.path.exists(f"{crfolder}raw_feature_bc_matrix.h5"):
        assert not (data.X != pg.read_input(f"{crfolder}raw_feature_bc_matrix.h5").X).todense().any()
    pg.write_output(data, outfile)
