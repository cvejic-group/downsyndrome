import warnings
warnings.simplefilter("ignore")

import sys
import bbknn
import numpy as np
import scanpy as sc
import pandas as pd
import seaborn as sns
import scrublet as scr
import matplotlib.pyplot as plt
import matplotlib.backends.backend_pdf as mpdf

from math import log
from scipy import sparse
from matplotlib import cm
from matplotlib import colors
from scipy.sparse import issparse
from sklearn.utils import shuffle
from scipy.spatial import distance
from sklearn.preprocessing import normalize
from sklearn.model_selection import train_test_split

sys.path.append("/nfs/research1/gerstung/nelson/downsyndrome/Functions/")

# Get the bespoke analysis functions
from scRNA_functions import scRNA_functions
fc = scRNA_functions()

# Dictionary of all patient imformation
#from multiomeMetadata import multiomeMetadata
from patientMetadata_full import patientMetadata
#from patientMetadata_healthy import patientMetadata
#from foetalMetadata import patientMetadata

# Global colour settings
myColors = ['#e6194b', '#3cb44b', '#ffe119', '#4363d8', '#f58231',
        '#911eb4', '#46f0f0', '#f032e6', '#bcf60c', '#fabebe',
        '#008080', '#e6beff', '#9a6324', '#fffac8', '#800000',
        '#aaffc3', '#808000', '#ffd8b1', '#000075', '#808080',
        '#307D7E', '#000000', "#DDEFFF", "#000035", "#7B4F4B",
        "#A1C299", "#300018", "#C2FF99", "#0AA6D8", "#013349",
        "#00846F", "#8CD0FF", "#3B9700", "#04F757", "#C8A1A1",
        "#1E6E00", "#DFFB71", "#868E7E", "#513A01", "#CCAA35"]

myColorTissues = ['#ffe119', '#3cb44b', '#e6194b']

# Define a nice colour map for gene expression
colors2 = plt.cm.Reds(np.linspace(0, 1, 128))
colors3 = plt.cm.Greys_r(np.linspace(0.7,0.8,20))
colorsComb = np.vstack([colors3, colors2])
mymap = colors.LinearSegmentedColormap.from_list('my_colormap', colorsComb)
sns.set_style("white")

# Inputs and outputs
folder = "/nfs/research1/gerstung/nelson/data/downsyndrome/Samples/10X/"
#folder = "/nfs/research1/gerstung/nelson/data/scATACseq/downsyndrome/"
outDir = "/nfs/research1/gerstung/nelson/outputs/downsyndrome/1_mergingAndQualityControl/"

# Return an annotated anndata object, specific to the particular sample/patient/title specified
def add_sample_anndata(patient, title, sample, chromosome_dir, folder_mtx):
    # Instantiate the anndata object
    if folder_mtx=="Healthy":
        adata  = fc.load10XSanger("/nfs/research1/gerstung/nelson/data/foetalliver10X/"+sample, title=title, patient=patient, folder_mtx=folder_mtx, chromosome_dir=chromosome_dir)
    elif folder_mtx=="Foetal_all":
        adata  = fc.load10XSanger("/nfs/research1/gerstung/nelson/data/foetal_all_inc_liver/"+sample, title=title, patient=patient, folder_mtx=folder_mtx, chromosome_dir=chromosome_dir)
    else:
        adata  = fc.load10XSanger(folder+sample, title=title, patient=patient, folder_mtx=folder_mtx, chromosome_dir=chromosome_dir)

    # Annotate with additional observations
    infos = ["organ", "age", "sorting", "mean reads/cell", "median genes/cell", "environment", "# isolated cells", "# estimated cells", "sangerID"]
    for info in infos:
        #adata.obs[info] = multiomeMetadata[patient][title][info]
        adata.obs[info] = patientMetadata[patient][title][info]

    # Add feature_types to healthy foetal data since they do not include it
    if folder_mtx=="Healthy" or folder_mtx=="Foetal_all":
        adata.var["feature_types"] = "Gene Expression"

    # Annotate with additional variables
    adata.var = adata.var[['gene_ids', 'feature_types', 'n_cells', 'chromosome']] if chromosome_dir is not None else adata.var[['gene_ids', 'feature_types', 'n_cells']]
    adata.var.rename(columns=
            {"gene_ids"     : "gene_ids_{0}".format(title),
            "feature_types" : "feature_types_{0}".format(title),
            "n_cells"       : "n_cells_{0}".format(title)}, inplace=True
    )

    if chromosome_dir is not None:
        adata.var.rename(columns=
                {"chromosome"      : "chromosome_{0}".format(title)}, inplace=True
        )

    return adata

def main():
    pdf = mpdf.PdfPages(outDir+"Healthy_FullscRNAseq_mergingAndQualityControl.pdf")

    # Each tuple of (patient, title, sample, folder_mtx) corresponds to one collection
    collections = [(i, j, patientMetadata[i][j]['sample'],patientMetadata[i][j]["folder_mtx"]) for i in patientMetadata.keys() for j in patientMetadata[i].keys()]
    #collections = [(i, j, multiomeMetadata[i][j]['sample'],multiomeMetadata[i][j]["folder_mtx"]) for i in multiomeMetadata.keys() for j in multiomeMetadata[i].keys()]
    #collections = [c for c in collections if c[0]=="Healthy"] # Hack for running on healthy only
    # We are adding the chromosomes
    chromosome_dir='/nfs/research1/gerstung/nelson/downsyndrome/Functions/adding_chromosomes/'
    print(collections)
    # Loop over all samples and append to the adatas list
    adatas = [add_sample_anndata(patient=col[0], title=col[1], sample=col[2], chromosome_dir=chromosome_dir, folder_mtx=col[3]) for col in collections]

    # Perform the sample merging
    merged = adatas[0].concatenate(adatas[1:], join='outer', index_unique="-") if len(adatas) > 1 else adatas[0]
    del adatas

    # Annotation of the merged object
    new_cols = [col.split("-")[0] for col in merged.var.columns]
    merged.var.columns = new_cols
    merged.X   = np.nan_to_num(merged.X)
    merged.var = merged.var.fillna(value=0)

    merged.obs['patient_sample'] = merged.obs['patient'].astype(str)+' '+merged.obs['sample'].astype(str)

    # Add Ensembl IDs as a variable on the merged object
    ens_columns = [name for name in merged.var.columns.tolist() if name.startswith(('gene_ids')) ]
    ens = merged.var[ens_columns]
    ens = ens.T
    ensIDs = [np.trim_zeros(np.unique(list(ens[idx])))[-1] for idx in ens.columns.tolist()]
    merged.var["Ensembl"] = ensIDs

    # Add corresponding chromosomes for the Ensembl IDs as a variable on the merged object
    if chromosome_dir is not None:
        chr_columns = [name for name in merged.var.columns.tolist() if name.startswith(('chromosome')) ]
        chr = merged.var[chr_columns]
        chr = chr.T
        chrs = [np.trim_zeros(np.unique(list(chr[idx])))[-1] for idx in chr.columns.tolist()]
        merged.var["Chromosome"] = chrs

    # Remove features
    to_delete = ['gene_ids', 'feature_types', 'chromosome'] if chromosome_dir is not None else ['gene_ids', 'feature_types']
    for dels in to_delete:
        column_name = [name for name in merged.var.columns.tolist() if name.startswith((dels)) ]
        for c in column_name:
            del merged.var[c]

    merged.var["feature_types"] = "Gene Expression"
    merged.obs["exp"] = "10X"
    merged.obs["patient_sample"] = merged.obs["patient_sample"].astype("category")
    merged.obs["patient_sample"].cat.categories

    # Quality control
    # 1. All cell
    samples = len(np.unique(merged.obs["patient_sample"]))
    y = 2
    x = int(samples/2)
    if x*y < samples:
        x+=1

    f, axs = plt.subplots(x,y,figsize=(30,60), squeeze=False)
    sns.set(font_scale=2)
    sns.set_style("white")

    for idx, sample in enumerate(merged.obs["patient_sample"].cat.categories):
        reduced = merged[merged.obs["patient_sample"]==sample]
        sns.distplot(reduced.obs['n_genes_by_counts'], bins=200, kde=False, ax=axs[int(idx/2)][idx%2], color="black")
        axs[int(idx/2)][idx%2].set_xlim(0, 10000)
        axs[int(idx/2)][idx%2].set_title(sample)

    for i in range(x):
        for j in range(y):
            if i*y+j >= samples:
                f.delaxes(axs[i][j])

    #sns.despine(offset=10, trim=False)
    plt.tight_layout()
    pdf.savefig()
    plt.close("all")

    f, axs = plt.subplots(x,y,figsize=(30,60), squeeze=False)
    sns.set(font_scale=2)
    sns.set_style("white")

    for idx, sample in enumerate(merged.obs["patient_sample"].cat.categories):
        reduced = merged[merged.obs["patient_sample"]==sample]

        sns.distplot(reduced.obs['total_counts'], bins=200, kde=False, ax=axs[int(idx/2)][idx%2], color="black")
        axs[int(idx/2)][idx%2].set_xlim(0, 100000)
        axs[int(idx/2)][idx%2].set_title(sample)

    for i in range(x):
        for j in range(y):
            if i*y+j >= samples:
                f.delaxes(axs[i][j])

    #sns.despine(offset=10, trim=False)
    plt.tight_layout()
    pdf.savefig()
    plt.close("all")

    # 2. Make Violin plot for genes, counts, ribosomal counts, and mitocondrial counts resulting from e.g. mitocondrial RNA leakage
    f, axs = plt.subplots(1,4,figsize=(40,8))
    fc.plotViolinVariableGroup(merged, variable='n_genes_by_counts', group_by="exp", cut=0, width=4, ax=axs[0], pdf=None)
    fc.plotViolinVariableGroup(merged, variable='total_counts',      group_by="exp", cut=0, width=4, ax=axs[1], pdf=None)
    fc.plotViolinVariableGroup(merged, variable='pct_counts_rb',     group_by="exp", cut=0, width=4, ax=axs[2], pdf=None)
    fc.plotViolinVariableGroup(merged, variable='pct_counts_mt',     group_by="exp", cut=0, width=4, ax=axs[3], pdf=None)

    #sns.despine(offset=10, trim=False)
    plt.tight_layout()
    pdf.savefig()
    plt.close("all")

    fc.plotScatter(merged, None, x="pct_counts_mt", y="n_genes_by_counts", palette=sns.color_palette("deep"), pdf=pdf)
    fc.plotScatter(merged, "pct_counts_mt", x="total_counts", y="n_genes_by_counts", colormap="viridis", pdf=pdf)

    # 3. Number of cell and Median number of genes before filtering cells
    fc.plotBarPlotFeatures(merged,
        group_by1="patient_sample",
        features=["n_genes_by_counts", "total_counts"],
        show_df=False,
        height=20,
        width=60,
        palette=None,
        pdf=pdf)

    nCells = merged.n_obs
    merged_tmp   = merged.copy()

    min_genes    = 250
    #min_genes    = 100 # For multiome
    max_genes    = 8500
    min_counts   = 750
    #min_counts   = 400 # For multiome
    max_counts   = 110000
    max_pctMito  = 20

    # Filter cells according to identified QC thresholds:
    print('Total number of cells: %d'%(merged_tmp.n_obs))

    sc.pp.filter_cells(merged_tmp, min_genes = min_genes)
    loosing = (100.-100.*(float(merged_tmp.n_obs)/nCells))
    print('Number of cells after min gene filter: %d, lost %3.2f%% of cells'%(merged_tmp.n_obs, loosing))

    sc.pp.filter_cells(merged_tmp, max_genes = max_genes)
    loosing = (100.-100.*(float(merged_tmp.n_obs)/nCells))
    print('Number of cells after max gene filter: %d, lost %3.2f%% of cells'%(merged_tmp.n_obs, loosing))

    sc.pp.filter_cells(merged_tmp, min_counts = min_counts)
    loosing = (100.-100.*(float(merged_tmp.n_obs)/nCells))
    print('Number of cells after min count filter: %d, lost %3.2f%% of cells'%(merged_tmp.n_obs, loosing))

    sc.pp.filter_cells(merged_tmp, max_counts = max_counts)
    loosing = (100.-100.*(float(merged_tmp.n_obs)/nCells))
    print('Number of cells after max count filter: %d, lost %3.2f%% of cells'%(merged_tmp.n_obs, loosing))

    merged_tmp = merged_tmp[merged_tmp.obs['pct_counts_mt'] < max_pctMito]
    loosing = (100.-100.*(float(merged_tmp.n_obs)/nCells))
    print('Number of cells after MT filter: %d, lost %2.f%% of cells'%(merged_tmp.n_obs, loosing))


    #sc.pp.filter_genes(merged_tmp, min_cells=10)
    print(" * Initial 10X Object: %d genes across %d single cells"%(merged_tmp.n_vars, merged_tmp.n_obs))

    beforeRemovingS  = merged.obs.index
    afterRemovingS   = merged_tmp.obs.index
    indices          = np.invert(beforeRemovingS.isin(afterRemovingS.tolist()))

    merged.obs["Removed"] = ["Yes" if i in indices else "No" for i in merged.obs.index]
    merged.obs['Removed'] = merged.obs.Removed.astype('category')
    fc.plotScatter(merged, "Removed", x="total_counts", y="n_genes_by_counts", palette=["gray", "red"], pdf=pdf)

    f, axs = plt.subplots(1,4,figsize=(40,8))
    sns.set(font_scale=1.2)
    sns.set_style("white")

    # Add violin plot after removal of low gene counts
    fc.plotViolinVariableGroup(merged_tmp, variable='n_genes_by_counts', cut=0, width=4, ax=axs[0], pdf=None)
    fc.plotViolinVariableGroup(merged_tmp, variable='total_counts',      cut=0, width=4, ax=axs[1], pdf=None)
    fc.plotViolinVariableGroup(merged_tmp, variable='pct_counts_rb',     cut=0, width=4, ax=axs[2], pdf=None)
    fc.plotViolinVariableGroup(merged_tmp, variable='pct_counts_mt',     cut=0, width=4, ax=axs[3], pdf=None)

    #sns.despine(offset=10, trim=False)
    plt.tight_layout()
    pdf.savefig()
    plt.close("all")

    # 4. Removing doublets
    # Doublet removal achieved using scrublet: https://www.cell.com/cell-systems/pdfExtended/S2405-4712(18)30474-5
    merged_tmp = fc.scrublet_doublet_removal_10X(merged_tmp)

    fc.plotScatter(merged_tmp,
               x="total_counts",
               y="n_genes_by_counts",
               variable="predicted_doublets",
               palette=["gray", "red"],
               pdf=pdf)

    merged_tmp = merged_tmp[merged_tmp.obs['predicted_doublets'] != True]
    merged_tmp.obs.drop("predicted_doublets", axis=1, inplace=True)

    # Numbers after applying scrublet
    print(" * 10X Object (after scrublet): %d genes across %d single cells"%(merged_tmp.n_vars,
                                                                         merged_tmp.n_obs))
    # Now reassign to the original merged object
    merged = merged_tmp.copy()
    del merged_tmp

    # Number of cell and Median number of genes after filtering cells
    fc.plotBarPlotFeatures(merged,
        group_by1="patient_sample",
        features=["n_genes_by_counts", "total_counts"],
        show_df=False,
        height=20,
        width=60,
        palette=None,
        pdf=pdf)

    # 5. Batch assignment - each batch corresponds to a "patient" sample
    merged.obs['patient_sample'].value_counts()
    merged.obs['batch'] = 'No'

    for idx, sample in enumerate(merged.obs["patient_sample"].cat.categories):
        merged.obs['batch'][merged.obs['patient_sample'] == sample] = idx

    merged.obs['batch'] = merged.obs['batch'].astype('category')
    merged.obs['batch'].value_counts()
    merged.obs['patient_sample'].value_counts()

    # 6. Add additional sample-specific information
    info_dict = {}

    for idx, sample in enumerate(merged.obs["patient_sample"].cat.categories):
        reduced = merged[merged.obs["patient_sample"]==sample].copy()
        sc.pp.filter_genes(reduced, min_cells=10)

        info_dict[idx] = [sample,
            np.unique(reduced.obs["sorting"].astype(str))[0],
            np.unique(reduced.obs["# isolated cells"].astype(int))[0],
            np.unique(reduced.obs["# estimated cells"].astype(int))[0],
            np.unique(reduced.obs["mean reads/cell"].astype(int))[0],
            np.unique(reduced.obs["median genes/cell"].astype(int))[0],
            reduced.n_obs,
            reduced.n_vars,
            int(np.median(reduced.obs["n_genes_by_counts"])),
            int(np.median(reduced.obs["total_counts"]))]

        df = pd.DataFrame.from_dict(info_dict, orient='index')

        df.rename(
            {0:"Sample",
             1:"Sorting strategy",
             2:"# isolated cells",
             3:"# estimated cells",
             4: "Mean # reads per cell before QC",
             5: "Median # genes per cell before QC",
             6:"# cells after QC",
             7:"# genes after QC",
             8:"Median # genes per cell after QC",
             9:"Median # UMIs per cell after QC"}, axis="columns", inplace=True
        )
        del reduced

    df["Success rate (%) - Estimated"] = round(df["# estimated cells"]/df["# isolated cells"]*100, 2)
    df["Success rate (%) - after QC"]  = round(df["# cells after QC"]/df["# isolated cells"]*100, 2)
    print("Mean success rate - Estimated = %s%%"%round(np.average(df["Success rate (%) - Estimated"]),2))
    print("Mean success rate - After QC  = %s%%"%round(np.average(df["Success rate (%) - after QC"]),2))

    df.set_index("Sample", inplace=True)
    df.index.name = None

    # Write this to a CSV file
    df.to_csv(outDir+"QC_summary_Healthy_FullscRNAseq.csv", sep="\t")

    # 8. Saving layers of the anndata objects

    # Save counts into the layer
    merged.layers["counts"] = np.asarray(merged.X)

    # Normalise and save the data into the layer
    # IMPORTANT: the normalisation step is included here, but generally should be applied
    # at the batch correction stage, where we first split the environments. After splitting environments,
    # apply lines like those below to correctly normalise.
    sc.pp.normalize_per_cell(merged, counts_per_cell_after=1e4)
    merged.layers["norm"] = np.asarray(merged.X).copy()
    print('Normalise')

    # Logarithmise and save the data into the layer
    sc.pp.log1p(merged)
    merged.layers["log"] = np.asarray(merged.X.copy())
    print('Logarithmise')

    # Save in adata.raw.X for this normalised and logarithmise data
    merged.raw = merged.copy()

    # Scale and save the data into the layer
    sc.pp.scale(merged, max_value=10)
    merged.layers["scale"] = np.asarray(merged.X.copy())
    print('Scale')


    # 9. Adding cell cycle scoring
    pathCellCycle = '/nfs/research1/gerstung/nelson/downsyndrome/MetaData/Resources/regev_lab_cell_cycle_genes.txt'
    if "10X" in folder:
        fc.CellCycleScoring10x(pathCellCycle, merged)
    else:
        fc.CellCycleScoring(pathCellCycle, merged)
    pd.DataFrame(merged.obs["phase"].value_counts())
    print('Completed cell cycle scring')

    # 10.Adding generalised PHASE
    phase_list = ["G1" if i == "G1" else "G2M+S" for i in merged.obs["phase"].tolist()]
    merged.obs["PHASE"] = phase_list
    pd.DataFrame(merged.obs["PHASE"].value_counts()).sort_index()
    print('PHASE added')

    # 11. Normalization - log transformation for high-variation gene detection
    merged.X = merged.layers["log"]

    # Close pdf
    pdf.close()
    print('Closing PDF')

    # 12. Save object (need sufficient memory for the saving to work)
    path = "/nfs/research1/gerstung/nelson/data/downsyndrome/ScanpyObjects/merged/10X_Healthy_Full.h5ad"
    #path = "/nfs/research1/gerstung/nelson/data/downsyndrome/ScanpyObjets/merged/10X_Multiome_DownSyndrome.h5ad"
    #path = "/nfs/research1/gerstung/nelson/data/foetal_all_inc_liver/AnnData/Foetal_All_withAnnot.h5ad"
    merged.write(path, compression='gzip')
    print('Finished writing. Exciting gracefully.')


if __name__=="__main__":
    main()
