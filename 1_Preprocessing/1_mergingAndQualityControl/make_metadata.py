from os import system as sys
import json
import pandas as pd

inFile = "/nfs/research1/gerstung/nelson/data/E-MTAB-7407.sdrf.txt"
outDir = "/nfs/research1/gerstung/nelson/downsyndrome/PythonPipeline/1_mergingAndQualityControl/"
def main():
    df = pd.read_csv(inFile, sep = "\t")
    df = df.loc[df["Factor Value[organism part]"]=="liver"] # Only want liver samples
    metadata = dict() # Put all metadata into a dictionary
    samples = dict()
    for index, row in df.iterrows():
        samples[row["Source Name"]] = {
                'organ':             row["Characteristics[organism part]"],
                'age':               "{0} pcw".format(row["Characteristics[age]"].split(" ")[0]),
                'sorting':           row["Characteristics[facs sorting]"],
                '# isolated cells':  None, # Not known
                '# estimated cells': None, # Not known
                'sangerID':          row["Source Name"],
                'sample':            row["Source Name"],
                'folder_mtx':        "Healthy",

        }
    metadata["Healthy"] = samples

    with open(outDir+'healthy_foetal_liver_metadata.json', 'w') as outFile:
        json.dump(metadata, outFile, indent=8)

if __name__=="__main__":
    main()
