# Merging samples and QC

The heavy-lifting is done by the ```run_merging_samples_and_QC.py``` script, which combines and filters all data-sets specified in the
metadata dictionary ```patientMetadata_full.py```. Directly editing the ```patientMetadata_full.py``` will therefore affect which samples
are merged by ```run_merging_samples_and_QC.py```, and consequently the final output. ```run_merging_samples_and_QC.py``` can be called
directly from the command line, but if you are processing many CellRanger outputs I recommend running ```batch_submit.py```, which runs
the same python script on an LSF cluster with the usual commands. By defauly, it submits to a ```bigmem``` cluster (using the EBI codon-login
LSF naming conventions; this will of course vary for different LSF/batch farm systems), due to the high memory allocation
typically required to merge multiple scRNA-Seq data-sets and save the filtered matrices to ```h5ad``` format. 
