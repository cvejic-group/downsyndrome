from os import system as sys

fname = "Healthy_Merge_FullscRNAseq"
f = open(fname+".sh", 'w')
f.write("cd /nfs/research1/gerstung/nelson/downsyndrome/PythonPipeline/1_mergingAndQualityControl/\n")
f.write("conda activate downsyndrome_env\n" )
f.write("python run_merging_samples_and_QC.py")
f.close()

print("bsub -P bigmem -M 500000 -o {0}.txt -e {0} bash {0}.sh".format(fname))
sys("bsub -P bigmem -M 500000 -o {0}.txt -e {0} bash {0}.sh".format(fname))
