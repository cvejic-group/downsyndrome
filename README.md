# Down Syndrome Study

This repository contains scripts to reproduce some results of the manuscript *Single-cell multi-omics map of human foetal blood in Down’s Syndrome*.

The folders contain scripts as described as below:
- `1_Preprocessing`: pre-processing scripts from empty droplet removal to clustering
- `2_AbundanceAnalysis`: scripts contrasting cell type abundances
- `3_CellPhoneDBAnalysis`: ligand-receptor analysis with CellPhoneDB
- `4_AnalyseStem`: analysis with HSCs
- `5_validate_with_integration`: comparison of separate annotations vs Harmony/scVI integrations and vs label transfer results
- `6_TrajectoryAnalysis`: trajectory analysis
- `7_SpatialAnalysis`: scripts for spatial transcriptomics analysis, including an examination of usage of separate references for cell type deconvolution.
