import sys
import scanpy as sc
import numpy as np
import pandas as pd
from scipy.stats import mannwhitneyu

def create_df(adata, cell_types, cell_labels, merge_types=True):
    df = pd.DataFrame()
    for cell in cell_types:
        df[cell] = np.array([adata[((adata.obs[cell_labels]==cell)&(adata.obs["patient"]==p)),:].n_obs for p in adata.obs["patient"].unique()])
    df.index = adata.obs["patient"].unique()

    # Save a raw copy
    df_c = df

    df = df.T

    # Normalise per biological replicate
    for col in df.columns:
        sum = df[col].sum()
        if sum > 0:
            df[col] = df[col]*100/sum
        else:
            df[col] = 0
    df = df.T

    if merge_types:
        # Find all the relevant pairs based on the cycling cells
        pairs = [(" ".join(c.split(" ")[1:])[0].upper()+" ".join(c.split(" ")[1:])[1:], c) for c in cell_types if "Cycling" in c]
        # Horrid hack to correct the cDC2 naming
        for p in pairs:
            if p[0]=="CDC2":
                lst = list(p)
                lst[0] = "cDC2"
                pairs.append(tuple(lst))
                pairs.remove(p)
            if p[0]=="PDCs":
                lst = list(p)
                lst[0] = "pDCs"
                pairs.append(tuple(lst))
                pairs.remove(p)
            if p[1]=="Cycling erythroid cells":
                pairs.remove(p)
            if p[1]=="Cycling muscle cells":
                pairs.remove(p)
        for pair in pairs:
            df[pair[0]] += df[pair[1]]
            del df[pair[1]]

    return df, df_c

in_dir = "/nfs/research/gerstung/nelson/data/downsyndrome/scRNAseq/outputs/"
def main(input_1="", input_2="", cell_labels_1="", cell_labels_2="", sorting="CD235a-"):
    adata_1 = sc.read_h5ad(in_dir+input_1+".h5ad")
    adata_2 = sc.read_h5ad(in_dir+input_2+".h5ad")

    adata_1, adata_2 = adata_1[adata_1.obs["sorting"]==sorting,:], adata_2[adata_2.obs["sorting"]==sorting,:]
    # We also want age-matched foetuses
    samples_1 = ["15582", "15646", "15669", "15712", "15724", "15734"]
    samples_2 = ["15633", "15657", "15781"]

    adata_1, adata_2 = adata_1[adata_1.obs["patient"].isin(samples_1),:], adata_2[adata_2.obs["patient"].isin(samples_2),:]
    print("Number of cells {0}: ".format(input_1), adata_1.n_obs)
    print("Number of cells {0}: ".format(input_2), adata_2.n_obs)
    cell_types = list(set(list(adata_1.obs[cell_labels_1].unique())) & set(list(adata_2.obs[cell_labels_2].unique()))) # Get the unique cell types
    remove = ["Odd PTPRC+ cells (to remove)", "34,0 (to remove)", "Odd NK cells (to remove)", "Pre pro B cells,4 (to remove)",
             "Osteoblasts,4 (to remove)", "Megakaryocytes,3 (to remove)", "0 (to remove)", "Unknown 1", "Unknown 2", "Unknown",
             "X", "38,0", "31,0", "Erythroid cells,2,1", "34", "Megakaryocytes,2,0", "36,1", "36,0,0", "36,0,1",
             "38,1", "Megakaryocytes,2,1", "Striated muscle cells"] # List to remove
    cell_types = [c for c in cell_types if c not in remove]
    cycling_states = [c for c in cell_types if "Cycling" in c]
    non_cycling_states = [" ".join(c.split(" ")[1:])for c in cycling_states]

    f = lambda s: s[0].upper()+s[1:] if (s!="cDC2" and s!="pDCs") else s
    non_cycling_states = [f(nc) for nc in non_cycling_states]
    dict_merge = {cycling_states[i] : non_cycling_states[i] for i in range(len(cycling_states)) if non_cycling_states[i] in cell_types}

    sys.path.append("/nfs/research/gerstung/nelson/downsyndrome/Functions/")
    from scRNA_functions import scRNA_functions
    fc = scRNA_functions()
    fc.mergeClusters(adata_1, newName="new_cell_labels_1", labels=dict_merge, cluster=cell_labels_1, verbose=True)
    fc.mergeClusters(adata_2, newName="new_cell_labels_2", labels=dict_merge, cluster=cell_labels_2, verbose=True)

    df_1, df_raw_1 = create_df(adata_1, cell_types, "new_cell_labels_1", merge_types=False)
    df_2, df_raw_2 = create_df(adata_2, cell_types, "new_cell_labels_2", merge_types=False)

    pvalue_dict = dict()
    mean_dict = dict()
    for cell in df_1.columns.values:
        if df_1[cell].all()!=0 and df_2[cell].all()!=0:
            pvalue_dict[cell] = {"Raw": mannwhitneyu(df_1[cell].values, df_2[cell].values, alternative="two-sided")[1]}
            diff = df_1[cell].mean()-df_2[cell].mean()
            diff_spread_1 = df_1[cell].std()/np.sqrt(len(df_1[cell])-1)
            diff_spread_2 = df_2[cell].std()/np.sqrt(len(df_2[cell])-1)
            overall_spread = 2*np.sqrt(diff_spread_1**2 + diff_spread_2**2) # 2 sigma spread
            mean_dict[cell] = {"Difference": diff, "Difference spread": overall_spread}


    # Apply B-H correction to p-values
    pvalues = [value["Raw"] for key, value in pvalue_dict.items()]
    from statsmodels.stats.multitest import fdrcorrection
    corrected = fdrcorrection(pvalues,0.05)[1]
    count = 0
    for key, value in pvalue_dict.items():
        value["Corrected"] = corrected[count]
        count+=1

    df = pd.DataFrame()
    df["Cell type"] = np.array([key for key, value in pvalue_dict.items()])
    df["p-value"] = np.array([value["Raw"] for key, value in pvalue_dict.items()])
    df["B-H corrected p-value"] = np.array([value["Corrected"] for key, value in pvalue_dict.items()])

    df_diff = pd.DataFrame()
    df_diff["Cell type"] = np.array([key for key, value in mean_dict.items()])
    df_diff["Difference"] = np.array([value["Difference"] for key, value in mean_dict.items()])
    df_diff["Difference spread"] = np.array([value["Difference spread"] for key, value in mean_dict.items()])

    df_1.to_csv(in_dir+"stat_tests_updated/"+input_1+"_numbers_by_foetus_"+sorting+"_age_matched.csv")
    df_2.to_csv(in_dir+"stat_tests_updated/"+input_2+"_numbers_by_foetus_"+sorting+"_age_matched.csv")
    df_raw_1.to_csv(in_dir+"stat_tests_updated/"+input_1+"_raw_numbers_by_foetus_"+sorting+"_age_matched.csv")
    df_raw_2.to_csv(in_dir+"stat_tests_updated/"+input_2+"_raw_numbers_by_foetus_"+sorting+"_age_matched.csv")
    df.to_csv(in_dir+"stat_tests_updated/"+input_1+"_"+input_2+"_significances_"+sorting+"_age_matched.csv")
    df_diff.to_csv(in_dir+"stat_tests_updated/"+input_1+"_"+input_2+"_Differences_"+sorting+"_age_matched.csv")

if __name__=="__main__":
    # Adding an argument parser
    from argparse import ArgumentParser
    parser = ArgumentParser()
    parser.add_argument("-i1", "--input_1", default="", type=str)
    parser.add_argument("-i2", "--input_2", default="", type=str)
    parser.add_argument("-c1", "--cell_labels_1", default="", type=str)
    parser.add_argument("-c2", "--cell_labels_2", default="", type=str)
    parser.add_argument("-s", "--sorting", default="CD235a-", type=str)

    options = parser.parse_args()

    # Defining dictionary to be passed to the main function
    option_dict = dict( (k, v) for k, v in vars(options).items() if v is not None)
    print(option_dict)
    main(**option_dict)
