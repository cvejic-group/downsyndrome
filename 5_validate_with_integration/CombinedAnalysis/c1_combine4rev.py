import os
import gc
import sys
import scanpy as sc
import pandas as pd
import harmonypy as hm

PROJ_DIR = "/lustre/scratch126/casm/team-cvejic/haoliang/pj_DownSyndrome/results/"
IN_DIR = f"{PROJ_DIR}annotated_data/"

OUT_DIR = f"{PROJ_DIR}combined_analysis_res4rev/"
if not os.path.exists(OUT_DIR):
    print("Creating folder:", OUT_DIR, file=sys.stderr)
    os.makedirs(OUT_DIR)


if __name__ == "__main__":
    print("Combining all 4 datasets...", file=sys.stderr)
    env_src_lst = ["Healthy_Femur", "DownSyndrome_Femur", "DownSyndrome_Liver", "Healthy_Liver"]
    # Load stored adata
    adata_lst = [sc.read_h5ad(f"{IN_DIR}10X_{env_src}.h5ad") for env_src in env_src_lst]
    combined = None
    for i in range(len(adata_lst)):
        env_src = env_src_lst[i]
        print(f"=====> {env_src}", file=sys.stderr)
        print("--- Before unification:", file=sys.stderr)
        print(adata_lst[i], file=sys.stderr)
        # Parse latest clustring results
        a = [col[8:] for col in adata_lst[i].obs.columns if 'leiden_v' in col]
        max_num = max([int(x) for x in a if x.isdigit()])
        col_name = f"leiden_v{str(max_num)}"
        print(f"\tUsing {col_name} as the latest clustering column.",
              file=sys.stderr)
        adata_lst[i].obs.rename(columns={col_name: "leiden_latest"}, inplace=True)
        # Only keep relevant columns in observations and variables
        adata_lst[i].obs = adata_lst[i].obs[["patient_sample", "leiden_latest"]]
        adata_lst[i].var = adata_lst[i].var[["Ensembl", "Chromosome", "highly_variable"]]
        del adata_lst[i].obsm, adata_lst[i].varm, adata_lst[i].obsp, adata_lst[i].uns
        print("--- After unification:", file=sys.stderr)
        print(adata_lst[i], file=sys.stderr)
    # Merge this adata into the combined variable
    combined = adata_lst[0].concatenate(adata_lst[1:], join="inner", index_unique="-",
                                        batch_key="environment",
                                        batch_categories=env_src_lst)
    for col in combined.obs.columns:
        print(f"Categorising {col} in combined.obs", file=sys.stderr)
        combined.obs[col] = combined.obs[col].astype("category")
    for col in combined.var.columns:
        print(f"Categorising {col} in combined.var", file=sys.stderr)
        combined.var[col] = combined.var[col].astype("category")
    del adata_lst
    gc.collect()
    print(f"=====> all combined", file=sys.stderr)
    print(combined, file=sys.stderr)
    combined.write(f"{OUT_DIR}combined_adata.h5ad")
    print("Combining datasets completed.\n\n", file=sys.stderr)
