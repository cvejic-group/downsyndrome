import sys
import scanpy as sc

# ========== Path Parameters ========== #
OUT_DIR = f"/lustre/scratch126/casm/team-cvejic/haoliang/pj_DownSyndrome/results/combined_analysis_res4rev/"

if __name__ == "__main__":
    # Read scanpy object
    adata = sc.read_h5ad(f"{OUT_DIR}e_combined_adata_harmonised_reclustered.h5ad")
    for clust in adata.obs["new_clusters"].cat.categories:
        print(clust, file=sys.stderr)
        adata[adata.obs["new_clusters"].isin([clust]), :].write_h5ad(f"{OUT_DIR}stored_by_subclusters/cluster{clust}.h5ad")
