import gc
import sys
import scanpy as sc
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import matplotlib.backends.backend_pdf as mpdf

GENES = ["CD34", "SPINK2", "MLLT3", "GATA1", "TESPA1", "ALAS2", "GATA2", "AZU1",
         "CD14", "CD68", "FCN1", "NKG7", "PAX5", "IGHM", "CLEC4A", "MKI67"]
ENV_ORDER = ["Healthy_Liver", "DownSyndrome_Liver", "Healthy_Femur", "DownSyndrome_Femur"]
FINE_CT_ORDER = {"HSC/Progenitors": ["HSCs/MPPs", "Cycling HSCs/MPPs",
                                     "MEMPs", "Cycling MEMPs",
                                     "Granulocyte progenitors", "Cycling granulocyte progenitors"]}

MODE = "all_4datasets"
PROJ_DIR = "/lustre/scratch119/casm/team-cvejic/haoliang/results/DownSyndrome/combined_analysis_res/"
OUT_DIR = f"{PROJ_DIR}{MODE}/"


def plot_dots_byenv(adata, pdf):
    sns.set_theme(style="white", font_scale=1.5)
    sc.pl.dotplot(adata, layer="log", var_names=GENES, groupby="env annot",
                  use_raw=False, standard_scale=None, color_map='Reds',
                  dendrogram=False, figsize=(12.5, 18), show=True, 
                  linewidths=2)
    plt.subplots_adjust(left=0.5, right=0.9)
    pdf.savefig()
    plt.close()


if __name__ == "__main__":
    # Load anndata and merge with broad cell types
    adata = sc.read_h5ad(f"{PROJ_DIR}{MODE}/combined_adata_harmonised_reclustered.h5ad")
    adata.obs["leiden_latest"][(adata.obs["environment"] == "Healthy_Femur") & (adata.obs["leiden_latest"] == "MEMPs")] = "Cycling MEMPs"
    adata.X = adata.layers["scale"]
    celltype_map = pd.read_csv(f"{PROJ_DIR}/celltype_map.csv")
    adata.obs["leiden_latest"] = adata.obs["leiden_latest"].astype("str")
    adata.obs = pd.merge(adata.obs.reset_index(),
                         celltype_map, how="left").set_index("index")
    adata.obs["broad.cell.types"].fillna("removed", inplace=True)
    adata.obs["leiden_latest"] = adata.obs["leiden_latest"].astype("category")
    adata.obs["broad.cell.types"] = adata.obs["broad.cell.types"].astype("category")
    # Select the broad cell type
    pdf = mpdf.PdfPages(f"{OUT_DIR}dotplot_HSC_Progenitors.pdf")
    adata_sub = adata[adata.obs["broad.cell.types"].isin(["HSC/Progenitors"]), GENES].copy()
    # Make annotations to check
    adata_sub.obs["env annot"] = adata_sub.obs["leiden_latest"].astype("str") + "\n" + adata_sub.obs["environment"].astype("str")
    adata_sub.obs["env annot"] = adata_sub.obs["env annot"].astype("category")
    cate_order = [f"{x}\n{y}" for x in FINE_CT_ORDER["HSC/Progenitors"] for y in ENV_ORDER]
    print(cate_order, file=sys.stderr)
    print(adata_sub.obs["env annot"].unique(), file=sys.stderr)
    cate_order = [x for x in cate_order if x in adata_sub.obs["env annot"].unique()]
    print(cate_order, file=sys.stderr)
    adata_sub.obs["env annot"] = adata_sub.obs["env annot"].cat.reorder_categories(cate_order, ordered=True)
    plot_dots_byenv(adata_sub, pdf)
    pdf.close()
    gc.collect()

