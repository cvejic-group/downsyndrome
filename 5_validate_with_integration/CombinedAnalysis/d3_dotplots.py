import os
import gc
import sys
import scanpy as sc
import seaborn as sns
import matplotlib.pyplot as plt

OUT_DIR = "/lustre/scratch126/casm/team-cvejic/haoliang/pj_DownSyndrome/results/combined_analysis_res4rev/"

GENES = ["CD34", "SPINK2", "MLLT3", "GATA1", "KLF1", "TESPA1", "AHSP", "ALAS2", "HBA1",
         "GYPA", "GATA2", "HDC", "CPA3", "ITGA2B", "GP9", "MPO", "AZU1", "SPI1", "LYZ", 
         "CD14", "CD68", "S100A9", "MNDA", "FCN1", "CD163", "MS4A7", "CTSB", "NKG7", 
         "PRF1", "GZMA", "IL7R", "DHFR", "PAX5", "MME", "IGLL1", "IGHM", "CD79A", "CD19",
         "JCHAIN", "IRF8", "CLEC4C", "IL3RA", "CD1C", "CLEC4A", "CLEC10A", "MKI67", 
         "ALB", "AFP", "DCN", "COL1A1", "COL3A1", "RBP1", "ACTA2", "TAGLN", "MYL9", "CDH5",
         "KDR", "STAB1", "STAB2", "LYVE1", "KRT19"]

def plot_dots(adata2plt):
    sns.set_theme(style="white", font_scale=1.5)
    sc.pl.dotplot(adata2plt, layer="log", var_names=GENES, groupby="to_annot",
                  use_raw=False, standard_scale="var", color_map='Reds',
                  dendrogram=False, figsize=(35, 20), show=True,
                  linewidths=2)
    plt.subplots_adjust(left=0.5, right=0.9)

obj_dir = f"{OUT_DIR}stored_by_subclusters_new/stored_by_subclusters_group1/"
print(obj_dir, file=sys.stderr)
adata_lst = [sc.read_h5ad(f"{obj_dir}{x}")
             for x in os.listdir(obj_dir) if x.endswith(".h5ad")]
adata = adata_lst[0].concatenate(adata_lst[1:], join="inner", index_unique="-")
adata.obs["to_annot"] = adata.obs["new_clusters_v3"].astype("str") + "-" + adata.obs["environment"].astype("str")
adata.obs["to_annot"] = adata.obs["to_annot"].astype("category")
print(adata.obs["to_annot"].value_counts(), file=sys.stderr)
del adata_lst
gc.collect()
plot_dots(adata)
plt.savefig(f"{obj_dir}dotplots_unsure.png", bbox_inches="tight", facecolor="white")
plt.close()
del adata
gc.collect()

obj_dir = f"{OUT_DIR}stored_by_subclusters/stored_by_subclusters_group1/"
print(obj_dir, file=sys.stderr)
adata_lst = [sc.read_h5ad(f"{obj_dir}{x}.h5ad")
             for x in ["cDC2", "erythroid", "HSCs", "inflammatory_macro", "mast_cells",
                       "megakaryocytes", "NK_cells", "tolerogenic_macro"]]
adata = adata_lst[0].concatenate(adata_lst[1:], join="inner", index_unique="-",
                                 batch_key="new_clusters_v3",
                                 batch_categories=["cDC2", "erythroid", "HSCs", "inflammatory_macro", "mast_cells",
                                                   "megakaryocytes", "NK_cells", "tolerogenic_macro"])
adata.obs["to_annot"] = adata.obs["new_clusters_v3"].astype("str") + "-" + adata.obs["environment"].astype("str")
adata.obs["to_annot"] = adata.obs["to_annot"].astype("category")
print(adata.obs["to_annot"].value_counts(), file=sys.stderr)
del adata_lst
gc.collect()
plot_dots(adata)
plt.savefig(f"{obj_dir}dotplots_sure.png", bbox_inches="tight", facecolor="white")
