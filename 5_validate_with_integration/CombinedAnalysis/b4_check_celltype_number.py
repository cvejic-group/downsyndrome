import gc
import sys
import scanpy as sc
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import matplotlib.backends.backend_pdf as mpdf


MODE = "all_4datasets"
PROJ_DIR = "/lustre/scratch119/casm/team-cvejic/haoliang/results/DownSyndrome/combined_analysis_res/"
OUT_DIR = f"{PROJ_DIR}{MODE}/check_res/"

CELLTYPE_COLOURS = ["violet", "deepskyblue", "tomato", "darkseagreen"]


def plot_umap(adata):
    # Plot UMAP by cell type
    env_src_lst = adata.obs["environment"].unique()
    n_subplots, n_cols = len(env_src_lst), 2
    n_rows = int(np.ceil(n_subplots / n_cols))
    for i, fct in enumerate(["MEMPs", "Cycling MEMPs", "HSCs/MPPs", "Cycling HSCs/MPPs"]):
        print(f"Plotting {fct} with colour {CELLTYPE_COLOURS[i]}",
              file=sys.stderr)
        _, axes = plt.subplots(nrows=n_rows, ncols=2, figsize=(18, 18))
        for j, env_src in enumerate(env_src_lst):
            adata_sub = adata[adata.obs["environment"] == env_src, :]
            sns.set_theme(style="white", font_scale=2)
            this_ax = sc.pl.umap(adata_sub, color="leiden_latest", size=30,
                                 groups=[fct], na_in_legend=False,
                                 components="1,2", use_raw=False,
                                 legend_loc=None, palette=[CELLTYPE_COLOURS[i]],
                                 ax=axes[int(j/n_cols), int(j % n_cols)], show=False)
            sc.pl.umap(adata, color="new clusters", size=0,
                       components="1,2", use_raw=False,
                       legend_loc="on data", legend_fontsize=15,
                       ax=this_ax,
                       title=f"{fct}: {env_src}")
        plt.tight_layout()
        plt.savefig(f"{OUT_DIR}check_umap_{fct.replace(' ', '_').replace('/', '_')}.png")
        plt.close()


if __name__ == "__main__":
    # Load anndata and merge with broad cell types
    adata = sc.read_h5ad(f"{PROJ_DIR}{MODE}/combined_adata_harmonised_reclustered.h5ad")
    adata.obs["leiden_latest"][(adata.obs["environment"] == "Healthy_Femur") & (adata.obs["leiden_latest"] == "MEMPs")] = "Cycling MEMPs"
    adata = adata[~adata.obs["leiden_latest"].isin(["0 (to remove)", "Osteoblasts,4 (to remove)", "Megakaryocytes,3 (to remove)",
                                                    "Odd PTPRC+ cells (to remove)", "34,0 (to remove)", "Odd NK cells (to remove)",
                                                    "Pre pro B cells,4 (to remove)", "38,0", "31,0", "Erythroid cells,2,1", "34",
                                                    "Megakaryocytes,2,0", "36,1", "36,0,0", "36,0,1", "38,1", "Megakaryocytes,2,1",
                                                    "X", "Unknown", "Unknown 1", "Unknown 2"]), :]
    clust_counts = adata.obs[["leiden_latest",
                              "environment",
                              "new clusters"]].value_counts().reset_index().rename(columns={0: "count"})
    clust_counts.sort_values(by=["leiden_latest",
                                 "environment",
                                 "count"],
                             ascending=False).to_csv(f"{OUT_DIR}celltype_counts.csv",
                                                     index=False)
    plot_umap(adata)
