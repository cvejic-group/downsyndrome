import gc
import sys
import scanpy as sc
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import matplotlib.backends.backend_pdf as mpdf


GENES = ["TMEM219", "APOBR", "TFRC", "TFR2", "NOTCH1", "NOTCH2", "CD44", "PLXNB2", "SORL1", "MIF",
         "ICAM3", "PTPRC", "TNFRSF14", "PTGER4", "MDK", "VEGFB", "GRN", "LRP8", "ANGPT1", "EPHB6",
         "TNFRSF1A", "PDGFD", "LGALS9", "CD36", "TNFRSF1B", "CD46", "TGFB1", "PDGFC", "CD47", "ANGPT2",
         "NR3C1", "SPN", "FAM3C", "LAMP1", "TNFSF13B", "RIPK1", "SCGB3A1", "KIT", "CD200", "COPA",
         "CLEC2D", "MRC2", "LRPAP1", "SLC1A5", "PROCR", "GDF11", "ADGRG5", "MERTK", "LRP6", "IGF1R",
         "KLRG1", "IDE", "SCT", "SLC7A1", "EFNA4", "EFNA1", "TNFRSF25", "SIRPA", "TNFSF4", "LRP5",
         "CD74", "CXCR4", "DLK1", "LTB", "TIMP1", "CXCL8", "AREG", "TNFSF10", "CXADR", "EREG",
         "IGFBP3", "APOB", "TF", "HGF", "PTN", "EGFR", "TNFRSF10D", "SPP1", "CLEC4M", "MRC1",
         "LRP1", "APP", "NRP1", "FLT1", "RELN", "TEK", "EFNB1", "PDGFRB", "THBS2", "JAG1",
         "TGFBR3", "PDGFRA", "FLT4", "CCL2", "ICAM1", "DLL4", "CD40", "NOTCH3", "PTPRK", "KITLG",
         "CD200R1", "COLEC12", "DAG1", "MET", "JAG2", "SORT1", "PROC", "ANTXR1", "CNTN1", "GAS6",
         "DKK2", "IGF1", "DLL1", "CDH1", "CCL23", "VIPR1", "CSF1", "EPHA3", "TNFSF12", "TNFRSF4",
         "DKK1", "SOSTDC1", "EPHA4", "EPHA2", "CXCL12", "LTBR", "FGFR2", "CXCL14", "TNFRSF10B", "SEMA4G", "LGALS3"]

MODE = "all_4datasets"
PROJ_DIR = "/lustre/scratch119/casm/team-cvejic/haoliang/results/DownSyndrome/combined_analysis_res/"
OUT_DIR = f"{PROJ_DIR}{MODE}/"


def plot_dots_byenv(adata, pdf):
    sns.set_theme(style="white", font_scale=1.5)
    sc.pl.dotplot(adata, layer="log", var_names=GENES, groupby="celltype-environment",
                  use_raw=False, standard_scale=None,
                  smallest_dot=10, mean_only_expressed=True,
                  dendrogram=False, figsize=(35, 50), show=True, 
                  linewidths=2, swap_axes=True, log=False)
    plt.subplots_adjust(bottom=0.2)
    pdf.savefig()
    plt.close()


if __name__ == "__main__":
    # Load anndata and merge with broad cell types
    adata = sc.read_h5ad(f"{PROJ_DIR}{MODE}/combined_adata_harmonised_reclustered.h5ad")
    adata = adata[~adata.obs["leiden_latest"].isin(["31,0", "34", "36,0,0", "36,0,1", "36,1", "38,0", "38,1", "X"]), :]
    adata = adata[adata.obs["environment"].isin(["Healthy_Liver", "DownSyndrome_Liver"]), :]
    adata.obs["celltype-environment"] = adata.obs["leiden_latest"].astype("str") + " - " + adata.obs["environment"].astype("str")
    adata.obs["celltype-environment"] = adata.obs["celltype-environment"].astype("category")
    # Select the broad cell type
    pdf = mpdf.PdfPages(f"{OUT_DIR}dotplot_LRs.pdf")
    # Make annotations to check
    plot_dots_byenv(adata, pdf)
    pdf.close()
    gc.collect()

