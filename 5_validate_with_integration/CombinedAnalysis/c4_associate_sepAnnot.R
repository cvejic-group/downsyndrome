rm(list = ls())

library(tidyr)

WK_DIR <- "../../combined_analysis_res4rev/"

obs.tab <- read.csv(paste0(WK_DIR, "b2_combined_adata_obs_harmonised_reclustered.csv"),
                    header = TRUE, row.names = 1)
obs.tab$leiden_latest[obs.tab$environment == "Healthy_Femur" & obs.tab$leiden_latest == "MEMPs"] <- "Cycling MEMPs"
obs.tab <- aggregate(rownames(obs.tab), FUN = length,
                     by = list(obs.tab$leiden_latest, obs.tab$environment, obs.tab$new_clusters)) %>%
    dplyr::rename(seperate_annotation = Group.1,
                  environment = Group.2,
                  combined_clusters = Group.3,
                  cell_number = x) %>%
    dplyr::mutate(sep_annot_env = paste(seperate_annotation, environment, sep = "-"))
ctype.lst <- unique(obs.tab$sep_annot_env)

obs.tab.wide <- pivot_wider(obs.tab[, c("combined_clusters", "sep_annot_env", "cell_number")],
                            id_cols = "combined_clusters", names_from = "sep_annot_env",
                            values_from = "cell_number", values_fill = 0)
obs.tab.wide$total_cell_number <- rowSums(obs.tab.wide[, ctype.lst])
for (ctype in ctype.lst) {
    obs.tab.wide[, ctype] <- obs.tab.wide[, ctype] / obs.tab.wide[, "total_cell_number"] * 100
}
obs.tab2 <- pivot_longer(obs.tab.wide, cols = c(-combined_clusters, -total_cell_number),
                         names_to = "sep_annot_env", values_to = "cell_percent") %>%
    dplyr::filter(cell_percent > 0) %>%
    dplyr::mutate(cell_percent = round(cell_percent, 2))
obs.tab2 <- obs.tab2[order(-obs.tab2$combined_clusters, obs.tab2$cell_percent, decreasing = TRUE),
                     c("combined_clusters", "sep_annot_env", "total_cell_number", "cell_percent")]
obs.tab3 <- lapply(0 : max(obs.tab2$combined_clusters),
                   FUN = function(x) {
                       obs.tab.sub <- obs.tab2[obs.tab2$combined_clusters == x, ]
                       return(obs.tab.sub[1 : min(4, nrow(obs.tab.sub)), ])}) %>%
    do.call(what = rbind)
write.csv(obs.tab3, file = paste0(WK_DIR, "d9_top4_sepAnnot_per_cluster.csv"),
          quote = FALSE)
