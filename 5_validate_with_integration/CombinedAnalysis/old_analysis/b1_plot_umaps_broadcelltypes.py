import os
import gc
import sys
import scanpy as sc
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import matplotlib.backends.backend_pdf as mpdf


MODE = "all_4datasets"
PROJ_DIR = "/lustre/scratch119/casm/team-cvejic/haoliang/results/DownSyndrome/combined_analysis_res/"
OUT_DIR = f"{PROJ_DIR}{MODE}/broad_cell_types/"
if not os.path.exists(OUT_DIR):
    print("Creating folder:", OUT_DIR, file=sys.stderr)
    os.makedirs(OUT_DIR)
CELLTYPE_COLOURS = ["red", "coral", "orange", "goldenrod", "limegreen", "lightseagreen", "deepskyblue", "slateblue", "violet"]


def plot_umap(adata):
    env_src_lst = adata.obs["environment"].unique()
    n_subplots, n_cols = len(env_src_lst), 2
    n_rows = int(np.ceil(n_subplots / n_cols))
    for i, ct in enumerate(adata.obs["broad.cell.types"].unique()):
        print(f"Plotting {ct} with colour {CELLTYPE_COLOURS[i]}", file=sys.stderr)
        _, axes = plt.subplots(nrows=n_rows, ncols=2, figsize=(18, 18))
        for j, env_src in enumerate(env_src_lst):
            adata_sub = adata[adata.obs["environment"] == env_src, :].copy()
            sns.set_theme(style="white", font_scale=2)
            this_ax = sc.pl.umap(adata_sub, color="broad.cell.types", size=10, 
                                 groups=[ct], na_in_legend=False,
                                 components="1,2", use_raw=False,
                                 legend_loc=None, palette=[CELLTYPE_COLOURS[i]],
                                 ax=axes[int(j/n_cols), int(j%n_cols)], show=False)
            sc.pl.umap(adata_sub, color="new clusters", size=0, 
                       components="1,2", use_raw=False, 
                       legend_loc="on data", legend_fontsize=15,
                       ax=this_ax,
                       title=f"{ct}: {env_src}")
            del adata_sub
            gc.collect()
        plt.tight_layout()
        plt.savefig(f"{OUT_DIR}broad_celltypes_on_integrated_umaps_{MODE}_{ct.replace(' ', '_').replace('/', '_')}.png")


if __name__ == "__main__":
    adata = sc.read_h5ad(f"{PROJ_DIR}{MODE}/combined_adata_harmonised_reclustered.h5ad")
    celltype_map = pd.read_csv(f"{PROJ_DIR}/celltype_map.csv")
    adata.obs["leiden_latest"] = adata.obs["leiden_latest"].astype("str")
    adata.obs = pd.merge(adata.obs.reset_index(), celltype_map, how="left").set_index("index")
    adata.obs["broad.cell.types"].fillna("removed", inplace=True)
    adata.obs["leiden_latest"] = adata.obs["leiden_latest"].astype("category")
    adata.obs["broad.cell.types"] = adata.obs["broad.cell.types"].astype("category")
    plot_umap(adata)
