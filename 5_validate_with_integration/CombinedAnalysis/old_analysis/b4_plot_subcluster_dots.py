import gc
import sys
import scanpy as sc
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import matplotlib.backends.backend_pdf as mpdf

GENES = ["CD34", "SPINK2", "MLLT3", "GATA1", "TESPA1", "ALAS2", "GATA2", "AZU1",
         "CD14", "CD68", "FCN1", "NKG7", "PAX5", "IGHM", "CLEC4A", "MKI67"]

MODE = "all_4datasets"
PROJ_DIR = "/lustre/scratch119/casm/team-cvejic/haoliang/results/DownSyndrome/combined_analysis_res/"
OUT_DIR = f"{PROJ_DIR}{MODE}/fine_cell_types/"


def plot_dots_byenv(adata, pdf):
    sns.set_theme(style="white", font_scale=1.5)
    sc.pl.dotplot(adata, layer="log",
                  var_names=GENES, groupby="env annot",
                  use_raw=False, standard_scale=None, color_map='Reds',
                  dendrogram=False, figsize=(30, 10), show=True, 
                  linewidths=2, swap_axes=True, title="summarised dotplot")
    plt.subplots_adjust(bottom=0.5)
    pdf.savefig()
    plt.close()


def plot_dots_bynewclust(adata, pdf):
    for env in adata.obs["environment"].unique():
        adata_sub = adata[adata.obs["environment"].isin([env]), :]
        fct_nc_counts = adata_sub.obs[["leiden_latest", 
                                       "new clusters"]].value_counts().sort_values(ascending=False)
        tot_counts = {fct: 0 for fct in adata_sub.obs["leiden_latest"].unique()}
        for fct, nc in fct_nc_counts.index:
            print(f"\t{fct}:{nc}\t{fct_nc_counts[(fct, nc)]}", file=sys.stderr)
            tot_counts[fct] += fct_nc_counts[(fct, nc)]
        princp_cluster_lst, accum_counts = [], {fct: 0 for fct in adata_sub.obs["leiden_latest"].unique()}
        for fct, nc in fct_nc_counts.index:
            if accum_counts[fct] + fct_nc_counts[(fct, nc)] < tot_counts[fct] * 0.95:
                princp_cluster_lst += [f"{fct}:{nc}"]
            accum_counts[fct] += fct_nc_counts[(fct, nc)]
        print(princp_cluster_lst, file=sys.stderr)
        adata_sub = adata_sub[adata_sub.obs["new annot"].isin(princp_cluster_lst), :]
        sns.set_theme(style="white", font_scale=1.5)
        sc.pl.dotplot(adata_sub, layer="log",
                      var_names=GENES, groupby="new annot",
                      use_raw=False, standard_scale=None, color_map='Reds',
                      dendrogram=False, figsize=(30, 10), show=True, 
                      linewidths=2, swap_axes=True, title=env)
        plt.subplots_adjust(bottom=0.5)
        pdf.savefig()
        plt.close()


if __name__ == "__main__":
    # Load anndata and merge with broad cell types
    adata = sc.read_h5ad(f"{PROJ_DIR}{MODE}/combined_adata_harmonised_reclustered.h5ad")
    adata.X = adata.layers["scale"]
    celltype_map = pd.read_csv(f"{PROJ_DIR}/celltype_map.csv")
    adata.obs["leiden_latest"] = adata.obs["leiden_latest"].astype("str")
    adata.obs = pd.merge(adata.obs.reset_index(),
                         celltype_map, how="left").set_index("index")
    adata.obs["broad.cell.types"].fillna("removed", inplace=True)
    adata.obs["leiden_latest"] = adata.obs["leiden_latest"].astype("category")
    adata.obs["broad.cell.types"] = adata.obs["broad.cell.types"].astype("category")
    # Make annotations to check
    adata.obs["env annot"] = adata.obs["leiden_latest"].astype("str") + "\n" + adata.obs["environment"].astype("str")
    adata.obs["new annot"] = adata.obs["leiden_latest"].astype("str") + ":" + adata.obs["new clusters"].astype("str")
    # Plot by broad cell type
    for bct in ["HSC/Progenitors"]:
        pdfname = f"fine_celltype_dotplot_{bct.replace(' ', '_').replace('/', '_')}.pdf"
        pdf = mpdf.PdfPages(f"{OUT_DIR}{pdfname}")
        plot_dots_byenv(adata[adata.obs["broad.cell.types"].isin([bct]), GENES], pdf)
        plot_dots_bynewclust(adata[adata.obs["broad.cell.types"].isin([bct]), GENES], pdf)
        gc.collect()
        pdf.close()

