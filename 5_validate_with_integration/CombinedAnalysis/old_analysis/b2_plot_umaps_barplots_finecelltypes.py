import os
import gc
import sys
import math
import scanpy as sc
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import matplotlib.backends.backend_pdf as mpdf


MODE = "all_4datasets"
PROJ_DIR = "/lustre/scratch119/casm/team-cvejic/haoliang/results/DownSyndrome/combined_analysis_res/"
OUT_DIR = f"{PROJ_DIR}{MODE}/fine_cell_types/"
if not os.path.exists(OUT_DIR):
    print("Creating folder:", OUT_DIR, file=sys.stderr)
    os.makedirs(OUT_DIR)
COLOURS = ["red", "coral", "orange", "goldenrod", "limegreen",
           "lightseagreen", "deepskyblue", "slateblue", "violet"]


def plot_umap(adata, bct):
    # Plot UMAP by cell type
    env_src_lst = adata.obs["environment"].unique()
    n_subplots, n_cols = len(env_src_lst), 2
    n_rows = int(np.ceil(n_subplots / n_cols))
    for i, fct in enumerate(adata.obs["leiden_latest"].loc[adata.obs["broad.cell.types"] == bct].unique()):
        print(f"Plotting {fct} with colour {COLOURS[i]}", file=sys.stderr)
        _, axes = plt.subplots(nrows=n_rows, ncols=2, figsize=(18, 18))
        for j, env_src in enumerate(env_src_lst):
            adata_sub = adata[adata.obs["environment"] == env_src, :]
            sns.set_theme(style="white", font_scale=2)
            this_ax = sc.pl.umap(adata_sub, color="leiden_latest", size=30,
                                 groups=[fct], na_in_legend=False,
                                 components="1,2", use_raw=False,
                                 legend_loc=None, palette=[COLOURS[i]],
                                 ax=axes[int(j/n_cols), int(j%n_cols)], show=False)
            sc.pl.umap(adata, color="new clusters", size=0,
                       components="1,2", use_raw=False,
                       legend_loc="on data", legend_fontsize=15,
                       ax=this_ax,
                       title=f"{fct}: {env_src}")
            del adata_sub
            gc.collect()
        plt.tight_layout()
        plt.savefig(f"{OUT_DIR}fine_celltype_umap_{fct.replace(' ', '_').replace('/', '_')}.png")
        plt.close()


def plot_bars(obs_tab, pdf):
    env_lst = obs_tab["environment"].unique()
    sns.set_theme(font_scale=1.5, style="white")
    for fct in obs_tab["leiden_latest"].unique():
        _, axes = plt.subplots(len(env_lst), 1, figsize=(20, 23), sharex=True)
        for i, env in enumerate(env_lst):
            sns.countplot(data=obs_tab.loc[obs_tab["environment"].isin([env]) & obs_tab["leiden_latest"].isin([fct]), :],
                          x="new clusters", color="skyblue", ax=axes[i])
            for p in axes[i].patches:
                if not math.isnan(p.get_height()):
                    axes[i].annotate(int(p.get_height()),
                                     (p.get_x()+p.get_width()/2, p.get_height()), ha="center")
                else:
                    axes[i].annotate(0,
                                     (p.get_x()+p.get_width()/2, 0), ha="center")
            if i < len(env_lst) - 1:
                axes[i].set_title(f"{fct}: {env}")
                axes[i].set_xlabel(None)
        plt.tight_layout()
        pdf.savefig()
        plt.close()


if __name__ == "__main__":
    # Load anndata and merge with broad cell types
    adata = sc.read_h5ad(f"{PROJ_DIR}{MODE}/combined_adata_harmonised_reclustered.h5ad")
    celltype_map = pd.read_csv(f"{PROJ_DIR}/celltype_map.csv")
    adata.obs["leiden_latest"] = adata.obs["leiden_latest"].astype("str")
    adata.obs = pd.merge(adata.obs.reset_index(),
                         celltype_map, how="left").set_index("index")
    adata.obs["broad.cell.types"].fillna("removed", inplace=True)
    adata.obs["leiden_latest"] = adata.obs["leiden_latest"].astype("category")
    adata.obs["broad.cell.types"] = adata.obs["broad.cell.types"].astype("category")
    # Plot by broad cell type
    # for bct in adata.obs["broad.cell.types"].unique():
    for bct in ["HSC/Progenitors"]:
        plot_umap(adata, bct)
        gc.collect()
    # Plot bars
    pdfname = f"fine_celltype_countplot_{bct.replace(' ', '_').replace('/', '_')}.pdf"
    pdf = mpdf.PdfPages(f"{OUT_DIR}{pdfname}")
    plot_bars(adata.obs, pdf)
    pdf.close()
