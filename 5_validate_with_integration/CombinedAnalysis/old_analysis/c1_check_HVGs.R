rm (list = ls())
setwd("~/Projects/DownSyndrome/downsyndrome/PythonPipeline/CombinedAnalysis")

library(UpSetR)
library(ggplot2)

adata.vars <- read.csv("../../../combined_analysis_res/adata_vars_highly_variable_genes.csv",
                       header = T, row.names = 1)
adata.vars.idx <- rownames(adata.vars)
hvgs <- list("HVGs.combined" = adata.vars.idx[adata.vars$highly_variable == "True"],
             "HVGs.H_Liver" = adata.vars.idx[adata.vars$highly_variable_Healthy_Liver.3 == "True"],
             "HVGs.H_Femur" = adata.vars.idx[adata.vars$highly_variable_Healthy_Femur.1 == "True"],
             "HVGs.DS_Liver" = adata.vars.idx[adata.vars$highly_variable_DownSyndrome_Liver.2 == "True"],
             "HVGs.DS_Femur" = adata.vars.idx[adata.vars$highly_variable_DownSyndrome_Femur.0 == "True"])
all(hvgs$HVGs.H_Liver == hvgs$HVGs.H_Femur)
all(hvgs$HVGs.DS_Femur == hvgs$HVGs.DS_Femur)
upset(fromList(hvgs), nintersects = NA, order.by = "freq", text.scale = 2)
+
    ggsave(filename = "../../../combined_analysis_res/HVGs_comparison.png")
