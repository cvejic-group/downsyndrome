import gc
import sys
import scanpy as sc
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import matplotlib.backends.backend_pdf as mpdf

GENES = ["CD34", "SPINK2", "MLLT3", "GATA1", "TESPA1", "ALAS2", "GATA2", "AZU1",
         "CD14", "CD68", "FCN1", "NKG7", "PAX5", "IGHM", "CLEC4A", "MKI67"]
GENES = sorted(GENES)

MODE = "all_4datasets"
PROJ_DIR = "/lustre/scratch119/casm/team-cvejic/haoliang/results/DownSyndrome/combined_analysis_res/"
OUT_DIR = f"{PROJ_DIR}{MODE}/fine_cell_types/"


def plot_violin(gene_counts):
    cell_counts = gene_counts["new clusters"].value_counts().sort_values(ascending=False)
    total_cell_counts = cell_counts.sum()
    major_cluster_lst = []
    gene_counts["major cluster"] = "No"
    accumulated_counts = 0
    for icluster in cell_counts.index:
        print(f"\t{icluster}\t{cell_counts[icluster]}", file=sys.stderr)
        accumulated_counts += cell_counts[icluster]
        gene_counts.loc[gene_counts["new clusters"] == icluster, "major cluster"] = "Yes"
        if accumulated_counts > total_cell_counts * 0.95:
            break
    sns.set(rc={"figure.figsize": (30, 50)})
    sns.set_theme(style="white", font_scale=1.5)
    _, axs = plt.subplots(len(GENES) + 1, 1, sharex=True, sharey=False)
    sns.countplot(data=gene_counts, x="new clusters", hue="major cluster",
                  order=cell_counts.index, palette={"Yes": "tomato", "No": "gray"},
                  saturation=1, ax=axs[0])
    for i, g in enumerate(GENES):
        sns.violinplot(data=gene_counts, x="new clusters", y=g, hue="major cluster",
                       order=cell_counts.index, palette={"Yes": "royalblue", "No": "gray"},
                       saturation=1, ax=axs[i+1])
        axs[i+1].get_legend().remove()
        if i < len(GENES):
            axs[i].set_xlabel(None)
    plt.tight_layout()


def plot_box(gene_counts):
    sns.set(rc={"figure.figsize": (30, 50)})
    sns.set_theme(style="white", font_scale=1.5)
    _, axs = plt.subplots(len(GENES) + 1, 1, sharex=True, sharey=False)
    sns.countplot(data=gene_counts, x="new clusters",
                  color="tomato", saturation=1, ax=axs[0])
    for i, g in enumerate(GENES):
        sns.boxplot(data=gene_counts, x="new clusters", y=g, 
                    color="skyblue", saturation=1, ax=axs[i+1])
        if i < len(GENES):
            axs[i].set_xlabel(None)
    plt.tight_layout()


if __name__ == "__main__":
    # Load anndata and merge with broad cell types
    adata = sc.read_h5ad(f"{PROJ_DIR}{MODE}/combined_adata_harmonised_reclustered.h5ad")
    adata.X = adata.layers["scale"]
    celltype_map = pd.read_csv(f"{PROJ_DIR}/celltype_map.csv")
    adata.obs["leiden_latest"] = adata.obs["leiden_latest"].astype("str")
    adata.obs = pd.merge(adata.obs.reset_index(),
                         celltype_map, how="left").set_index("index")
    adata.obs["broad.cell.types"].fillna("removed", inplace=True)
    adata.obs["leiden_latest"] = adata.obs["leiden_latest"].astype("category")
    adata.obs["broad.cell.types"] = adata.obs["broad.cell.types"].astype("category")
    # Plot by broad cell type
    for bct in ["HSC/Progenitors"]:
        pdfname = f"fine_celltype_violinbox_{bct.replace(' ', '_').replace('/', '_')}.pdf"
        pdf = mpdf.PdfPages(f"{OUT_DIR}{pdfname}")
        fct_lst = adata.obs["leiden_latest"].loc[adata.obs["broad.cell.types"].isin([bct])].unique()
        for fct in fct_lst:
            idx_subset = adata.obs["leiden_latest"].isin([fct])
            gene_counts = pd.DataFrame(adata[idx_subset, GENES].X,
                                       columns=adata[idx_subset, GENES].var.index,
                                       index=adata[idx_subset, GENES].obs.index)
            gene_counts = pd.concat([gene_counts, adata[idx_subset, GENES].obs["new clusters"].astype("int")], axis=1)
            plot_violin(gene_counts)
            plt.suptitle(fct)
            pdf.savefig()
            # plot_box(gene_counts)
            # pdf.savefig()
            gc.collect()
        pdf.close()
