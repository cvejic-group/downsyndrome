import sys
import scanpy as sc
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt

# ========== Parameters ========== #
OUT_DIR = f"/lustre/scratch126/casm/team-cvejic/haoliang/pj_DownSyndrome/results/combined_analysis_res4rev/"
GENES2PLT = ["CD34", "SPINK2", "MLLT3", "GATA1", "KLF1", "TESPA1", "AHSP", "ALAS2", "HBA1",
             "GYPA", "GATA2", "HDC", "CPA3", "ITGA2B", "GP9", "MPO", "AZU1", "SPI1", "LYZ", 
             "CD14", "CD68", "S100A9", "MNDA", "FCN1", "CD163", "MS4A7", "CTSB", "NKG7", 
             "PRF1", "GZMA", "IL7R", "DHFR", "PAX5", "MME", "IGLL1", "IGHM", "CD79A", "CD19",
             "JCHAIN", "IRF8", "CLEC4C", "IL3RA", "CD1C", "CLEC4A", "CLEC10A", "MKI67", 
             "ALB", "AFP", "DCN", "COL1A1", "COL3A1", "RBP1", "ACTA2", "TAGLN", "MYL9", "CDH5",
             "KDR", "STAB1", "STAB2", "LYVE1", "KRT19"]


if __name__ == "__main__":
    # Read scanpy object
    adata = sc.read_h5ad(f"{OUT_DIR}e_combined_adata_harmonised_reclustered.h5ad")
    adata.X = adata.layers["log"]
    sc.tl.leiden(adata, resolution=3, random_state=42, key_added="new_clusters_res3")
    sc.tl.umap(adata, random_state=42, n_components=2, init_pos="random")
    cluster_size = {nc: sum(adata.obs["new_clusters_res3"] == nc)
                    for nc in adata.obs["new_clusters_res3"].unique()}
    pd.DataFrame.from_dict(cluster_size, orient="index").to_csv(f"{OUT_DIR}resolu3/a_cluster_size.csv")
    adata.var.to_csv(f"{OUT_DIR}resolu3/b1_combined_adata_var_harmonised_reclustered.csv")
    adata.obs.to_csv(f"{OUT_DIR}resolu3/b2_combined_adata_obs_harmonised_reclustered.csv")
    # Plot heatmap 
    for clust in adata.obs["new_clusters_res3"].cat.categories:
        print(clust, file=sys.stderr)
        adata_sub = adata[adata.obs["new_clusters_res3"].isin([clust]), :]
        # adata_sub.write_h5ad(f"{OUT_DIR}stored_by_subclusters/cluster{clust}.h5ad")
        sns.set_theme(style="white", font_scale=1.5)
        sc.pl.heatmap(adata_sub, var_names=GENES2PLT, groupby="environment", standard_scale=None,
                      use_raw=False, log=False, cmap="magma", swap_axes=False,
                      show_gene_labels=True, show=False, figsize=(20, 12))
        plt.savefig(f"{OUT_DIR}resolu3/cluster{clust}.png")
        plt.close()
