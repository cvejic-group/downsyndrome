import os
import gc
import sys
import math
import scanpy as sc
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import matplotlib.backends.backend_pdf as mpdf


MODE = "all_4datasets"
PROJ_DIR = "/lustre/scratch119/casm/team-cvejic/haoliang/results/DownSyndrome/combined_analysis_res/"
OUT_DIR = f"{PROJ_DIR}{MODE}/"
if not os.path.exists(OUT_DIR):
    print("Creating folder:", OUT_DIR, file=sys.stderr)
    os.makedirs(OUT_DIR)
COLOURS = ["tomato", "orange", "limegreen", "deepskyblue", "darkblue", "violet"]


def plot_umap(adata2plot, col2colour, pdf=None, figdir=None, palette=None): 
    for ct, col in palette.items():
        print(f"{ct}: {col}", file=sys.stderr)
    if len(palette) == 1:
        figtype = "HSCsProgenitors"
    else:
        figtype = "FineCellTypes"
    # Plot UMAP by cell type
    env_src_lst = adata2plot.obs["environment"].unique()
    n_subplots, n_cols = len(env_src_lst), 2
    n_rows = int(np.ceil(n_subplots / n_cols))
    sns.set_theme(style="white", font_scale=2)
    for j, env_src in enumerate(env_src_lst):
        _, ax = plt.subplots(1, 1, figsize=(9, 9))
        sc.pl.umap(adata2plot[adata2plot.obs["environment"]== env_src, :],
                   use_raw=False, components="1,2",
                   color=col2colour, size=30,
                   legend_loc=None, title=None,
                   palette=palette,
                   ax=ax)
        ax.set_xlabel(None)
        ax.set_ylabel(None)
        plt.tight_layout()
        if pdf is not None:
            pdf.savefig()
        elif figdir is not None:
            plt.savefig(f"{figdir}{figtype}_{env_src}.png")
        plt.close()
        gc.collect()


if __name__ == "__main__":
    # Load anndata and merge with broad cell types
    adata = sc.read_h5ad(f"{PROJ_DIR}{MODE}/combined_adata_harmonised_reclustered.h5ad")
    celltype_map = pd.read_csv(f"{PROJ_DIR}celltype_map.csv")
    adata.obs["leiden_latest"] = adata.obs["leiden_latest"].astype("str")
    adata.obs = pd.merge(adata.obs.reset_index(),
                         celltype_map,
                         how="left").set_index("index")
    adata.obs["broad.cell.types"].fillna("removed", inplace=True)
    adata.obs["leiden_latest"][(adata.obs["environment"] == "Healthy_Femur") & (adata.obs["leiden_latest"] == "MEMPs")] = "Cycling MEMPs"
    adata.obs["broad cell types"] = [x if x == "HSC/Progenitors" else pd.NA for x in adata.obs["broad.cell.types"]]
    adata.obs["fine cell types"] = [adata.obs.loc[i, "leiden_latest"] if adata.obs.loc[i, "broad.cell.types"] == "HSC/Progenitors" else pd.NA
                                    for i in adata.obs.index]
    # Plot by broad cell type
    # pdf = mpdf.PdfPages(f"{OUT_DIR}umaps_HSC_Progenitors.pdf")
    plot_umap(adata, "broad cell types", pdf=None, figdir=OUT_DIR, palette={"HSC/Progenitors": "red"})
    adata.obs.to_csv(f"{OUT_DIR}adata_obs_HSC_Progenitors.csv")
    plot_umap(adata, "fine cell types", pdf=None, figdir=OUT_DIR, 
              palette={fct: COLOURS[i] for i, fct in enumerate(adata.obs["fine cell types"].dropna().unique())})
    gc.collect()
    # pdf.close()
