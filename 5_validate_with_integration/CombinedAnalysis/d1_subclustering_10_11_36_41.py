import gc
import scanpy as sc
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import matplotlib.backends.backend_pdf as mpdf

# clusters in 10 subclusters - 10, 11, 36, 41
# Epression of SPINK2, GATA1, KLF1, MPO, ITGA2B, GYPA, HDC, TESPA, AHSP, MKI67

# ========== Path Parameters ========== #
OUT_DIR = f"/lustre/scratch126/casm/team-cvejic/haoliang/pj_DownSyndrome/results/combined_analysis_res4rev/"

CLUST2DIVIDE, NSUBCLUST = ["10", "11", "36", "41"], 10
GENES2PLT = ["SPINK2", "GATA1", "KLF1", "MPO", "ITGA2B", "GYPA", "HDC", "TESPA", "AHSP", "MKI67"]
OLD_COL, NEW_COL = "new_clusters", "new_clusters_v2"

if __name__ == "__main__":
    # Read scanpy object
    adata = sc.read_h5ad(f"{OUT_DIR}e_combined_adata_harmonised_reclustered.h5ad")
    adata.X = adata.layers["log"]
    print(adata.X.todense(), file=sys.stderr)
    # Subclustering
    for clust in CLUST2DIVIDE:
        ## Select subset for re-clustering
        idx_subset = adata.obs[OLD_COL].isin([clust])
        ## K-means clustering
        X = np.array(adata[idx_subset, :].obsm["X_umap"])
        labels = [str(x) for x in KMeans(n_clusters=NSUBCLUST, random_state=42).fit(X).labels_]
        ## Assign labels
        adata.obs[NEW_COL] = adata.obs[OLD_COL].astype("str")
        adata.obs[NEW_COL].loc[idx_subset] = adata.obs[OLD_COL].loc[idx_subset].astype("str") + ":" + labels
        print(adata.obs[NEW_COL].value_counts(), "\n", file=sys.stderr)
    # Write result object
    adata.write_h5ad(f"{OUT_DIR}f1_combined_adata_harmonised_reclustered.v2.h5ad")
    # Final figure PDF
    pdf = mpdf.PdfPages(f"{OUT_DIR}f2_gene_expression_plots.v2.pdf")
    ## Summary heatmap
    sc.pl.heatmap(adata[adata.obs[OLD_COL].isin(CLUST2DIVIDE), :],
                  var_names=GENES2PLT, groupby=NEW_COL, standard_scale="var",
                  use_raw=False, log=False, cmap="magma",
                  swap_axes=True, show_gene_labels=True, show=False, figsize=(20, 12))
    pdf.savefig()
    plt.close()
    ## Ridge plot
    for gene in GENES2PLT:
        sns.set_theme(style="white", font_scale=1)
        _, axes = plt.subplots(len(subclusts), 1, figsize=(12, len(subclusts) * 1), sharex=True)
        for i, clust in enumerate(CLUST2DIVIDE):
            sns.kdeplot(data=pd.DataFrame(adata[adata.obs[NEW_COL] == clust,
                                                adata.var.index == gene].X.todense(),
                                          columns=[gene]),
                        x=gene, ax=axes[i], fill=True)
            axes[i].text(0, 0.2, clust, fontweight="bold", ha="left", va="center", transform=axes[i].transAxes)
            axes[i].spines["top"].set_visible(False)
            axes[i].spines["right"].set_visible(False)
            axes[i].spines["left"].set_visible(False)
            axes[i].set_xlabel(None)
            axes[i].set_ylabel(None)
        plt.tight_layout()
        plt.suptitle(gene)
        pdf.savefig()
        plt.close()
    # Close PDF
    pdf.close()
