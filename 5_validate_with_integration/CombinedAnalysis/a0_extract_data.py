import os
import gc
import sys
import scanpy as sc

from random import sample

PROJ_DIR = "/lustre/scratch119/casm/team-cvejic/haoliang/results/DownSyndrome/combined_analysis_res/"
IN_DIR = f"{PROJ_DIR}all_combined/"
OUT_DIR = f"{PROJ_DIR}test/"
if not os.path.exists(OUT_DIR):
    print("Creating folder:", OUT_DIR, file=sys.stderr)
    os.makedirs(OUT_DIR)


if __name__ == "__main__":
    adata = sc.read_h5ad(f"{IN_DIR}combined_adata_harmonised.h5ad")
    print(adata.obs["leiden_latest"].unique(), file=sys.stderr)
    adata[sample(list(range(adata.n_obs)), 10000), :].write(f"{OUT_DIR}combined_adata_harmonised_sub.h5ad",
                                                            compression="gzip" )
    gc.collect()
