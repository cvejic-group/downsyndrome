import os
import gc
import sys
import scanpy as sc
import pandas as pd
import harmonypy as hm
import matplotlib.pyplot as plt
import seaborn as sns

PROJ_DIR = "/lustre/scratch126/casm/team-cvejic/haoliang/pj_DownSyndrome/results/"
IN_DIR = f"{PROJ_DIR}annotated_data/"

OUT_DIR = f"{PROJ_DIR}combined_analysis_res4rev/"

def extract_rank_info(adata, key, field):
    mk_info = pd.DataFrame(adata.uns[key][field])
    mk_info.rename(index={i: f"gene{int(i) + 1}" for i in mk_info.index},
                   columns={i: f"cluster{i}" for i in mk_info.columns},
                   inplace=True)
    mk_info_long = pd.melt(mk_info.reset_index().rename(columns={"index": "gene"}),
                           id_vars="gene", var_name="cluster",
                           value_name=field).set_index(["cluster", "gene"])
    return(mk_info, mk_info_long)

if __name__ == "__main__":
    adata = sc.read_h5ad(f"{OUT_DIR}combined_adata.h5ad")
    # Raw counts
    adata.layers["counts"] = adata.X.copy()
    # Normalised counts
    sc.pp.normalize_per_cell(adata, counts_per_cell_after=1e4)
    # Logarithmised counts
    sc.pp.log1p(adata)
    adata.layers["log"] = adata.X.copy()
    # Scaled counts
    sc.pp.scale(adata, max_value=10)
    print("Scaling finished", file=sys.stderr)
    
    # Set HVGs by union across the datasets
    adata.var["highly_variable"] = False
    for env_src in adata.obs["environment"].cat.categories:
        adata.var["highly_variable"] = (adata.var["highly_variable"].astype("bool") | adata.var[f"highly_variable-{env_src}"].astype("bool"))
    # PCA
    sc.tl.pca(adata, n_comps=50, svd_solver="arpack", use_highly_variable=True)
    # Harmony
    ho = hm.run_harmony(adata.obsm["X_pca"][:, :15], adata.obs, ["patient_sample"],
                        max_iter_kmeans=25, max_iter_harmony=500)
    adata.obsm["X_pca_harmonize"] = ho.Z_corr.T
    # Clustering
    sc.pp.neighbors(adata, n_pcs=adata.obsm["X_pca_harmonize"].shape[1],
                    use_rep="X_pca_harmonize", knn=True, random_state=42,
                    method="umap", metric="euclidean")
    sc.tl.leiden(adata, resolution=2, random_state=42, key_added="new_clusters")
    sc.tl.umap(adata, random_state=42, n_components=2, init_pos="random")
    cluster_size = {nc: sum(adata.obs["new_clusters"] == nc)
                    for nc in adata.obs["new_clusters"].unique()}
    pd.DataFrame.from_dict(cluster_size, orient="index").to_csv(f"{OUT_DIR}a_cluster_size.csv")
    adata.var.to_csv(f"{OUT_DIR}b1_combined_adata_var_harmonised_reclustered.csv")
    adata.obs.to_csv(f"{OUT_DIR}b2_combined_adata_obs_harmonised_reclustered.csv")
    # Plot UMAP after clustering
    n_ctypes, ncols = len(adata.obs["new_clusters"].unique()), 5
    nrows = int(n_ctypes / ncols) + 1
    sns.set_theme(style="white", font_scale=1.5)
    _, axes = plt.subplots(nrows, ncols, figsize=(ncols * 5, nrows * 5))
    for i in range(n_ctypes):
        sc.pl.umap(adata, color="new_clusters", components="1,2", groups=[str(i)],
                   show=False, use_raw=False, ax=axes[int(i / ncols), i % ncols],
                   title=f"cluster {i}", palette=["red"], legend_loc=None, size=20)
    for i in range(n_ctypes, nrows * ncols):
        axes[int(i / ncols), i % ncols].set_axis_off()
    plt.tight_layout()
    plt.savefig(f"{OUT_DIR}c_clusteringUMAPs.new_clusters.png", dpi=200)
    adata.X = adata.layers["log"]
    gc.collect()
    # Marker genes
    sc.tl.rank_genes_groups(adata, groupby="new_clusters", n_genes=100, pts=True, rankby_abs=False,
                            method="wilcoxon", corr_method="bonferroni", key_added="rank_genes_groups")
    total_info = None 
    for i, field in enumerate(["names", "scores", "logfoldchanges", "pvals_adj"]):
        info, info_long = extract_rank_info(adata, "rank_genes_groups", field)
        info.to_csv(f"{OUT_DIR}d{i + 1}_markerGene_{field}.new_clusters.csv")
        if field == "names":
            total_info = info_long
        else:
            total_info = pd.concat([total_info, info_long], axis=1)
    adata.uns["rank_genes_groups"]["pts"].to_csv(f"{OUT_DIR}d5_markerGene_pt.new_clusters.csv")
    total_info.to_csv(f"{OUT_DIR}d6_markerGene_allinfo.new_clusters.csv")
    adata.write_h5ad(f"{OUT_DIR}e_combined_adata_harmonised_reclustered.h5ad")
    print("Analysis completed.\n\n", file=sys.stderr)
