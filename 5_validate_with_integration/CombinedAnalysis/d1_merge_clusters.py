import sys
import gc
import scanpy as sc


MERG_STR_LST = ["0+1+8+32+34+55+58+64+66=hepatocytes",
                "2+4+7+13+19+21+28+30+35+40+42+45+46+49+52+62=erythroid",
                "5+29+43+61=NK cells",
                "9=inflammatory macro",
                "31=tolerogenic macro",
                "36=mast cells",
                "12=megakaryocytes",
                "14=HSCs",
                "24=CAR cells",
                "33=endothelial cells",
                "39=pericytes",
                "53=cDC2",
                "65=muscle cells",
                "18=sinusoidal cells"]
OUT_DIR = "/lustre/scratch126/casm/team-cvejic/haoliang/pj_DownSyndrome/results/combined_analysis_res4rev/stored_by_subclusters/"


if __name__ == "__main__":
    for merg_str in MERG_STR_LST:
        clust_lst, targ = merg_str.split("=")
        clust_lst = clust_lst.split("+")
        print(clust_lst, targ, file=sys.stderr)
        adata_lst = [sc.read_h5ad(f"{OUT_DIR}cluster{clust}.h5ad") for clust in clust_lst]
        adata = adata_lst[0].concatenate(adata_lst[1:], join="inner", index_unique="-",
                                         batch_key="initial_cluster",
                                         batch_categories=[f"cluster{clust}" for clust in clust_lst])
        del adata_lst
        gc.collect()
        print(adata, file=sys.stderr)
        adata.write_h5ad(f"{OUT_DIR}stored_by_subclusters_group1/{targ.replace(' ', '_')}.h5ad")
