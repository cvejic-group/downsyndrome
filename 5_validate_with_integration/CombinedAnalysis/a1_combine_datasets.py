import os
import gc
import sys
import scanpy as sc
import pandas as pd
import harmonypy as hm

PROJ_DIR = "/lustre/scratch119/casm/team-cvejic/haoliang/results/DownSyndrome/"
IN_DIR = f"{PROJ_DIR}annotated_data/"

MODE = "liver_datasets"
OUT_DIR = f"{PROJ_DIR}combined_analysis_res/{MODE}/"
if not os.path.exists(OUT_DIR):
    print("Creating folder:", OUT_DIR, file=sys.stderr)
    os.makedirs(OUT_DIR)


def merge_dataset(env_src_lst):
    # Load stored adata
    adata_lst = [sc.read_h5ad(f"{IN_DIR}10X_{env_src}.h5ad")
                 for env_src in env_src_lst]
    combined = None
    for i in range(len(adata_lst)):
        env_src = env_src_lst[i]
        print(f"=====> {env_src}", file=sys.stderr)
        print("--- Before selection:", file=sys.stderr)
        print(adata_lst[i], file=sys.stderr)
        # Parse latest clustring results
        a = [col[8:] for col in adata_lst[i].obs.columns if 'leiden_v' in col]
        max_num = max([int(x) for x in a if x.isdigit()])
        col_name = f"leiden_v{str(max_num)}"
        print(f"\tUsing {col_name} as the latest clustering column.",
              file=sys.stderr)
        adata_lst[i].obs.rename(columns={col_name: "leiden_latest"}, inplace=True)
        # Only keep relevant columns in observations and variables
        adata_lst[i].obs = adata_lst[i].obs[["patient_sample", "leiden_latest"]]
        adata_lst[i].obs["environment"] = env_src
        adata_lst[i].var = adata_lst[i].var[["Ensembl", "Chromosome", "highly_variable"]]
        adata_lst[i].var.rename(columns={x: f"{x}_{env_src}"
                                         for x in adata_lst[i].var.columns},
                                inplace=True)
        del adata_lst[i].obsm, adata_lst[i].varm, adata_lst[i].obsp, adata_lst[i].uns
        print("--- After selection:", file=sys.stderr)
        print(adata_lst[i], file=sys.stderr)
    # Merge this adata into the combined variable
    combined = adata_lst[0].concatenate(adata_lst[1:],
                                        join="outer",
                                        index_unique="-") if len(adata_lst) > 1 else adata_lst[0]
    for col in combined.obs.columns:
        print(f"Categorising {col} in combined.obs", file=sys.stderr)
        combined.obs[col] = combined.obs[col].astype("category")
    for col in combined.var.columns:
        print(f"Categorising {col} in combined.var", file=sys.stderr)
        combined.var[col] = combined.var[col].astype("category")
    del adata_lst
    gc.collect()
    print(f"=====> all combined", file=sys.stderr)
    print(combined, file=sys.stderr)
    return combined


def transform_counts(combined_adata):
    # Raw counts
    combined_adata.layers["counts"] = combined_adata.X.copy()
    # Normalised counts
    sc.pp.normalize_per_cell(combined_adata, counts_per_cell_after=1e4)
    combined_adata.layers["norm"] = combined_adata.X.copy()
    # Logarithmised counts
    sc.pp.log1p(combined_adata)
    combined_adata.layers["log"] = combined_adata.X.copy()
    combined_adata.raw = combined_adata.copy()
    # Scaled counts
    sc.pp.scale(combined_adata, max_value=10)
    combined_adata.layers["scale"] = combined_adata.X.copy()
    return combined_adata


def harmonise_adata(transformed_adata):
    # HVG detection
    transformed_adata.X = transformed_adata.layers["log"]
    sc.pp.highly_variable_genes(transformed_adata,
                                min_mean=0.00125, max_mean=3, min_disp=0.5, 
                                batch_key="patient_sample")
    # PCA
    transformed_adata.X = transformed_adata.layers["scale"]
    sc.tl.pca(transformed_adata, n_comps=50,
              svd_solver='auto', use_highly_variable=True)
    # Harmony
    ho = hm.run_harmony(transformed_adata.obsm['X_pca'][:, :15],
                        transformed_adata.obs,
                        ["patient_sample"],
                        max_iter_kmeans=25,
                        max_iter_harmony=500)
    transformed_adata.obsm["X_pca_harmonize"] = ho.Z_corr.T
    return transformed_adata


def reclustering_adata(harmonised_adata):
    sc.pp.neighbors(harmonised_adata,
                    n_pcs=harmonised_adata.obsm["X_pca_harmonize"].shape[1],
                    use_rep="X_pca_harmonize", knn=True, random_state=42,
                    method='umap', metric='euclidean')
    sc.tl.leiden(harmonised_adata, resolution=1, random_state=42, key_added='new clusters')
    sc.tl.umap(harmonised_adata, random_state=42, n_components=2, init_pos='random')
    return harmonised_adata


if __name__ == "__main__":
    if MODE == "all_4datasets":
        print("Combining all 4 datasets...", file=sys.stderr)
        adata = merge_dataset(["DownSyndrome_Femur", "Healthy_Femur", 
                               "DownSyndrome_Liver", "Healthy_Liver"])
    elif MODE == "femur_datasets":
        print("Combining Femur datasets...", file=sys.stderr)
        adata = merge_dataset(["DownSyndrome_Femur", "Healthy_Femur"])
    elif MODE == "liver_datasets":
        print("Combining Liver datasets...", file=sys.stderr)
        adata = merge_dataset(["DownSyndrome_Liver", "Healthy_Liver"])
    elif MODE in ["DownSyndrome_Femur", "Healthy_Femur", 
                  "DownSyndrome_Liver", "Healthy_Liver"]:
        print("Using single dataset:", MODE, file=sys.stderr)
        adata = merge_dataset([MODE])
    else:
        print("ERROR: unknown running MODE!", file=sys.stderr)
        sys.exit(1)
    adata = transform_counts(adata)
    gc.collect()
    adata = harmonise_adata(adata)
    gc.collect()
    adata = reclustering_adata(adata)
    gc.collect()
    cluster_size = {nc: sum(adata.obs['new clusters'] == nc)
                    for nc in adata.obs["new clusters"].unique()}
    pd.DataFrame.from_dict(cluster_size, orient="index").to_csv(f"{OUT_DIR}cluster_size.csv")
    adata.write(f"{OUT_DIR}combined_adata_harmonised_reclustered.h5ad", compression="gzip")
    adata.var.to_csv(f"{OUT_DIR}combined_adata_var_harmonised_reclustered.csv")
    adata.obs.to_csv(f"{OUT_DIR}combined_adata_obs_harmonised_reclustered.csv")
    print("Combining datasets completed.\n\n", file=sys.stderr)
