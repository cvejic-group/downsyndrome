import gc
import sys
import scanpy as sc
import matplotlib.pyplot as plt

IN_DIR = "/lustre/scratch126/casm/team-cvejic/haoliang/pj_DownSyndrome/results/combined_analysis_res4rev/"
OUT_DIR = "/lustre/scratch126/casm/team-cvejic/haoliang/pj_DownSyndrome/results/compare_transfers_and_integrations/"

GENE2PLT = ["CD34", "SPINK2", "MLLT3", "GATA1", "KLF1", "TESPA1", "AHSP", "ALAS2", "HBA1", "GYPA", "GATA2", 
            "HDC", "CPA3", "ITGA2B", "GP9", "MPO", "AZU1", "SPI1", "LYZ", "CD14", "CD68", "S100A9", "MNDA",
            "FCN1", "CD163", "MS4A7", "CTSB", "DCN", "ACP5", "MMP9", "CTSK", "IL7R", "NKG7", "PRF1", "GZMA",
            "IGLL1", "IGHM", "MME", "PAX5", "CD79A", "CD19", "JCHAIN", "IRF8", "CLEC4C", "IL3RA", "CD1C",
            "CLEC4A", "CLEC10A", "MKI67"]
CTYPE2PLT = ["HSCs", "MEMP", "Erythroid", "Mast cells", "Megakaryocytes", "Granulocyte prog", "Inflammatory macrophages",
             "Tolerogenic", "Kupffer cells", "Neutrophils", "NK cells", "B cells", "cDC", "pDCs"]
ANNOTLST = ["Hepatocytes=1+2+4+8+26+35+36+47+66", "Inflammatory macrophages=3",
            "Erythroid=7+10+11+12+16+17+20+21+23+28+29+31+41+44+45+50+59+60",
            "Endothelial=5+13+54", "HSCs=6+30", "cDC=9+40", "NK cells=14+37+39+43+51",
            "Megakaryocytes=15+48", "Fibroblasts=18+42+49", "Neutrophils=19",
            "Kupffer cells=22", "Granulocyte prog=24", "CAR cells=25+34+58",
            "Pericytes=27", "pDCs=32", "Mast cells=33", "B cells=52+64",
            "Stellate cells=53", "Schwann cells=61", "Osteolineage=63", 
            "Osteoclasts=65", "Tolerogenic=57", "MEMP=38+46+55+56+62"]
ANNOTMAP = {y: x.split("=")[0] for x in ANNOTLST for y in x.split("=")[1].split("+")}


if __name__ == "__main__":
    adata = sc.read_h5ad(f"{IN_DIR}results_scvi_back/a2_combined_adata.processed.scVI.h5ad")
    print(adata.X[adata.X > 5], file=sys.stderr)
    adata.obs["combi_annot"] = adata.obs["Combi_Clust"].map(ANNOTMAP)
    adata.obs["combi_annot"] = adata.obs["combi_annot"].astype("category")
    # Subset anndata
    adata = adata[(adata.obs["environment"] == "Healthy_Liver") & adata.obs["combi_annot"].isin(CTYPE2PLT), :].copy()
    adata.obs["combi_annot"] = adata.obs["combi_annot"].cat.reorder_categories(CTYPE2PLT)
    print(adata, file=sys.stderr)
    gc.collect()
    # Dot plots
    sc.pl.dotplot(adata, var_names=GENE2PLT, groupby="combi_annot", use_raw=False, standard_scale="var", color_map="Reds",
                  swap_axes=False, dendrogram=False, figsize=(18, 6), show=False,
                  title=f"scVI integration - standardisation by gene")
    plt.savefig(f"{OUT_DIR}dotplot-scVI.pdf", bbox_inches="tight", facecolor="white")
