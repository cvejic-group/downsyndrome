import sys
import pandas as pd
import scanpy as sc
import matplotlib.pyplot as plt
import matplotlib.backends.backend_pdf as mpdf

IN_DIR = "/lustre/scratch126/casm/team-cvejic/haoliang/pj_DownSyndrome/results/annotated_data/"
OUT_DIR = "/lustre/scratch126/casm/team-cvejic/haoliang/pj_DownSyndrome/results/compare_transfers_and_integrations/"

GENES = ["CD34", "SPINK2", "MLLT3", "GATA1", "KLF1", "TESPA1", "AHSP", "ALAS2", "HBA1", "GYPA", "GATA2", 
         "HDC", "CPA3", "ITGA2B", "GP9", "MPO", "AZU1", "SPI1", "LYZ", "CD14", "CD68", "S100A9", "MNDA",
         "FCN1", "CD163", "MS4A7", "CTSB", "ACP5", "MMP9", "CTSK", "IL7R", "NKG7", "PRF1", "GZMA",
         "IGLL1", "IGHM", "MME", "PAX5", "CD79A", "CD19", "JCHAIN", "IRF8", "CLEC4C", "IL3RA", "CD1C",
         "CLEC4A", "CLEC10A", "MKI67"]
CTYPE_ORD = {"Separate": ["HSCs/MPPs", "MEMPs", "Cycling MEMPs", "Early erythroid cells", "Late erythroid cells",
                          "Cycling erythroid cells", "Mast cells", "Megakaryocytes", "Granulocyte progenitors",
                          "Neutrophils", "Monocyte progenitors", "Inflammatory macrophages", "Kupffer cells",
                          "NK progenitors", "NK cells", "Pre pro B cells", "Pro B cells", "B cells", "pDCs", "cDC2"],
             "Popescu_transfer": ["HSCs/MPPs", "MEMP", "Erythroid", "Mast cells", "Megakaryocytes", "Granulocytes",
                                  "Mono-Macrophages", "NK cells", "B cells", "Rejected", "cDC", "pDCs", "ILC", "T cells"],
             "T21_transfer": ["HSCs/MPPs", "Cycling HSCs/MPPs", "MEMPs", "Erythroid", "Mast cells", "Megakaryocytes",
                              "Granulocytes", "Mono-Macrophages", "NK cells", "B cells", "pDCs", "cDC", "Rejected", "root"]}


if __name__ == "__main__":
    # Load table indicating 45497 cells
    celltab = pd.read_table(f"{OUT_DIR}Healthy_liver_annotations.txt")
    celltab["index"] = celltab["cellname"] + "-" + celltab["sample"]
    celltab.set_index("index", inplace=True)
    # Load anndata of Healthy_Liver
    adata = sc.read_h5ad(f"{IN_DIR}10X_Healthy_Liver.h5ad")
    # Parse anndata annotations
    a = [col[8:] for col in adata.obs.columns if "leiden_v" in col]
    max_num = max([int(x) for x in a if x.isdigit()])
    col_name = f"leiden_v{str(max_num)}"
    print(f"\tUsing {col_name} as the latest clustering column.",
          file=sys.stderr)
    adata.obs.rename(columns={col_name: "leiden_latest"}, inplace=True)
    # Only keep relevant columns in observations and variables
    adata.obs = adata.obs[["patient_sample", "leiden_latest", "sample"]]
    adata.var = adata.var[["Ensembl", "Chromosome", "highly_variable"]]
    del adata.obsm, adata.varm, adata.obsp, adata.uns
    adata.obs["cellname"] = ["-".join(x.split("-")[:2]) for x in adata.obs.index]
    adata.obs["index"] = adata.obs["cellname"].astype("str") + "-" + adata.obs["sample"].astype("str")
    adata.obs.set_index("index", inplace=True)
    adata.obs = pd.concat([adata.obs, celltab[["Separate", "Integrated", "Popescu_transfer", "T21_transfer"]]], axis=1)
    # Normalisation - log1p transformation
    sc.pp.normalize_total(adata, target_sum=1e4)
    sc.pp.log1p(adata)
    # Select 45497 cells
    assert sum([x in adata.obs.index for x in celltab.index]) == 45497
    adata_sel = adata[celltab.index, :].copy()
    print(adata_sel, file=sys.stderr)
    # Plot dots
    pdf = mpdf.PdfPages(f"{OUT_DIR}dotplots-transfers.pdf")
    for grp_col in CTYPE_ORD.keys():
        adata_sel.obs[grp_col] = adata_sel.obs[grp_col].astype("category").cat.reorder_categories(CTYPE_ORD[grp_col])
        sc.pl.dotplot(adata_sel, var_names=GENES, groupby=grp_col, use_raw=False, standard_scale="var", color_map="Reds",
                      swap_axes=False, dendrogram=False, figsize=(18, 6), show=False,
                      title=f"{grp_col} - standardisation by gene")
        pdf.savefig(bbox_inches="tight", facecolor="white")
        plt.close()
    pdf.close()
