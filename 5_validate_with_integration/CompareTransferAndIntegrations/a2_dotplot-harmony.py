import gc
import sys
import scanpy as sc
import matplotlib.pyplot as plt

IN_DIR = "/lustre/scratch126/casm/team-cvejic/haoliang/pj_DownSyndrome/results/combined_analysis_res4rev/"
OUT_DIR = "/lustre/scratch126/casm/team-cvejic/haoliang/pj_DownSyndrome/results/compare_transfers_and_integrations/"

GENE2PLT = ["CD34", "SPINK2", "MLLT3", "GATA1", "KLF1", "TESPA1", "AHSP", "ALAS2", "HBA1", "GYPA", "GATA2", 
            "HDC", "CPA3", "ITGA2B", "GP9", "MPO", "AZU1", "SPI1", "LYZ", "CD14", "CD68", "S100A9", "MNDA",
            "FCN1", "CD163", "MS4A7", "CTSB", "DCN", "ACP5", "MMP9", "CTSK", "IL7R", "NKG7", "PRF1", "GZMA",
            "IGLL1", "IGHM", "MME", "PAX5", "CD79A", "CD19", "JCHAIN", "IRF8", "CLEC4C", "IL3RA", "CD1C",
            "CLEC4A", "CLEC10A", "MKI67"]
CTYPE2PLT = ["HSCs", "Cycling HSC", "MEMP", "Erythroid", "Mast cells", "Megakaryocytes", "Granulocyte progenitors",
             "Granulocytes", "Monocyte progenitors", "Inflammatory macrophages", "Tolerogenic macrophages",
             "Kupffer cells", "Collagen+ myeloid cells", "Osteoclasts", "NK", "B cells", "cDC2", "pDCs"]
ANNOTLST = ["Hepatocytes=1+3+8+26+27+35+47+70+30", "Erythroid=2+6+12+13+14+16+18+19+20+22+23+28+29+34+51+54+58+61+74",
            "HSCs=4+65+38", "Cycling HSC=46", "Endothelial cells=5+21+53+62+73", "Megakaryocytes=7", "CAR (and LEPR+CAR)=9",
            "Inflammatory macrophages=10+44", "NK=11+45+52+55+57+60+63", "cDC2=15+39+59+66", "MEMP=17", "Granulocytes=24+75",
            "Granulocyte progenitors=40", "B cells=37+48", "Kupffer cells=31", "Hepatic stellate cells=32+50+69",
            "Tolerogenic macrophages=64", "Osteoclasts=67", "Mast cells=33", "Fibroblasts=25+36+49+72", "pDCs=41",
            "Osteo-lineage=43", "Schwann=56", "Pericytes=42", "Collagen+ myeloid cells=68", "Monocyte progenitors=71"]
ANNOTMAP = {y: x.split("=")[0] for x in ANNOTLST for y in x.split("=")[1].split("+")}


if __name__ == "__main__":
    adata = sc.read_h5ad(f"{IN_DIR}results_noadjust/a2_combined_adata.processed.h5ad")
    print(adata[:50000, :].X.todense(), file=sys.stderr)
    adata.obs["combi_annot"] = adata.obs["combi_clust"].map(ANNOTMAP)
    adata.obs["combi_annot"] = adata.obs["combi_annot"].astype("category")
    # Subset anndata
    adata = adata[(adata.obs["environment"] == "Healthy_Liver") & adata.obs["combi_annot"].isin(CTYPE2PLT), :].copy()
    adata.obs["combi_annot"] = adata.obs["combi_annot"].cat.reorder_categories(CTYPE2PLT)
    print(adata, file=sys.stderr)
    gc.collect()
    # Dot plots
    sc.pl.dotplot(adata, var_names=GENE2PLT, groupby="combi_annot", use_raw=False, standard_scale="var", color_map="Reds",
                  swap_axes=False, dendrogram=False, figsize=(18, 6), show=False,
                  title=f"Harmony integration - standardisation by gene")
    plt.savefig(f"{OUT_DIR}dotplot-harmony.pdf", bbox_inches="tight", facecolor="white")
