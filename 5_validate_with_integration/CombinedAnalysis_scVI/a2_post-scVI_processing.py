import sys
import pegasus as pg
import matplotlib.pyplot as plt

PROJ_DIR = "/lustre/scratch126/casm/team-cvejic/haoliang/pj_DownSyndrome/results/combined_analysis_res4rev/"


if __name__ == "__main__":
    # Load and prepare
    data = pg.read_input(f"{PROJ_DIR}results_scvi/a1_combined_adata.harmonised.scVI.h5ad")
    # Post-integration processing
    pg.neighbors(data, K=100, rep="scVI", random_state=0)
    pg.leiden(data, rep="scVI", resolution=3.5, random_state=0, class_label="Combi_Clust")
    pg.umap(data, rep="scVI", n_components=2, n_neighbors=15, random_state=0)
    # Marker gene searching
    pg.de_analysis(data, "Combi_Clust")
    marker_dict = pg.markers(data, head=100)
    for k, v in marker_dict.items():
        v["up"].sort_values(by="log2FC", ascending=False).to_csv(f"{PROJ_DIR}/results_scvi/mkgs/up.{k}.csv")
        v["down"].sort_values(by="log2FC", ascending=True).to_csv(f"{PROJ_DIR}/results_scvi/mkgs/down.{k}.csv")
    pg.write_results_to_excel(marker_dict, f"{PROJ_DIR}results_scvi/mkgs/summary.xlsx")
    pg.write_output(data, f"{PROJ_DIR}results_scvi/a2_combined_adata.processed.scVI.h5ad", precision=6)
    data.var.to_csv(f"{PROJ_DIR}results_scvi/a3_tab.var.csv")
    data.obs.to_csv(f"{PROJ_DIR}results_scvi/a3_tab.obs.csv")
