import sys
import gc
import pegasus as pg
import matplotlib.pyplot as plt
import seaborn as sns


PROJ_DIR = "/lustre/scratch126/casm/team-cvejic/haoliang/pj_DownSyndrome/results/combined_analysis_res4rev/"


COLOUR_DICT = {"Stroma": "#000080", "Erythroid": "#F08080", "HSCs": "#FF0000", "Cycling HSCs": "#DC143C",
               "Megakaryocytes": "#663399", "Inflammatory macrophages": "#9932CC", "Tolerogenic macrophages": "#DA70D6",
               "NK cells": "#A0522D", "cDC": "#FFD700", "pDCs": "#FF8C00", "MEMP": "#FFA07A", "Neutrophils": "#CD853F",
               "Granulocyte prog": "#B8860B", "B cells": "#BDB76B", "Kupffer cells": "#FF69B4", "Osteoclasts": "#FFC0CB",
               "Mast cells": "#8B008B"}


if __name__ == "__main__":
    data = pg.read_input(f"{PROJ_DIR}results_scvi_back/b1_combined_anndata.subclustered.h5ad")
    data.obs["combi_annot_v3"] = data.obs["combi_annot_v2"].astype("str")
    print(data, file=sys.stderr)
    print("Removing non-sense cells", file=sys.stderr)
    data = data[~data.obs["combi_annot_v3"].str.startswith("Remove:"), :].copy()
    gc.collect()
    print(data, file=sys.stderr)
    print("Change niche cells to Stroma", file=sys.stderr)
    for ctype in ["Hepatocytes", "Stellate cells", "Endothelial", "Fibroblasts", "CAR cells",
                  "Osteolineage", "Schwann cells", "Pericytes"]:
        data.obs["combi_annot_v3"].replace(ctype, "Stroma", inplace=True)
    for i in range(1, 6):
        print(f"Change MEMP,{i} to MEMP", file=sys.stderr)
        data.obs["combi_annot_v3"].replace(f"MEMP,{i}", "MEMP", inplace=True)
    for i in range(1, 5):
        print(f"Change HSCs,{i} to HSCs", file=sys.stderr)
        data.obs["combi_annot_v3"].replace(f"HSCs,{i}", "HSCs", inplace=True)
    print(f"Change HSCs,5 to Cycling HSCs", file=sys.stderr)
    data.obs["combi_annot_v3"].replace(f"HSCs,5", "Cycling HSCs", inplace=True)
    data.obs["combi_annot_v3"] = data.obs["combi_annot_v3"].astype("category")
    print(data.obs["combi_annot_v3"].cat.categories, file=sys.stderr)
    data.obs["combi_annot_v3"] = data.obs["combi_annot_v3"].cat.reorder_categories(list(COLOUR_DICT.keys()))
    for env in data.obs["environment"].unique():
        pg.scatter(data, attrs="combi_annot_v3", basis="umap", restrictions=f"environment:{env}", show_background=True,
                   palettes=",".join(data.obs["combi_annot_v3"].cat.categories.map(COLOUR_DICT).tolist()),
                   panel_size=(6, 6), marker_size=20, dpi=600)
        plt.savefig(f"{PROJ_DIR}figures_final/scVI_UMAP.{env}.png", bbox_inches="tight", facecolor="white", dpi=600)
