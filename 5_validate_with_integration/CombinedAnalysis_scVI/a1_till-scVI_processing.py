import sys
import pegasus as pg
import matplotlib.pyplot as plt

PROJ_DIR = "/lustre/scratch126/casm/team-cvejic/haoliang/pj_DownSyndrome/results/combined_analysis_res4rev/"


if __name__ == "__main__":
    # Load and prepare
    data = pg.read_input(f"{PROJ_DIR}combined_adata.h5ad", genome="GRCh38")
    data.obs.rename(columns={"leiden_latest": "Sep_Annot"}, inplace=True)
    data.obs["organ"] = [x.split("_")[1] for x in data.obs["environment"]]
    data.obs["organ"] = data.obs["organ"].astype("category")
    data.obs["disease_status"] = [x.split("_")[0] for x in data.obs["environment"]]
    data.obs["disease_status"] = data.obs["disease_status"].astype("category")
    print(data.shape, file=sys.stderr)
    data.add_matrix("counts", mat=data.X)
    # Preprocessing
    pg.log_norm(data, norm_count=1e4)
    pg.identify_robust_genes(data, percent_cells=0.005)
    pg.highly_variable_features(data, n_top=5000, batch="patient_sample")
    pg.hvfplot(data, top_n=50, panel_size=(9, 5))
    plt.savefig(f"{PROJ_DIR}figures_scvi/a1_HVFplot.png",bbox_inches="tight", facecolor="white")
    # Integration by scVI
    print("Current .X matrix (should be norm-logged):", data.X[data.X > 0], file=sys.stderr)
    print("Previously stored raw matrix:", data.get_matrix("counts").todense(), file=sys.stderr)
    data.select_matrix("counts")
    print("Current .X matrix (should be raw):", data.X[data.X > 0], file=sys.stderr)
    print(data, file=sys.stderr)
    print(f"scVI on patient_sample, organ and disease_status.", file=sys.stderr)
    pg.run_scvi(data, categorical_covariate_keys=["patient_sample", "organ", "disease_status"], max_epochs=400)
    pg.write_output(data, f"{PROJ_DIR}results_scvi/a1_combined_adata.harmonised.scVI.h5ad", precision=6)
