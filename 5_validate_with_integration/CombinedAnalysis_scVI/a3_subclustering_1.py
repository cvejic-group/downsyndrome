import gc
import sys
import pegasus as pg
import pandas as pd
import matplotlib.backends.backend_pdf as mpdf
import matplotlib.pyplot as plt
import seaborn as sns


WKDIR = "/lustre/scratch126/casm/team-cvejic/haoliang/pj_DownSyndrome/results/combined_analysis_res4rev/"
GENES2PLT = ["CD34", "SPINK2", "MLLT3", "MKI67", "GATA1", "KLF1", "MPO", "IGLL1", "IGHM", "IL7R", "CD79A"] 
ANNOTLST = ["Hepatocytes=1+2+4+8+26+35+36+47+66", "Inflammatory macrophages=3+57",
            "Erythroid=7+10+11+12+16+17+20+21+23+28+29+31+41+44+45+50+59+60",
            "Endothelial=5+13+54", "HSCs=6+30", "cDC=9+40", "NK cells=14+37+39+43+51",
            "Megakaryocytes=15+48", "Fibroblasts=18+42+49", "Neutrophils=19",
            "Kupffer or Tolerogenic=22", "Granulocyte prog=24", "CAR cells=25+34+58",
            "Pericytes=27", "pDCs=32", "Mast cells=33", "B cells=52+64",
            "Stellate cells=53", "Schwann cells=61", "Osteolineage=63",
            "Osteoclasts=65", "MEMP=38+46+55+56+62"]
ANNOTMAP = {y: x.split("=")[0] for x in ANNOTLST for y in x.split("=")[1].split("+")}


if __name__ == "__main__":
    data = pg.read_input(f"{WKDIR}results_scvi/a2_combined_adata.processed.scVI.h5ad")
    data.obs["combi_annot_v1"] = data.obs["Combi_Clust"].map(ANNOTMAP)
    data.obs.loc[data.obs["environment"].str.endswith("Liver") \
                    & (data.obs["combi_annot_v1"] == "Kupffer or Tolerogenic"), "combi_annot_v1"] = "Kupffer cells"
    data.obs.loc[data.obs["environment"].str.endswith("Femur") \
                    & (data.obs["combi_annot_v1"] == "Kupffer or Tolerogenic"), "combi_annot_v1"] = "Tolerogenic macrophages"
    data.obs.loc[data.obs["environment"].str.endswith("Liver") \
                    & (data.obs["combi_annot_v1"] == "Osteoclasts"), "combi_annot_v1"] = "Remove: Osteoclasts in Liver"
    data.obs["combi_annot_v2"] = data.obs["combi_annot_v1"]
    # Subclustering HSCs
    data_hsc = data[data.obs["combi_annot_v1"] == "HSCs", :].copy()
    pg.leiden(data_hsc, rep="scVI", resolution=None, n_clust=5, random_state=0, class_label="combi_annot_v2") 
    data.obs.loc[data_hsc.obs.index, "combi_annot_v2"] = ("HSCs," + data_hsc.obs["combi_annot_v2"].astype("str")).astype("category")
    del data_hsc
    gc.collect()
    # Subclustering MEMPs
    data_memp = data[data.obs["combi_annot_v1"] == "MEMP", :].copy()
    pg.leiden(data_memp, rep="scVI", resolution=None, n_clust=5, random_state=0, class_label="combi_annot_v2")
    data.obs.loc[data_memp.obs.index, "combi_annot_v2"] = ("MEMP," + data_memp.obs["combi_annot_v2"].astype("str")).astype("category")
    del data_memp
    gc.collect()
    data.obs[["combi_annot_v2", "environment"]].value_counts().sort_index().reset_index().to_csv(f"{WKDIR}results_scvi/celltype_counts.subclusters.csv",
                                                                                                 index=False)
    pg.write_output(data, f"{WKDIR}results_scvi/b1_combined_anndata.subclustered.h5ad", precision=6)
    # Final figure PDF
    pdf = mpdf.PdfPages(f"{WKDIR}figures_scvi/subclustering_hscs_memps.pdf")
    ## Ridge plot
    for gene in GENES2PLT:
        assert gene in data.var_names
        sns.set_theme(style="white", font_scale=1)
        _, axes = plt.subplots(10, 1, figsize=(12, 10 * 1), sharex=True)
        for i, clust in enumerate([f"{x},{y}" for x in ["HSCs", "MEMP"] for y in range(1, 6)]):
            sns.kdeplot(data=pd.DataFrame(data[data.obs["combi_annot_v2"] == clust,
                                               data.var.index == gene].X.todense(),
                                          columns=[gene]),
                        x=gene, ax=axes[i], fill=True)
            axes[i].text(0, 0.2, clust, fontweight="bold", ha="left", va="center", transform=axes[i].transAxes)
            axes[i].spines["top"].set_visible(False)
            axes[i].spines["right"].set_visible(False)
            axes[i].spines["left"].set_visible(False)
            axes[i].set_xlabel(None)
            axes[i].set_ylabel(None)
        plt.tight_layout()
        plt.suptitle(gene)
        pdf.savefig()
        plt.close()
    # Close PDF
    pdf.close()
