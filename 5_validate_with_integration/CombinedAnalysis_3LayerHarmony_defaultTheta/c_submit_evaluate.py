#!/bin/bash

#BSUB -J c_evaluate
#BSUB -o logerr/c_evaluate.o%J 
#BSUB -e logerr/c_evaluate.e%J
#BSUB -q yesterday
#BSUB -sp 100
#BSUB -M 500000
#BSUB -R "select[mem>500000] rusage[mem=500000]"

set -e 

/lustre/scratch126/casm/team-cvejic/haoliang/miniconda3/envs/scanpy-ts21/bin/python c1_evaluate_cellcycle.py 
