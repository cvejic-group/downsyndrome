import gc
import sys
import scanpy as sc
import matplotlib.pyplot as plt
import matplotlib.backends.backend_pdf as mpdf

WKDIR = "/lustre/scratch126/casm/team-cvejic/haoliang/pj_DownSyndrome/results/combined_analysis_res4rev/"
GENE2PLT = {"blood": ["CD34", "SPINK2", "MLLT3", "GATA1", "KLF1", "TESPA1", "AHSP", "ALAS2", "HBA1", "GYPA", "GATA2", 
                      "HDC", "CPA3", "ITGA2B", "GP9", "MPO", "AZU1", "SPI1", "LYZ", "CD14", "CD68", "S100A9", "MNDA",
                      "FCN1", "CD163", "MS4A7", "CTSB", "DCN", "ACP5", "MMP9", "CTSK", "IL7R", "NKG7", "PRF1", "GZMA",
                      "IGLL1", "IGHM", "MME", "PAX5", "CD79A", "CD19", "JCHAIN", "IRF8", "CLEC4C", "IL3RA", "CD1C",
                      "CLEC4A", "CLEC10A", "MKI67"],
            "niche": ["ALB", "AFP", "DCN", "COL1A1", "COL3A1", "RBP1", "ACTA2", "TAGLN", "MYL9", "CDH5", "KDR", "CD34",
                      "STAB1", "STAB2", "LYVE1", "KRT19", "LEPR", "CXCL12", "PDGFRA", "PDGFRB", "PRRX1", "CD34", "NES", 
                      "ANGPT1", "KITLG", "PAX7", "MYOD1", "FOXC1", "SOX9", "RUNX2", "ACAN", "BGLAP", "IBSP", "SP7", "DCN",
                      "CDH5", "KDR", "STAB1", "STAB2", "TSPAN7", "LYVE1", "CDH11", "ACTA2", "TAGLN", "RGS5", "MCAM", "MYH11",
                      "MYL9", "NOTCH3", "MPZ", "NRXN1", "MKI67"]}
CTYPE2PLT = {"blood": ["HSCs", "Cycling HSC", "MEMP", "Erythroid", "Mast cells", "Megakaryocytes", "Granulocyte progenitors",
                       "Granulocytes", "Monocyte progenitors", "Inflammatory macrophages", "Tolerogenic macrophages",
                       "Kupffer cells", "Collagen+ myeloid cells", "Osteoclasts", "NK", "B cells", "cDC2", "pDCs"],
             "niche": ["Hepatocytes", "Hepatic stellate cells", "Endothelial cells", "Fibroblasts", "CAR (and LEPR+CAR)",
                       "Osteo-lineage", "Schwann", "Pericytes"]}
ANNOTLST = ["Hepatocytes=1+3+8+26+27+35+47+70+30", "Erythroid=2+6+12+13+14+16+18+19+20+22+23+28+29+34+51+54+58+61+74",
            "HSCs=4+65+38", "Cycling HSC=46", "Endothelial cells=5+21+53+62+73", "Megakaryocytes=7", "CAR (and LEPR+CAR)=9",
            "Inflammatory macrophages=10+44", "NK=11+45+52+55+57+60+63", "cDC2=15+39+59+66", "MEMP=17", "Granulocytes=24+75",
            "Granulocyte progenitors=40", "B cells=37+48", "Kupffer cells=31", "Hepatic stellate cells=32+50+69",
            "Tolerogenic macrophages=64", "Osteoclasts=67", "Mast cells=33", "Fibroblasts=25+36+49+72", "pDCs=41",
            "Osteo-lineage=43", "Schwann=56", "Pericytes=42", "Collagen+ myeloid cells=68", "Monocyte progenitors=71"]
ANNOTMAP = {y: x.split("=")[0] for x in ANNOTLST for y in x.split("=")[1].split("+")}
ENVANNT2RM = ["Tolerogenic macrophages|DownSyndrome_Liver", "Tolerogenic macrophages|Healthy_Liver", 
              "Kupffer cells|Healthy_Femur", "Kupffer cells|DownSyndrome_Femur", 
              "Collagen+ myeloid cells|DownSyndrome_Liver", "Collagen+ myeloid cells|Healthy_Liver", 
              "Osteoclasts|DownSyndrome_Liver", "Osteoclasts|Healthy_Liver"]


if __name__ == "__main__":
    adata = sc.read_h5ad(f"{WKDIR}results_noadjust/a2_combined_adata.processed.h5ad")
    adata.obs["combi_annot"] = adata.obs["combi_clust"].map(ANNOTMAP)
    adata.obs["env_combi_annot"] = adata.obs["combi_annot"] + "|" + adata.obs["environment"].astype("str")
    adata.obs["env_combi_annot"] = adata.obs["env_combi_annot"].astype("category")

    # Combined annotation mapping checking
    annot_df = adata.obs[["env_combi_annot"]].value_counts().reset_index()
    annot_df.rename(columns={0: "cell number"}, inplace=True)
    annot_df.to_csv(f"{WKDIR}results_final/Harmony_cellnumber_all_byenv.csv", index=False)
    # Save metadata table for cell type abundance
    adata.obs.to_csv(f"{WKDIR}results_final/Harmony_tab.obs.withannot.csv")
    # Dot plots
    for grp in ["blood", "niche"]:
        gene_lst, ctype_lst = GENE2PLT[grp], CTYPE2PLT[grp]
        print(gene_lst, ctype_lst, file=sys.stderr)
        adata_sub = adata[adata.obs["combi_annot"].isin(ctype_lst), :].copy()
        adata_sub = adata_sub[~adata_sub.obs["env_combi_annot"].isin(ENVANNT2RM), :].copy()
        gc.collect()
        adata_sub.obs["env_combi_annot"] = adata_sub.obs["env_combi_annot"].cat.reorder_categories([f"{x}|{y}"
                                                                                                    for x in ctype_lst
                                                                                                    for y in adata.obs["environment"].unique()
                                                                                                    if f"{x}|{y}" in adata_sub.obs["env_combi_annot"].tolist()])
        pdf = mpdf.PdfPages(f"{WKDIR}figures_final/Harmony_dotplot_{grp}_byenv.rmnonsense.pdf")
        sc.pl.dotplot(adata_sub, var_names=gene_lst, groupby="env_combi_annot", use_raw=False, standard_scale="var", color_map="Reds",
                      swap_axes=False, dendrogram=False, figsize=(18, 18), show=False, linewidths=2, title="standardisation by gene")
        pdf.savefig(bbox_inches="tight")
        plt.close()
        sc.pl.dotplot(adata_sub, var_names=gene_lst, groupby="env_combi_annot", use_raw=False, standard_scale=None, color_map="Reds",
                      swap_axes=False, dendrogram=False, figsize=(18, 18), show=False, linewidths=2, title="no standardisation")
        pdf.savefig(bbox_inches="tight")
        plt.close()
        pdf.close()
        del adata_sub
        gc.collect()
