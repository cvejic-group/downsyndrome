import sys
import scanpy as sc
import pandas as pd


PROJ_DIR = "/lustre/scratch126/casm/team-cvejic/haoliang/pj_DownSyndrome/results/combined_analysis_res4rev/"

if __name__ == "__main__":
    adata = sc.read_h5ad(f"{PROJ_DIR}results_noadjust/a2_combined_adata.processed.h5ad")

    cell_cycle_genes = [x.strip() for x in open("../MetaData/Resources/regev_lab_cell_cycle_genes.txt")]
    s_genes = cell_cycle_genes[:43]
    g2m_genes = cell_cycle_genes[43:]
    print(s_genes, g2m_genes, file=sys.stderr)
    
    cell_cycle_genes = [x for x in cell_cycle_genes if x in adata.var_names]
    print(cell_cycle_genes, file=sys.stderr)

    sc.pp.scale(adata)
    sc.tl.score_genes_cell_cycle(adata, s_genes=s_genes, g2m_genes=g2m_genes)
    
    adata.obs.to_csv(f"{PROJ_DIR}results_noadjust/a3_tab.obs.cellcyclescore.csv", index=True)
