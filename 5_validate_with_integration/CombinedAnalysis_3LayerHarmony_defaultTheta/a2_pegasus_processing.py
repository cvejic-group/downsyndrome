import gc
import sys
import pandas as pd
import pegasus as pg
import matplotlib.pyplot as plt

PROJ_DIR = "/lustre/scratch126/casm/team-cvejic/haoliang/pj_DownSyndrome/results/combined_analysis_res4rev/"


if __name__ == "__main__":
    cellgroup = pd.read_table("cellgroup_map.csv", header=None, index_col=0).to_dict()[1]
    data = pg.read_input(f"{PROJ_DIR}combined_adata.h5ad")
    data.obs["Organ"] = [x.split("_")[1] for x in data.obs["environment"]]
    data.obs["DiseaseStatus"] = [x.split("_")[0] for x in data.obs["environment"]]
    data.obs["cell_group"] = data.obs["leiden_latest"].map(cellgroup)
    print(data.shape, file=sys.stderr)
    maptab = data.obs[["leiden_latest",
                       "cell_group"]].value_counts().reset_index().rename(columns={0: "cell_number"})
    maptab.sort_values("cell_group").to_csv(f"{PROJ_DIR}a1_celltype_map.csv")
    pg.log_norm(data, norm_count=1e4)
    pg.identify_robust_genes(data, percent_cells=0.005)
    pg.highly_variable_features(data, n_top=5000, batch="patient_sample")
    pg.hvfplot(data, top_n=100, panel_size=(9, 5))
    plt.savefig(f"{PROJ_DIR}figures_noadjust/HVFplot.png", bbox_inches="tight", facecolor="white")
    pg.pca(data, n_components=150, standardize=True, random_state=0)
    pg.elbowplot(data, rep="pca", panel_size=(9, 5))
    plt.savefig(f"{PROJ_DIR}figures_noadjust/Elbowplot.png", bbox_inches="tight", facecolor="white")
    print(f"Harmony on patient_sample.", file=sys.stderr)
    pg.run_harmony(data, batch="patient_sample", rep="pca", n_comps=data.uns["pca_ncomps"],
                   random_state=0, max_iter_harmony=50)
    print(f"Harmony on Organ.", file=sys.stderr)
    pg.run_harmony(data, batch="Organ", rep="pca_harmony", n_comps=data.uns["pca_ncomps"],
                   random_state=0, max_iter_harmony=50)
    print(f"Harmony on DiseaseStatus.", file=sys.stderr)
    pg.run_harmony(data, batch="DiseaseStatus", rep="pca", n_comps=data.uns["pca_ncomps"],
                   random_state=0, max_iter_harmony=50)
    pg.neighbors(data, K=100, rep="pca_harmony", random_state=0, use_cache=False)
    pg.leiden(data, rep="pca_harmony", resolution=3.5, random_state=0, class_label="combi_clust")
    pg.umap(data, rep="pca_harmony", n_components=2, n_neighbors=15, random_state=0)
    pg.write_output(data, f"{PROJ_DIR}results_noadjust/a2_combined_adata.processed.h5ad", precision=6)
    data.var.to_csv(f"{PROJ_DIR}results_noadjust/a3_tab.var.csv")
    data.obs.to_csv(f"{PROJ_DIR}results_noadjust/a3_tab.obs.csv")
