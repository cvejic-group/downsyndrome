import sys
import gc
import pegasus as pg
import matplotlib.pyplot as plt
import seaborn as sns


PROJ_DIR = "/lustre/scratch126/casm/team-cvejic/haoliang/pj_DownSyndrome/results/combined_analysis_res4rev/"

ANNOTLST = ["Hepatocytes=1+3+8+26+27+35+47+70+30", "Erythroid=2+6+12+13+14+16+18+19+20+22+23+28+29+34+51+54+58+61+74",
            "HSCs=4+65+38", "Cycling HSC=46", "Endothelial cells=5+21+53+62+73", "Megakaryocytes=7", "CAR (and LEPR+CAR)=9",
            "Inflammatory macrophages=10+44", "NK=11+45+52+55+57+60+63", "cDC2=15+39+59+66", "MEMP=17", "Granulocytes=24+75",
            "Granulocyte progenitors=40", "B cells=37+48", "Kupffer cells=31", "Hepatic stellate cells=32+50+69",
            "Tolerogenic macrophages=64", "Osteoclasts=67", "Mast cells=33", "Fibroblasts=25+36+49+72", "pDCs=41",
            "Osteo-lineage=43", "Schwann=56", "Pericytes=42", "Collagen+ myeloid cells=68", "Monocyte progenitors=71"]
ANNOTMAP = {y: x.split("=")[0] for x in ANNOTLST for y in x.split("=")[1].split("+")}
ENVANNT2RM = ["Tolerogenic macrophages|DownSyndrome_Liver", "Tolerogenic macrophages|Healthy_Liver", 
              "Kupffer cells|Healthy_Femur", "Kupffer cells|DownSyndrome_Femur", 
              "Collagen+ myeloid cells|DownSyndrome_Liver", "Collagen+ myeloid cells|Healthy_Liver", 
              "Osteoclasts|DownSyndrome_Liver", "Osteoclasts|Healthy_Liver"]
COLOUR_DICT = {"Stroma": "#000080", "Erythroid": "#F08080", "HSCs": "#FF0000", "Cycling HSC": "#DC143C",
               "Megakaryocytes": "#663399", "Inflammatory macrophages": "#9932CC", "Tolerogenic macrophages": "#DA70D6",
               "NK": "#A0522D", "cDC2": "#FFD700", "pDCs": "#FF8C00", "MEMP": "#FFA07A", "Granulocytes": "#CD853F",
               "Granulocyte progenitors": "#B8860B", "B cells": "#BDB76B", "Kupffer cells": "#FF69B4", "Osteoclasts": "#FFC0CB",
               "Mast cells": "#8B008B", "Collagen+ myeloid cells": "#FF1493", "Monocyte progenitors": "#FF00FF"}


if __name__ == "__main__":
    data = pg.read_input(f"{PROJ_DIR}results_noadjust/a2_combined_adata.processed.h5ad")
    data.obs["combi_annot"] = data.obs["combi_clust"].map(ANNOTMAP)
    for ctype in ["Hepatocytes", "Hepatic stellate cells", "Endothelial cells", "Fibroblasts",
                  "CAR (and LEPR+CAR)", "Osteo-lineage", "Schwann", "Pericytes"]:
        data.obs["combi_annot"].replace(ctype, "Stroma", inplace=True)
    data.obs["env_combi_annot"] = data.obs["combi_annot"] + "|" + data.obs["environment"].astype("str")
    data.obs["combi_annot"] = data.obs["combi_annot"].astype("category")
    data.obs["combi_annot"] = data.obs["combi_annot"].cat.reorder_categories(list(COLOUR_DICT.keys()))
    data = data[~data.obs["env_combi_annot"].isin(ENVANNT2RM), :].copy()
    gc.collect()
    for env in data.obs["environment"].unique():
        pg.scatter(data, attrs="combi_annot", basis="umap", restrictions=f"environment:{env}", show_background=True,
                   palettes=",".join(data.obs["combi_annot"].cat.categories.map(COLOUR_DICT).tolist()),
                   panel_size=(6, 6), marker_size=20, dpi=600)
        plt.savefig(f"{PROJ_DIR}figures_final/Harmony_UMAP.{env}.png", bbox_inches="tight", facecolor="white", dpi=600)
