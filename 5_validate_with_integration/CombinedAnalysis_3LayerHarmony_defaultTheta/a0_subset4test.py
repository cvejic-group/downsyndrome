import os
import gc
import sys
import scanpy as sc
import pandas as pd
import harmonypy as hm
import matplotlib.pyplot as plt
import seaborn as sns

PROJ_DIR = "/lustre/scratch126/casm/team-cvejic/haoliang/pj_DownSyndrome/results/"
IN_DIR = f"{PROJ_DIR}combined_analysis_res4rev/"
OUT_DIR = f"{PROJ_DIR}combined_analysis_res4rev_3LHarmony/"

if __name__ == "__main__":
    adata = sc.read_h5ad(f"{IN_DIR}combined_adata.h5ad")
    sc.pp.subsample(adata, n_obs=100000)
    adata.write_h5ad(f"{OUT_DIR}combined_adata.subset.h5ad")
