import gc
import sys
import pegasus as pg

PROJ_DIR = "/lustre/scratch126/casm/team-cvejic/haoliang/pj_DownSyndrome/results/combined_analysis_res4rev/"


if __name__ == "__main__":
    data = pg.read_input(f"{PROJ_DIR}results_noadjust/a2_combined_adata.processed.h5ad")
    for clust in ["13", "16"]:
        print(clust, file=sys.stderr)
        pg.write_output(data[data.obs["combi_clust"] == clust, :].copy(),
                        f"{PROJ_DIR}results_noadjust/splitted_outputs/cluster{clust}.h5ad", precision=6)
        gc.collect()
