import gc
import sys
import pandas as pd
import pegasus as pg
import matplotlib.pyplot as plt

PROJ_DIR = "/lustre/scratch126/casm/team-cvejic/haoliang/pj_DownSyndrome/results/combined_analysis_res4rev/"


if __name__ == "__main__":
    data = pg.read_input(f"{PROJ_DIR}results_noadjust/a2_combined_adata.processed.h5ad")
    pg.de_analysis(data, "combi_clust")
    marker_dict = pg.markers(data, head=100)
    for k, v in marker_dict.items():
        v["up"].sort_values(by="log2FC", ascending=False).to_csv(f"{PROJ_DIR}/results_noadjust/mkgs/up.{k}.csv")
        v["down"].sort_values(by="log2FC", ascending=True).to_csv(f"{PROJ_DIR}/results_noadjust/mkgs/down.{k}.csv")
    pg.write_results_to_excel(marker_dict, f"{PROJ_DIR}results_noadjust/mkgs/summary.xlsx")
