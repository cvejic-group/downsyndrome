from os import system as sys

samples = ['cellranger310_count_HVJYLDMXX_SIGAA2_GRCh38-3_0_0',
           'cellranger310_count_HVJYLDMXX_SIGAB2_GRCh38-3_0_0',
           'cellranger310_count_HVJYLDMXX_SIGAC2_GRCh38-3_0_0',
           'cellranger310_count_HVJYLDMXX_SIGAD2_GRCh38-3_0_0',
           'cellranger310_count_HVJYLDMXX_SIGAE2_GRCh38-3_0_0',
           'OT10xRNA8761787',
           'OT10xRNA8761795',
           'OT10xRNA8761795_LowerBound',
]

for sample in samples:

    fname = "add_chromosomes_{0}".format(sample)
    f = open(fname+".sh", 'w')
    f.write("cd /nfs/research1/gerstung/nelson/data/downsyndrome/Samples/10X/{0}/raw_feature_bc_matrix\n".format(sample))
    f.write("conda activate base\n" )
    f.write("source /nfs/research1/gerstung/nelson/data/downsyndrome/perl_add_chromosomes.sh")
    f.close()
    print("bsub -M 20000 -n 2 -o {0}.txt -e {0} bash {0}.sh".format(fname))
    sys("bsub -M 20000 -n 2 -o {0}.txt -e {0} bash {0}.sh".format(fname))
