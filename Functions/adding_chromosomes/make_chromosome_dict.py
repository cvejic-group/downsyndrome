from os import system as sys
import json

samples = ['cellranger310_count_HVJYLDMXX_SIGAA2_GRCh38-3_0_0',
           'cellranger310_count_HVJYLDMXX_SIGAB2_GRCh38-3_0_0',
           'cellranger310_count_HVJYLDMXX_SIGAC2_GRCh38-3_0_0',
           'cellranger310_count_HVJYLDMXX_SIGAD2_GRCh38-3_0_0',
           'cellranger310_count_HVJYLDMXX_SIGAE2_GRCh38-3_0_0',
           'OT10xRNA8761787',
           'OT10xRNA8761795',
           'OT10xRNA8761795_LowerBound',
]

chromosome_dict = dict()
inDir = "/nfs/research1/gerstung/nelson/data/downsyndrome/"

def main():
    for sample in samples:
        f = open(inDir+"add_chromosomes_{0}.txt".format(sample), "r")
        sample_dict = dict()
        for line in f:
            if "ENSG" in line:
                sample_dict[line.split(" ")[0]] = line.split(" ")[-3]

        chromosome_dict[sample] = sample_dict
        del sample_dict

    with open(inDir+'chromosome_numbers_downsyndrome.json', 'w') as outFile:
        json.dump(chromosome_dict, outFile)

if __name__=="__main__":
    main()
