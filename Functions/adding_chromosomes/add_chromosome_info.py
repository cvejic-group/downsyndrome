import pandas as pd
import numpy as np
import gzip

inDir = "/nfs/research1/gerstung/nelson/data/downsyndrome/"

def main(sample="cellranger310_count_HVJYLDMXX_SIGAA2_GRCh38-3_0_0"):

    f = open(inDir+"add_chromosomes_{0}.txt".format(sample), "r")

    # Make the chromosome dictionary
    chromosome_dict = dict()
    for line in f:
        if "ENSG" in line:
            chromosome_dict[line.split(" ")[0]] = line.split(" ")[-3]

    # Now modify the features
    df = pd.read_csv(inDir+"/Samples/10X/{0}/raw_feature_bc_matrix/features.tsv.gz".format(sample), sep='\t', names=['Gene ID', 'Gene Symbol', 'Feature'] )
    print("Number of dict keys: ", len(chromosome_dict.keys()))
    print("Number of df rows: : ", df.shape[0])
    df["Chromosome number"] = np.array([chromosome_dict[ID] if ID in chromosome_dict.keys() else 'Not found' for ID in df["Gene ID"]])
    df.to_csv(inDir+"/Samples/10X/{0}/raw_feature_bc_matrix/features_with_chromosome.tsv.gz".format(sample), index=False, header=False, compression="gzip", sep="\t")

if __name__=="__main__":
    # Adding an argument parser
    from argparse import ArgumentParser
    parser = ArgumentParser()
    parser.add_argument("-s", "--sample", help="Specify the sample to run on",default="cellranger310_count_HVJYLDMXX_SIGAA2_GRCh38-3_0_0")

    options = parser.parse_args()

    # Defining dictionary to be passed to the main function
    option_dict = dict( (k, v) for k, v in vars(options).items() if v is not None)
    print(option_dict)
    main(**option_dict)
