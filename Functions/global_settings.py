def global_settings():

    import warnings
    warnings.simplefilter("ignore")

    import bbknn
    import numpy as np
    import scanpy as sc
    import pandas as pd
    import seaborn as sns
    import scrublet as scr
    import matplotlib.pyplot as plt
    import matplotlib.backends.backend_pdf as mpdf

    from math import log
    from scipy import sparse
    from matplotlib import cm
    from matplotlib import colors
    from scipy.sparse import issparse
    from sklearn.utils import shuffle
    from scipy.spatial import distance
    from sklearn.preprocessing import normalize
    from sklearn.model_selection import train_test_split

    print("Loading global settings")

    # Global colour settings
    myColors = ['#e6194b', '#3cb44b', '#ffe119', '#4363d8', '#f58231',
            '#911eb4', '#46f0f0', '#f032e6', '#bcf60c', '#fabebe',
            '#008080', '#e6beff', '#9a6324', '#fffac8', '#800000',
            '#aaffc3', '#808000', '#ffd8b1', '#000075', '#808080',
            '#307D7E', '#000000', "#DDEFFF", "#000035", "#7B4F4B",
            "#A1C299", "#300018", "#C2FF99", "#0AA6D8", "#013349",
            "#00846F", "#8CD0FF", "#3B9700", "#04F757", "#C8A1A1",
            "#1E6E00", "#DFFB71", "#868E7E", "#513A01", "#CCAA35"]

    myColorTissues = ['#ffe119', '#3cb44b', '#e6194b']

    # Define a nice colour map for gene expression
    colors2 = plt.cm.Reds(np.linspace(0, 1, 128))
    colors3 = plt.cm.Greys_r(np.linspace(0.7,0.8,20))
    colorsComb = np.vstack([colors3, colors2])
    mymap = colors.LinearSegmentedColormap.from_list('my_colormap', colorsComb)
    sns.set_style("white")
