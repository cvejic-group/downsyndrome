rm(list = ls())

library(ggplot2)
library(ggrepel)

gene.stats <- read.table("../../DEG_res/res_by_Andrew_HighFC/HSC_v_CyclingHSC.txt",
                         header = TRUE)
gene.stats$significant <- (gene.stats$adj.P.Val < 0.05)
gene.stats$updown <- (abs(gene.stats$logFC) > 1)
gene.stats$colour <- (gene.stats$significant & gene.stats$updown)
gene.stats$marker <- apply(gene.stats, MARGIN = 1,
                           FUN = function(x) ifelse(x["names"] %in% c("NRIP1", "SUMO1", "SUMO2", "SUMO3", "APP", 
                                                                      "VDAC1", "ULK1", "FUNDC1", "NBR1",
                                                                      "JAK1", "IFNAR1", "ISG15", "MAVS"),
                                                    yes = x["names"], no = "")) %>% unlist()

plt <- ggplot() +
    geom_point(data = gene.stats,
               aes(x = -logFC, y = -log10(adj.P.Val), colour = colour)) +
    geom_point(data = gene.stats[gene.stats$marker != "", ],
               aes(x = -logFC, y = -log10(adj.P.Val), colour = marker)) +
    geom_text_repel(data = gene.stats[gene.stats$marker != "", ],
                    aes(x = -logFC, y = -log10(adj.P.Val),
                        label = marker, colour = marker), max.overlaps = 1000) +
    scale_colour_manual(values = c("FALSE" = "gray", "TRUE" = "skyblue",
                                   "NRIP1" = "brown2", "SUMO1" = "brown2", "SUMO2" = "brown2", "SUMO3" = "brown2", "APP" = "brown2",
                                   "VDAC1" = "darkorange", "ULK1" = "darkorange", "FUNDC1" = "darkorange", "NBR1" = "darkorange",
                                   "JAK1" = "darkviolet", "IFNAR1" = "darkviolet", "ISG15" = "darkviolet", "MAVS" = "darkviolet")) +
    xlab("log fold change") +
    ylab("-log10(FDR)") +
    theme_minimal() +
    theme(legend.position = "none",
          text = element_text(size = 20))
plt
ggsave(plt, filename = "../../DEG_res/res_by_Andrew_HighFC/volcano.pdf",
       width = 12, height = 5)
