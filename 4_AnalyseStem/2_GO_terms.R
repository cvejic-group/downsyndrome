rm(list = ls())

library(clusterProfiler)
library(org.Hs.eg.db)
library(ggplot2)
library(ggrepel)

gene.stats <- read.table("../../DEG_res/res_by_Andrew_HighFC/HSC_v_CyclingHSC.txt",
                         header = TRUE)
gene.stats$logFC <- -gene.stats$logFC
gene.stats <- gene.stats[gene.stats$adj.P.Val < 0.05, ]
gene.stats.up <- gene.stats[gene.stats$logFC > 1, ]
gene.stats.up <- gene.stats.up[order(gene.stats.up$logFC, decreasing = TRUE), ]
gene.stats.down <- gene.stats[gene.stats$logFC < -1, ]
gene.stats.down <- gene.stats.down[order(gene.stats.down$logFC, decreasing = FALSE), ]

ego.up <- enrichGO(gene.stats.up$names,
                   keyType = "SYMBOL", OrgDb = org.Hs.eg.db, ont = "BP",
                   pAdjustMethod = "BH", pvalueCutoff = 0.01, qvalueCutoff = 0.05,
                   readable = TRUE)
ego.up.df <- as.data.frame(ego.up)
ego.up.df$group <- "UP"
write.csv(ego.up.df,
          paste0("../../DEG_res/res_by_Andrew_HighFC/new_HSC_v_CyclingHSC_upGO.csv"),
          row.names = FALSE, quote = FALSE)

ego.down <- enrichGO(gene.stats.down$names,
                     keyType = "SYMBOL", OrgDb = org.Hs.eg.db, ont = "BP",
                     pAdjustMethod = "BH", pvalueCutoff = 0.01, qvalueCutoff = 0.05,
                     readable = TRUE)
ego.down.df <- as.data.frame(ego.down)
ego.down.df$group <- "DOWN"
write.csv(ego.up.df,
          paste0("../../DEG_res/res_by_Andrew_HighFC/new_HSC_v_CyclingHSC_downGO.csv"),
          row.names = FALSE, quote = FALSE)


ego.df <- rbind(ego.up.df, ego.down.df)
ego.df <- ego.df[order(ego.df$qvalue, decreasing = FALSE), ]
ego.df$marker <- ifelse(ego.df$Description %in% ego.df$Description[1:20],
                        yes = ego.df$Description, no = NA)

plt <- ggplot(ego.df) +
    geom_point(aes(x = group, y = -log10(qvalue), colour = group),
               position = position_jitter(seed = 0)) +
    geom_text_repel(aes(x = group, y = -log10(qvalue), label = marker, colour = group), 
                    min.segment.length = -1, max.overlaps = 100,
                    position = position_jitter(seed = 0)) +
    scale_colour_manual(values = c("UP" = "orange", "DOWN" = "lightblue"),
                        na.value = "skyblue") +
    xlab("Ontology") +
    ylab("-log10(FDR)") +
    theme_bw() +
    theme(text = element_text(size = 15), legend.position = "none")
plt
ggsave(plt, filename = "../../DEG_res/res_by_Andrew_HighFC/GOterm_scatter.pdf", 
       width = 9, height = 9)

